use std::collections::HashMap;

use backend::primitives::shared::{types::DeviceHandle, utils::PortSet};

pub enum FwAction {
    Allow,
    Deny,
}

pub enum RuleDirection {
    OneWay,
    Bidirectional,
    Established,
}

pub struct FwRule {
    source_ports:      PortSet,
    destination_ports: PortSet,
    direction:         RuleDirection,
    action:            FwAction,
}

pub struct Firewall(HashMap<DeviceHandle, HashMap<DeviceHandle, Vec<FwRule>>>);
