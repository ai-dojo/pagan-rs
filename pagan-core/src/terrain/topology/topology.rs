use std::collections::HashMap;

use backend::{
    error::PaganError,
    primitives::{
        accounts::Account,
        shared::{
            enums::AccessType,
            types::{AccountHandle, DeviceHandle, PaganResult},
        },
    },
    stores::{AccountStore, CredentialStore, DeviceStore, StoreRead},
};

use crate::terrain::topology::firewall::Firewall;

pub struct Topology {
    device_store:     DeviceStore,
    account_store:    AccountStore,
    credential_store: CredentialStore,
    accesses:         HashMap<DeviceHandle, Vec<(DeviceHandle, AccessType)>>,
    firewall:         Firewall,
}

impl Topology {
    pub fn device_store(&self) -> &DeviceStore { &self.device_store }

    pub fn accesses(&self) -> &HashMap<DeviceHandle, Vec<(DeviceHandle, AccessType)>> { &self.accesses }

    // TODO: all devices have to be accessible to themselves, use LocalHost access type
    pub fn attacker(&self) -> PaganResult<DeviceHandle> {
        match self.device_store.attacker() {
            None => Err(PaganError::TopologyError(
                "Attacker not set but queried through topology.".into(),
            )),
            Some(handle) => Ok(handle),
        }
    }

    pub fn account_store(&self) -> &AccountStore { &self.account_store }

    pub fn handle_to_account_ref(&self, handle: AccountHandle) -> PaganResult<&'static Account> {
        self.account_store
            .get(handle)
            .map(|acc| unsafe { std::mem::transmute::<&Account, &'static Account>(acc) })
    }

    pub fn credential_store(&self) -> &CredentialStore { &self.credential_store }
}
