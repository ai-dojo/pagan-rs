mod wkp {
    // WellKnownPorts
    use backend::primitives::shared::utils::PortSet;
    use once_cell::sync::Lazy;

    static REMOTE_MANAGEMENT: Lazy<PortSet> = Lazy::new(|| PortSet::try_from([3389, 22, 5985, 5986, 5900]).unwrap()); // # RDP, SSH, WINRM-HTTP, WINRM-HTTPS, VNC
    static ACTIVE_DIRECTORY: Lazy<PortSet> = Lazy::new(|| {
        let mut singles = vec![123, 135, 464, 389, 636, 3268, 3269, 53, 853, 88, 445];
        singles.extend(49152 .. 65535);
        PortSet::try_from(singles).unwrap()
    });
    // based on https://learn.microsoft.com/en-US/troubleshoot/windows-server/identity/config-firewall-for-ad-domains-and-trusts

    static DATABASES: Lazy<PortSet> = Lazy::new(|| {
        PortSet::try_from([
            7210, 3306, 1521, 1830, 5432, 1433, 1434, 8529, 7000, 7001, 9042, 5984, 9200, 9300, 27017, 27018, 27019,
            28017, 7473, 7474, 6379, 8087, 8098, 8080, 28015, 29015, 7574, 8983,
        ])
        .unwrap()
    });

    static REMOTE_FILESYSTEM: Lazy<PortSet> = Lazy::new(|| PortSet::try_from([20, 21, 989, 990, 944, 445]).unwrap());
    static DYNAMIC_PORTS: Lazy<PortSet> = Lazy::new(|| PortSet::try_from(49152 .. 65535).unwrap());
    static VPN: Lazy<PortSet> =
        Lazy::new(|| PortSet::try_from([443, 1194, 51820, 992, 5555, 4500, 1701, 4500, 500]).unwrap());
    static EMAIL_CLIENT_SERVER: Lazy<PortSet> =
        Lazy::new(|| PortSet::try_from([25, 587, 465, 2525, 110, 995, 143, 993]).unwrap());
    static EMAIL_SERVER_RELAY: Lazy<PortSet> = Lazy::new(|| PortSet::try_from([25]).unwrap());
    static ALL: Lazy<PortSet> = Lazy::new(|| PortSet::try_from(1 .. 65535).unwrap());
    static WEB: Lazy<PortSet> = Lazy::new(|| PortSet::try_from([80, 443]).unwrap());
    static DNS: Lazy<PortSet> = Lazy::new(|| PortSet::try_from([53]).unwrap());
    static ICS: Lazy<PortSet> = Lazy::new(|| {
        let mut singles = vec![
            502, 1089, 1090, 1091, 1541, 2222, 3480, 4000, 5050, 5051, 5052, 5065, 5450, 9600, 10307, 10311, 10364,
            10365, 10407, 10414, 10415, 10428, 10431, 10432, 10447, 10449, 10450, 12316, 12645, 12647, 12648, 13722,
            11001, 12135, 12136, 12137, 13724, 13782, 13783, 18000, 20000, 34962, 34963, 34964, 34980, 38589, 38593,
            38000, 38001, 38011, 38012, 38014, 38015, 38200, 38210, 38301, 38400, 38600, 38700, 38971, 39129, 39278,
            44818, 45678, 47808, 50110, 50111, 55000, 55001, 55002, 55003, 55555, 62900, 62911, 62924, 62930, 62938,
            62956, 62957, 62963, 62981, 62982, 62985, 62992, 63012, 63041, 63075, 63079, 63082, 63088, 63094, 65443,
        ];
        singles.extend(50001 .. 50016);
        singles.extend(50018 .. 50021);
        singles.extend(50025 .. 50028);
        singles.extend(56001 .. 56099);
        singles.extend(63027 .. 63036);
        PortSet::try_from(singles).unwrap()
    }); // https://github.com/ITI/ICS-Security-Tools/blob/master/protocols/PORTS.md
}
