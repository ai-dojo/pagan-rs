use std::{
    borrow::{Borrow, Cow},
    cell::RefCell,
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    ops::BitOrAssign,
    pin::{pin, Pin},
    rc::Rc,
    sync::{Arc, LockResult, Mutex, MutexGuard, RwLock, TryLockError, TryLockResult},
};

use backend::{
    error::PaganError,
    iter_or_empty,
    make_borrowed_cow,
    pinptr_inner_ref,
    primitives::{
        accounts::Account,
        adversary::{ArtifactSatchel, UsageScheme},
        capabilities::{AttackerCapability, Capability},
        credentials::{Credential, ServiceCredential, SystemCredential},
        device::Device,
        enablers::EnablerTemplate,
        person::Person,
        predicate::Predicate,
        quiver::ADVERSARY_ACTION_MAP,
        shared::{
            enums::{
                enabler_impacts_as_capabilities,
                service_impacts_as_capabilities,
                AccessType,
                CapabilitySpecifier,
                CapabilityType,
                CredentialOrigin,
                CredentialType,
                DeviceRole,
                EnablerImpact,
                PrivilegeLevel,
                PrivilegeOrigin,
                ServiceImpact,
                Token,
            },
            traits::{ComplexStrength, IntoBorrowedCow},
            types::{
                AccountHandle,
                ActionHandle,
                DeviceHandle,
                ImpactRepr,
                PaganResult,
                RunAs,
                SchemeHandle,
                ServiceHandle,
            },
            utils::{HandleSet, SetOption, SetOptionAtomicOps, SetOptionCombinatorOps},
        },
    },
    services_only,
    stores::{EnablerStore, ServiceStore, StoreRead, StoreReverseGet},
};
use flagset::{FlagSet, Flags};
use petgraph::graph::NodeIndex;
use rayon::ThreadPoolBuilder;
use strum::IntoEnumIterator;

use crate::{
    scenario::automaton::{AdversaryControlState, EdgeData, ScenarioStateMachine, StateHandle},
    shared::{config::ScenarioConfiguration, custom_hash_set::CustomHashSetWithImmutableAndImmovableElements},
    terrain::Topology,
};




/**
   Sugar to send a result via an MPSC channel with panicking on it being closed or poisoned.
**/
#[macro_export]
macro_rules! report_mpsc {
    ($status:expr, $channel:expr) => {
        $channel
            .send($status)
            .expect("Thread panic, the MPSC channel was disconnected while a compute was still working.")
    };
}

macro_rules! unwrap_iter_or_report_err {
    ($from_set:expr, $channel:ident) => {
        match $from_set {
            Ok(iter) => iter,
            Err(e) => {
                report_mpsc!(StateExplorationResult::Error(e), $channel);
                Box::new(std::iter::empty())
            },
        }
    };
}

macro_rules! handle_maybe_runas {
    ($query_res:expr, $channel:ident, $fastfail:expr) => {
        match $query_res {
            Ok(maybe_handle) => match maybe_handle {
                None => $fastfail,
                Some(handle) => RunAs::Account(handle),
            },
            Err(e) => {
                report_mpsc!(StateExplorationResult::Error(e), $channel);
                $fastfail
            },
        }
    };
}

macro_rules! handle_must_be_runas {
    ($query_res:expr, $handle:expr, $channel:ident, $fastfail:expr) => {
        match $query_res {
            None => {
                report_mpsc!(
                    StateExplorationResult::Error(PaganError::TopologyError(
                        format!("Device {} has no compulsory (system or service) account.", $handle).into()
                    )),
                    $channel
                );
                $fastfail
            },
            Some(acc) => RunAs::Account(acc),
        }
    };
}

macro_rules! primitive_to_flagset {
    ($primitive:expr, $typ:ty, $channel:ident) => {
        match FlagSet::<$typ>::new($primitive) {
            Ok(fs) => fs,
            Err(e) => {
                report_mpsc!(StateExplorationResult::Error(e.into()), $channel);
                continue;
            },
        }
    };
}

macro_rules! insert_token_if_missing {
    ($new_token:expr, $device_id:expr, $old_collection:ident) => {
        match $old_collection.get(&$device_id) {
            None => {
                let _ = $old_collection.to_mut().insert($device_id, FlagSet::from($new_token));
            },
            Some(flag) => {
                if !flag.contains($new_token) {
                    $old_collection
                        .to_mut()
                        .entry($device_id)
                        .and_modify(|old_flag| old_flag.bitor_assign($new_token));
                }
            },
        }
    };
}

macro_rules! extend_credentials_if_missing {
    ($new_creds:expr, $old_creds:ident) => {{
        let nc_ref = $new_creds.into_iter().collect::<BTreeSet<_>>();
        if !$old_creds.is_superset(&nc_ref) {
            $old_creds.to_mut().extend(nc_ref)
        }
    }};
}

macro_rules! insert_credential_if_missing {
    ($new_cred:expr, $old_creds:ident) => {{
        let nc: Cow<Credential> = $new_cred;
        if !$old_creds.contains(&nc) {
            $old_creds.to_mut().insert(nc);
        }
    }};
}

/**
Converts RunAs variants to RunAs::AccountHandle if possible. Otherwise, reports an error.
**/
macro_rules! handleize_runas {
    ($original:ident, $topology:ident, $channel:ident, $state_data:ident) => {
        match $original {
            RunAs::Account(acc) => match $topology.account_store().get_handle(acc) {
                None => {
                    report_mpsc!(
                        StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                            "Could not find handle by account".into()
                        )),
                        $channel
                    );
                    continue;
                },
                Some(handle) => RunAs::AccountHandle(handle),
            },
            RunAs::AccountHandle(_) => $original,
            RunAs::Privilege(privilege) => match privilege {
                PrivilegeLevel::Service => {
                    handle_must_be_runas!(
                        $topology
                            .device_store()
                            .get($state_data.device())
                            .unwrap()
                            .service_account(),
                        $state_data.device(),
                        $channel,
                        continue
                    )
                },
                PrivilegeLevel::LocalSystem => {
                    handle_must_be_runas!(
                        $topology
                            .device_store()
                            .get($state_data.device())
                            .unwrap()
                            .system_account(),
                        $state_data.device(),
                        $channel,
                        continue
                    )
                },
                _ => {
                    report_mpsc!(
                        StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                            format!("Runas in edge data can not be computed on the fly from {privilege}").into()
                        )),
                        $channel
                    );
                    continue;
                },
            },
            RunAs::Invalid => {
                continue;
            },
        }
    };
}

/**
Checks if the received artifact collection is borrowed or owned. If borrowed,
no change was done to it, thus it exist in the global collection and the ref to it is valid and can be returned.
If it's owned, a change was made, so we need to try inserting into the global collection, and
receive & return a ref to it.
**/
macro_rules! handle_received_artifacts {
    ($artifacts:expr, $collection:expr) => {
        match $artifacts {
            Cow::Borrowed(a_ref) => &*a_ref,
            Cow::Owned(owned) => $collection.get_or_insert(owned),
        }
    };
}

macro_rules! privilege_on_device {
    ($topology:ident, $account_id:expr, $device:ident) => {
        $topology
            .account_store()
            .get($account_id)
            .unwrap()
            .capabilities()
            .get(&$device)
            .unwrap()
            .iter()
            .find(|cap| cap.to() == CapabilityType::CodeExecution)
            .unwrap()
            .privilege_level()
    };
}

/**
   Datastructures to wrap results and reports passed between computes and the main thread.
**/
pub enum StateExplorationResult {
    Done,
    SubResult {
        new_device:                    DeviceHandle,
        new_tokens:                    Cow<'static, BTreeMap<Option<DeviceHandle>, FlagSet<Token>>>,
        new_capabilities:              Cow<'static, BTreeSet<AttackerCapability>>,
        new_os_credentials:            Cow<'static, BTreeSet<Cow<'static, Credential>>>,
        new_binding_service_creds:     Cow<'static, BTreeSet<Cow<'static, Credential>>>,
        new_non_binding_service_creds: Cow<'static, BTreeMap<Option<&'static Person>, Cow<'static, Credential>>>,
        from_node:                     NodeIndex,
        edge_data:                     Box<EdgeData>,
        accepting:                     bool,
    },
    Error(PaganError),
    TryAgain {
        acs:       Arc<AdversaryControlState>,
        idx:       NodeIndex,
        new_depth: usize,
    },
}

/**
The core of the generation algorithm.

It does state exploration, parallelized the following way:
   - The main thread acts as a task-server, result-receiver. It receives newly explored states
       from the computes, tries to insert it into the automaton. If the reported state is wholly
       new, a new exploration task is created for it; otherwise it is left untouched.
   - Because the depth of the states can change (we explore new, shorter paths) the main thread
       also schedules depth-repair jobs.

   - Computes run exploration and depth-repair tasks.
       - An exploration tasks consists of taking a state, and enumerating all its followers. Then
           reporting these followers to the main thread for optional further scheduling.
       - A depth-repair consist of taking the queue of states marked for reparation, and searching their
           subtrees, changing the depths to the minimum. If we find existing states, that were not
           explored beforehand due to depth limitations, but now the depth was decreased,
           we report those for further exploration.

   - Critical sections:
       - Exploration tasks do not use data that would change while they compute,
           thus no critical sections are present.
       - The repair jobs share the automatons' graph-part mutably with the main thread,
           thus an RwLock mechanism is set up to synchronize accesses.
**/
pub struct ScenarioBuilder {
    configuration: Arc<ScenarioConfiguration>,
    automaton: Arc<Mutex<ScenarioStateMachine>>,
    root: NodeIndex,
    topology: Arc<Topology>,
    computes: rayon::ThreadPool,
    token_sets: CustomHashSetWithImmutableAndImmovableElements<BTreeMap<Option<DeviceHandle>, FlagSet<Token>>>,
    capability_sets: CustomHashSetWithImmutableAndImmovableElements<BTreeSet<AttackerCapability>>,
    os_credential_sets: CustomHashSetWithImmutableAndImmovableElements<BTreeSet<Cow<'static, Credential>>>,
    binding_service_cred_sets: CustomHashSetWithImmutableAndImmovableElements<BTreeSet<Cow<'static, Credential>>>,
    non_binding_service_cred_sets:
        CustomHashSetWithImmutableAndImmovableElements<BTreeMap<Option<&'static Person>, Cow<'static, Credential>>>,
}

impl ScenarioBuilder {
    pub fn new(configuration: ScenarioConfiguration, topology: Arc<Topology>) -> PaganResult<Self> {
        let mut slf = Self {
            configuration: Arc::new(configuration),
            automaton: Default::default(),
            root: NodeIndex::new(0),
            topology,
            computes: ThreadPoolBuilder::new().build()?,
            token_sets: Default::default(),
            capability_sets: Default::default(),
            os_credential_sets: Default::default(),
            binding_service_cred_sets: Default::default(),
            non_binding_service_cred_sets: Default::default(),
        };
        slf.setup()?;
        Ok(slf)
    }

    fn setup(&mut self) -> PaganResult<()> {
        let root_data = Arc::new(AdversaryControlState::new(
            self.topology.device_store().attacker().unwrap(),
            self.capability_sets.get_or_insert(BTreeSet::new()),
            self.os_credential_sets.get_or_insert(
                self.configuration
                    .available_credentials()
                    .into_iter()
                    .filter_map(|elem| match elem.as_ref() {
                        Credential::System(_) => Some(elem.make_borrowed()),
                        _ => None,
                    })
                    .collect::<BTreeSet<Cow<'static, Credential>>>(),
            ),
            self.binding_service_cred_sets.get_or_insert(BTreeSet::new()),
            self.non_binding_service_cred_sets.get_or_insert(
                self.configuration
                    .available_credentials()
                    .into_iter()
                    .filter_map(|elem| match elem.as_ref() {
                        Credential::Service(sc) => Some((sc.holder(), elem.make_borrowed())),
                        _ => None,
                    })
                    .collect::<BTreeMap<Option<&Person>, Cow<Credential>>>(),
            ),
            self.token_sets
                .get_or_insert(self.configuration.available_tokens().to_owned()),
            false,
        ));
        self.root = self
            .automaton
            .lock()
            .and_then(|mut automaton| Ok(automaton.add_vertex(root_data, Option::from(0usize)).unwrap().unseal()))?;

        Ok(())
    }

    pub fn configuration(&self) -> &ScenarioConfiguration { &self.configuration }

    pub fn automaton(&self) -> &Mutex<ScenarioStateMachine> { self.automaton.borrow() }

    pub fn root(&self) -> NodeIndex { self.root }

    pub fn topology(&self) -> &Arc<Topology> { &self.topology }

    /**
    The main automaton building function, that schedules state exploration. Runs on the main thread.
    Works as a task server & result handler.
    **/
    pub fn build(&mut self) -> PaganResult<()> {
        let mut created_jobs = 0usize;
        let mut finished_jobs = 0usize;

        let (send_end, receive_end): (
            crossbeam::channel::Sender<StateExplorationResult>,
            crossbeam::channel::Receiver<StateExplorationResult>,
        ) = crossbeam::channel::unbounded();

        // Create initial state exploration job on the computes
        let root_idx = self.root;
        let root_data = match self.automaton.lock() {
            Ok(automaton) => automaton.get_node(self.root),
            Err(_) => return Err(PaganError::LockError("Poisoned automaton mutex".into())),
        }?;
        let config_arc = Arc::clone(&self.configuration);
        let topology_arc = Arc::clone(&self.topology);
        let report_channel = send_end.clone();
        self.computes.spawn(move || {
            explore_state(
                Option::from(0),
                root_idx,
                root_data,
                config_arc,
                topology_arc,
                report_channel,
            );
        });
        let mut repair_counter = 0;
        loop {
            // while there are unprocessed reports
            while let Ok(exploration_result) = receive_end.try_recv() {
                repair_counter += 1;
                if repair_counter >= 10_000 {
                    // time for depth-repair if needed
                    let report_channel = send_end.clone();
                    let automaton_arc: Arc<Mutex<ScenarioStateMachine>> = Arc::clone(&self.automaton);
                    let configuration_arc = Arc::clone(&self.configuration);
                    self.computes.spawn(move || match automaton_arc.lock() {
                        Ok(mut automaton) => {
                            automaton.correct_depths(report_channel, configuration_arc.max_action_count())
                        },
                        Err(_e) => {
                            report_mpsc!(
                                StateExplorationResult::Error(PaganError::LockError("Poisoned automaton mutex".into())),
                                report_channel
                            )
                        },
                    });
                    repair_counter = 0;
                }
                match exploration_result {
                    StateExplorationResult::Done => {
                        // a states' exploration was completely finished
                        finished_jobs += 1;
                    },
                    StateExplorationResult::SubResult {
                        new_device,
                        new_tokens,
                        new_capabilities,
                        new_os_credentials,
                        new_binding_service_creds,
                        new_non_binding_service_creds,
                        from_node,
                        edge_data,
                        accepting, // a reachable state was found and reported
                    } => {
                        match self.automaton.try_lock() {
                            Err(e) => match e {
                                TryLockError::Poisoned(_) => {
                                    return Err(PaganError::LockError("Poisoned automaton mutex".into()))
                                },
                                TryLockError::WouldBlock => {
                                    report_mpsc!(
                                        StateExplorationResult::SubResult {
                                            new_device,
                                            new_tokens,
                                            new_capabilities,
                                            new_os_credentials,
                                            new_binding_service_creds,
                                            new_non_binding_service_creds,
                                            from_node,
                                            edge_data,
                                            accepting
                                        },
                                        send_end
                                    )
                                },
                            },
                            Ok(mut automaton) => {
                                let handle = automaton.add_vertex(
                                    Arc::new(AdversaryControlState::new(
                                        new_device,
                                        handle_received_artifacts!(new_capabilities, self.capability_sets),
                                        handle_received_artifacts!(new_os_credentials, self.os_credential_sets),
                                        handle_received_artifacts!(
                                            new_binding_service_creds,
                                            self.binding_service_cred_sets
                                        ),
                                        handle_received_artifacts!(
                                            new_non_binding_service_creds,
                                            self.non_binding_service_cred_sets
                                        ),
                                        handle_received_artifacts!(new_tokens, self.token_sets),
                                        // handle_received_artifacts!(new_tokens, self.token_sets),
                                        accepting,
                                    )),
                                    None,
                                )?;
                                automaton.add_edge(handle.unseal(), from_node, edge_data)?;
                                if let StateHandle::New(node_idx) = handle {
                                    // state seen for the first time, create an exploration job on the computes
                                    let node_data = automaton.get_node(node_idx)?;
                                    let depth = automaton.get_depth(node_idx);
                                    let config_arc = Arc::clone(&self.configuration);
                                    let topology_arc = Arc::clone(&self.topology);
                                    let report_channel = send_end.clone();
                                    self.computes.spawn(move || {
                                        explore_state(
                                            depth,
                                            node_idx,
                                            node_data,
                                            config_arc,
                                            topology_arc,
                                            report_channel,
                                        );
                                    });
                                    created_jobs += 1;
                                }
                            },
                        }
                    },
                    StateExplorationResult::Error(e) => {
                        // error reported, log
                        log::info!("State exploration returned an error: {}", e);
                    },
                    StateExplorationResult::TryAgain { acs, idx, new_depth } => {
                        // depth-repair allowed some previously too deep paths to be explored further
                        // create a new exploration job for them
                        let config_arc = Arc::clone(&self.configuration);
                        let topology_arc = Arc::clone(&self.topology);
                        let report_channel = send_end.clone();
                        self.computes.spawn(move || {
                            explore_state(
                                Option::from(new_depth),
                                idx,
                                acs,
                                config_arc,
                                topology_arc,
                                report_channel,
                            );
                        });
                        created_jobs += 1;
                    },
                }
            }
            // no report - do some maintenance: depth repair, but wait out its result
            let report_channel = send_end.clone();
            let automaton_arc: Arc<Mutex<ScenarioStateMachine>> = Arc::clone(&self.automaton);
            let configuration_arc = Arc::clone(&self.configuration);
            self.computes.install(move || match automaton_arc.lock() {
                Ok(mut automaton) => automaton.correct_depths(report_channel, configuration_arc.max_action_count()),
                Err(_e) => {
                    report_mpsc!(
                        StateExplorationResult::Error(PaganError::LockError("Poisoned automaton mutex".into())),
                        report_channel
                    )
                },
            });
            repair_counter = 0;
            if created_jobs == finished_jobs && receive_end.is_empty() {
                break; // nothing more to explore and all computes are vacant
            }
        }
        Ok(())
    }
}

fn split_services(
    service_store: &ServiceStore,
    enabler_store: &EnablerStore,
    services: HandleSet,
    by_privilege: bool,
    capability_specifier: CapabilitySpecifier,
    report_channel: &crossbeam::channel::Sender<StateExplorationResult>,
) -> HashMap<(PrivilegeLevel, ImpactRepr), HandleSet> {
    let mut res = HashMap::new();
    let all_services = service_store.handles();
    match capability_specifier {
        CapabilitySpecifier::None | CapabilitySpecifier::Action => {
            res.insert((PrivilegeLevel::None, 0), services);
        },
        CapabilitySpecifier::Service => {
            for (service, enabler) in services.as_iterator(Option::from(&all_services)) {
                let service_ref = service_store.get(service).unwrap();
                res.entry((
                    if by_privilege {
                        service_ref.get_effective_privilege()
                    } else {
                        PrivilegeLevel::None
                    },
                    service_ref.impacts(),
                ))
                .or_insert_with(|| HandleSet::ServicesMaybeEnablers(SetOption::None))
                .insert((service, enabler));
            }
        },
        CapabilitySpecifier::Enabler => {
            for (service, enabler) in services.as_iterator(Option::from(&all_services)) {
                let service_ref = service_store.get(service).unwrap();
                let enabler_ref = if enabler.is_some() {
                    enabler_store.get(enabler.unwrap()).unwrap()
                } else {
                    report_mpsc!(
                        StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                            "Enabler should be specifying impact, but is None".into()
                        )),
                        report_channel
                    );
                    continue;
                };
                res.entry((
                    if by_privilege {
                        service_ref.get_effective_privilege()
                    } else {
                        PrivilegeLevel::None
                    },
                    enabler_ref.impact(),
                ))
                .or_insert_with(|| HandleSet::ServicesMaybeEnablers(SetOption::None))
                .insert((service, enabler));
            }
        },
    };
    res
}

fn extend_with_new_tokens(
    old_tokens: &mut Cow<BTreeMap<Option<DeviceHandle>, FlagSet<Token>>>,
    given: FlagSet<Token>,
    target_device_handle: DeviceHandle,
    new_capabilities: &BTreeSet<AttackerCapability>,
    new_credentials: &BTreeSet<Cow<Credential>>,
    topology: &Topology,
    report_channel: &crossbeam::channel::Sender<StateExplorationResult>,
) {
    for ttype in Token::iter() {
        if given.contains(ttype) {
            match ttype {
                Token::Capability | Token::CapabilityAll | Token::Data | Token::ImpactDenialOfService => { // local tokens
                insert_token_if_missing!(ttype, Some(target_device_handle), old_tokens);
                }
                Token::TaintedSharedContent // global tokens
                | Token::InfectedRemovableMedia
                | Token::InternalPhishingMail
                | Token::MaliciousDomainConfigurationObject => {
                    insert_token_if_missing!(ttype, None, old_tokens);
                }
                //special bois
                Token::Credential => {
                    for credential in new_credentials
                        .iter()
                        .filter(|cred| !cred.r#type().is_disjoint(CredentialType::AnyPrimary))
                    {
                        for device_id in unwrap_iter_or_report_err!(credential
                            .devices()
                            .as_iterator(Some(&topology.device_store().handles())), report_channel)
                        {
                            insert_token_if_missing!(Token::Credential, Some(device_id), old_tokens);
                        }
                    }
                }
                Token::CredentialMfa => {
                    for credential in new_credentials
                        .iter()
                        .filter(|cred| !cred.r#type().is_disjoint(CredentialType::Mfa))
                    {
                        for device_id in unwrap_iter_or_report_err!(credential
                            .devices()
                            .as_iterator(Some(&topology.device_store().handles())), report_channel)
                        {
                            insert_token_if_missing!(Token::CredentialMfa, Some(device_id), old_tokens);
                        }
                    }
                }
                Token::Session => {
                    if new_capabilities.iter().find(|cap| cap.to() == CapabilityType::CodeExecution).is_some() { // we only get a session if we obtained code execution
                    insert_token_if_missing!(Token::Session, Some(target_device_handle), old_tokens);

                        // inherit flags such as containerization, usb access
                        let device = topology.device_store().get(target_device_handle).unwrap();
                        if device.r#virtual() {
                            insert_token_if_missing!(Token::Virtualized, Some(target_device_handle), old_tokens);
                        }
                        if !device.usb_access() {
                           insert_token_if_missing!(Token::NoUsbAccess, Some(target_device_handle), old_tokens);
                        }

                        // inherit accesses via shared filesys, remote media
                        for (destination, access_type) in topology.accesses().get(&target_device_handle).unwrap_or(&vec![]) {
                            match access_type {
                                AccessType::Media => {
                                    insert_token_if_missing!(Token::AccessViaRemovableMedia, Some(*destination), old_tokens);
                                }
                                AccessType::SharedFileSystem => {
                                    insert_token_if_missing!(Token::AccessViaSharedFilesys, Some(*destination), old_tokens);
                                }
                                _ => {}
                            }
                        }

                    }
                },
                Token::AccessViaNetwork => {
                    for (destination, access_type) in topology.accesses().get(&target_device_handle).unwrap_or(&vec![]) {
                        match access_type {
                            AccessType::Network |
                            AccessType::LocalHost => {
                                insert_token_if_missing!(Token::AccessViaNetwork, Some(*destination), old_tokens);
                            }
                            _ => {}
                        }
                    }
                }
                _ => {} // other tokens are either obtained via a session, or must be defined in parameters
            }
        }
    }
}

fn compute_new_states(
    target_device_handle: DeviceHandle,
    target_device: &Device,
    services: HandleSet,
    scheme: &UsageScheme,
    credentials: Option<Cow<'static, Credential>>,
    run_as: RunAs,
    state_idx: NodeIndex,
    state_data: &Arc<AdversaryControlState>,
    configuration: &Arc<ScenarioConfiguration>,
    topology: &Arc<Topology>,
    report_channel: &crossbeam::channel::Sender<StateExplorationResult>,
    action_id: ActionHandle,
    scheme_id: SchemeHandle,
) {
    if run_as == RunAs::Invalid {
        report_mpsc!(
            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                "Entered state computation with an invalid RunAS".into()
            )),
            report_channel
        );
        return;
    }
    // capabilities and credentials are exclusive for quiver techniques
    if scheme.grant.contains(Token::Capability) {
        //obtaining capabilities
        let split_services = split_services(
            configuration.service_store(),
            configuration.enabler_store(),
            services,
            scheme.privilege_origin == PrivilegeOrigin::Service,
            scheme.capability_specifier,
            report_channel,
        );

        for ((privilege, impacts), services) in split_services {
            let target_run_as = match scheme.privilege_origin {
                PrivilegeOrigin::None => {
                    report_mpsc!(
                        StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                            "Scheme grants capability but origin is not provided.".into()
                        )),
                        report_channel
                    );
                    continue;
                },
                PrivilegeOrigin::Service => match &privilege {
                    PrivilegeLevel::None => {
                        report_mpsc!(
                            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                "Privileges originate from services, but a batch runs under None".into()
                            )),
                            report_channel
                        );
                        continue;
                    },
                    PrivilegeLevel::Service => {
                        handle_must_be_runas!(
                            target_device.service_account(),
                            target_device_handle,
                            report_channel,
                            continue
                        )
                    },
                    PrivilegeLevel::User => {
                        handle_maybe_runas!(target_device.get_random_user_account(), report_channel, continue)
                    },
                    PrivilegeLevel::Administrator => {
                        handle_maybe_runas!(target_device.get_random_admin_account(), report_channel, continue)
                    },
                    PrivilegeLevel::LocalSystem => {
                        handle_must_be_runas!(
                            target_device.system_account(),
                            target_device_handle,
                            report_channel,
                            continue
                        )
                    },
                },
                PrivilegeOrigin::Credential => match &credentials {
                    None => {
                        report_mpsc!(
                            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                "Privilege origin should be from a credential but non was provided".into()
                            )),
                            report_channel
                        );
                        continue;
                    },
                    Some(cred) => match (*cred).borrow() {
                        Credential::System(sc) => match sc
                            .accounts()
                            .iter()
                            .filter(|acc| acc.is_for_device(target_device_handle))
                            .max_by_key(|acc| acc.strength(None))  // using global best - should suffice as the probability of a better local account for a credential is miserable
                        {
                            None => {
                                report_mpsc!(StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                    "No suitable account found for the credential".into()
                                )), report_channel);
                                continue;
                            }
                            // YEEEEEEEEEEEEEE HAW #4
                            Some(acc) => RunAs::Account(unsafe {std::mem::transmute::<&Account, &'static Account>(*acc)}),
                        },
                        _ => {
                            report_mpsc!(
                                StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                    "Wildcard and service credential can not provide privileges".into()
                                )),
                                report_channel
                            );
                            continue;
                        },
                    },
                },
                PrivilegeOrigin::RandomUser => {
                    handle_maybe_runas!(target_device.get_random_interactive_account(), report_channel, continue)
                },
                PrivilegeOrigin::InheritUser => match run_as {
                    RunAs::Account(_) => run_as,
                    RunAs::AccountHandle(handle) => {
                        // let reference: &'static Account = topology.handle_to_account_ref(handle).unwrap();
                        RunAs::Account(topology.handle_to_account_ref(handle).unwrap())
                    },
                    RunAs::Privilege(_) => {
                        report_mpsc!(
                            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                "Can not inherit from privilege level, needs account!".into()
                            )),
                            report_channel
                        );
                        continue;
                    },
                    RunAs::Invalid => continue,
                },
                PrivilegeOrigin::NewUser => match run_as {
                    RunAs::Invalid => {
                        report_mpsc!(
                            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                "Should inherit an the actual accounts' capabilities, but it is undefined. ".into()
                            )),
                            report_channel
                        );
                        continue;
                    },
                    RunAs::Account(acc) => match acc.capabilities().get(&target_device_handle) {
                        None => {
                            report_mpsc!(
                                StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                    "The account to inherit from has no capabilities on the target device".into()
                                )),
                                report_channel
                            );
                            continue;
                        },
                        Some(caps) => match caps.iter().find(|cap| cap.to() == CapabilityType::CodeExecution) {
                            None => {
                                report_mpsc!(
                                    StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                        "The account to inherit from has no code execution on the target device".into()
                                    )),
                                    report_channel
                                );
                                continue;
                            },
                            Some(cap) => RunAs::Privilege(cap.privilege_level()),
                        },
                    },
                    RunAs::AccountHandle(handle) => {
                        RunAs::Privilege(privilege_on_device!(topology, handle, target_device_handle))
                    },
                    RunAs::Privilege(_) => run_as,
                },
                PrivilegeOrigin::ElevatedUser => match run_as {
                    RunAs::Privilege(_) => {
                        report_mpsc!(
                            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                "Should inherit an the actual accounts' capabilities, but it is undefined.".into()
                            )),
                            report_channel
                        );
                        continue;
                    },
                    RunAs::AccountHandle(old_acc) => RunAs::Account(
                        topology
                            .handle_to_account_ref(topology.account_store().uac_elevate(old_acc))
                            .unwrap(),
                    ),
                    RunAs::Account(acc) => RunAs::Account(
                        topology
                            .handle_to_account_ref(
                                topology
                                    .account_store()
                                    .uac_elevate(topology.account_store().get_handle(acc).unwrap()),
                            )
                            .unwrap(),
                    ),
                    RunAs::Invalid => continue,
                },
                PrivilegeOrigin::Fixed(privilege) => match privilege {
                    PrivilegeLevel::None => {
                        report_mpsc!(
                            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                "Fixed privilege origin can not be None".into()
                            )),
                            report_channel
                        );
                        continue;
                    },
                    PrivilegeLevel::Service => {
                        handle_must_be_runas!(
                            target_device.service_account(),
                            target_device_handle,
                            report_channel,
                            continue
                        )
                    },
                    PrivilegeLevel::User => {
                        handle_maybe_runas!(target_device.get_random_user_account(), report_channel, continue)
                    },
                    PrivilegeLevel::Administrator => {
                        handle_maybe_runas!(target_device.get_random_admin_account(), report_channel, continue)
                    },
                    PrivilegeLevel::LocalSystem => {
                        handle_must_be_runas!(
                            target_device.system_account(),
                            target_device_handle,
                            report_channel,
                            continue
                        )
                    },
                },
            };

            let mut new_capabilities = match target_run_as.inherit_capabilities(target_device_handle) {
                Ok(v) => v,
                Err(e) => {
                    report_mpsc!(StateExplorationResult::Error(e), report_channel);
                    continue;
                },
            };
            if !scheme.grant.contains(Token::CapabilityAll) {
                match &scheme.capability_specifier {
                    CapabilitySpecifier::Service => {
                        let s_impacts = primitive_to_flagset!(impacts, ServiceImpact, report_channel);
                        let allowed = service_impacts_as_capabilities(s_impacts);
                        new_capabilities.retain(|acap| allowed.contains(&acap.to()))
                    },
                    CapabilitySpecifier::Enabler => {
                        let e_impacts = primitive_to_flagset!(impacts, EnablerImpact, report_channel);
                        let allowed = enabler_impacts_as_capabilities(e_impacts);
                        new_capabilities.retain(|acap| allowed.contains(&acap.to()))
                    },
                    CapabilitySpecifier::Action => {
                        for template in scheme.capabilities_provided.iter() {
                            new_capabilities.insert(AttackerCapability::new(Capability::new(
                                template.to(),
                                match &target_run_as {
                                    RunAs::Account(acc) => {
                                        privilege_on_device!(
                                            topology,
                                            topology.account_store().get_handle(acc).unwrap(),
                                            target_device_handle
                                        )
                                    },
                                    RunAs::AccountHandle(handle) => {
                                        privilege_on_device!(topology, handle.to_owned(), target_device_handle)
                                    },
                                    RunAs::Privilege(privilege) => privilege.to_owned(),
                                    RunAs::Invalid => continue,
                                },
                                target_device_handle,
                            )));
                        }
                    },
                    _ => {},
                }
            }

            let mut new_state_tokens = state_data.attacker_tokens();
            extend_with_new_tokens(
                &mut new_state_tokens,
                scheme.given,
                target_device_handle,
                &new_capabilities,
                &BTreeSet::new(),
                topology,
                report_channel,
            );

            let mut new_state_capabilities = state_data.attacker_capabilities();
            if !new_state_capabilities.is_superset(&new_capabilities) {
                new_state_capabilities.to_mut().extend(new_capabilities);
            }; // this could be done without superset checking, but that would mean a copy of the state capabilities even if there are no changes

            let edge_data = Box::new(EdgeData::new(
                action_id,
                scheme_id,
                handleize_runas!(run_as, topology, report_channel, state_data),
                services,
                match &credentials {
                    None => None,
                    Some(cred_cow) => match cred_cow {
                        Cow::Borrowed(referenced) => Some(Cow::Borrowed(*referenced)),
                        Cow::Owned(owned) => match owned {
                            Credential::SplitMultiCred { primary, mfa } => {
                                Some(Cow::Owned(Credential::SplitMultiCred {
                                    primary: primary.make_borrowed(),
                                    mfa:     mfa.make_borrowed(),
                                }))
                            },
                            _ => {
                                report_mpsc!(
                                    StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                        "Used credential can not be owned when not of type Credential::SplitMultiCred."
                                            .into()
                                    )),
                                    report_channel
                                );
                                continue;
                            },
                        },
                    },
                },
            ));

            report_mpsc!(
                StateExplorationResult::SubResult {
                    edge_data,
                    accepting: configuration.goal().conforming(
                        action_id,
                        target_device_handle,
                        new_state_tokens
                            .get(&Some(target_device_handle))
                            .map_or(Token::none(), |val| *val),
                        &new_state_capabilities
                    ),
                    new_device: target_device_handle,
                    new_tokens: new_state_tokens,
                    new_capabilities: new_state_capabilities,
                    new_os_credentials: state_data.available_os_credentials(),
                    new_binding_service_creds: state_data.available_binding_service_credentials(),
                    new_non_binding_service_creds: state_data.available_non_binding_service_credentials(),
                    from_node: state_idx,
                },
                report_channel
            );
        }
    } else if scheme.grant.contains(Token::Credential) || scheme.grant.contains(Token::CredentialMfa) {
        // obtaining credentials
        let mut new_os_credentials = state_data.available_os_credentials();
        let mut new_non_binding_service_credentials = state_data.available_non_binding_service_credentials();
        let mut new_binding_service_credentials = state_data.available_binding_service_credentials();
        let device = topology.device_store().get(target_device_handle).unwrap();
        match scheme.credential_origin {
            CredentialOrigin::None => {
                report_mpsc!(
                    StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                        "Usage grants credential but origin is not specified.".into()
                    )),
                    report_channel
                );
                return;
            },
            CredentialOrigin::OsAllLocal => {
                extend_credentials_if_missing!(
                    device
                        .accounts()
                        .iter()
                        .filter(|acc| {
                            if device.roles().contains(&DeviceRole::DomainController) {
                                true
                            } else {
                                !acc.domain_account()
                            }
                        })
                        .flat_map(|acc| {
                            acc.credentials()
                                .iter()
                                .filter(|cred| cred.r#type().contains(CredentialType::Os))
                        })
                        .map(|cred| Cow::Borrowed(*cred)),
                    new_os_credentials
                );
            },
            CredentialOrigin::OsCached => {
                extend_credentials_if_missing!(
                    device
                        .get_associated_credentials()
                        .iter()
                        .filter(|cred| {
                            cred.r#type().contains(CredentialType::Os) && cred.is_for_device(target_device_handle)
                        })
                        .map(|cred| Cow::Borrowed(*cred)),
                    new_os_credentials
                );
            },
            CredentialOrigin::OsActiveAccount => {
                match run_as {
                    RunAs::Account(acc) => extend_credentials_if_missing!(
                        acc.credentials()
                            .iter()
                            .filter(|cred| {
                                cred.r#type().contains(CredentialType::Os) && cred.is_for_device(target_device_handle)
                            })
                            .map(|cred| Cow::Borrowed(*cred)),
                        new_os_credentials
                    ),
                    RunAs::AccountHandle(handle) => extend_credentials_if_missing!(
                        topology
                            .handle_to_account_ref(handle)
                            .unwrap()
                            .credentials()
                            .iter()
                            .filter(|cred| {
                                cred.r#type().contains(CredentialType::Os) && cred.is_for_device(target_device_handle)
                            })
                            .map(|cred| Cow::Borrowed(*cred)),
                        new_os_credentials
                    ),
                    RunAs::Privilege(privilege) => {
                        match privilege {
                            PrivilegeLevel::Service => {
                                match handle_must_be_runas!(
                                    device.service_account(),
                                    target_device_handle,
                                    report_channel,
                                    RunAs::Invalid
                                ) {
                                    RunAs::Account(acc) => extend_credentials_if_missing!(
                                        acc.credentials().iter().map(|cred| Cow::Borrowed(*cred)),
                                        new_os_credentials
                                    ),
                                    _ => {}, // cant be other
                                }
                            },
                            PrivilegeLevel::User => {
                                match handle_maybe_runas!(
                                    device.get_random_user_account(),
                                    report_channel,
                                    RunAs::Invalid
                                ) {
                                    RunAs::Account(acc) => extend_credentials_if_missing!(
                                        acc.credentials().iter().map(|cred| Cow::Borrowed(*cred)),
                                        new_os_credentials
                                    ),
                                    _ => {}, // cant be other
                                }
                            },
                            PrivilegeLevel::Administrator => {
                                match handle_maybe_runas!(
                                    device.get_random_admin_account(),
                                    report_channel,
                                    RunAs::Invalid
                                ) {
                                    RunAs::Account(acc) => extend_credentials_if_missing!(
                                        acc.credentials().iter().map(|cred| Cow::Borrowed(*cred)),
                                        new_os_credentials
                                    ),
                                    _ => {}, // cant be other
                                }
                            },
                            _ => {},
                        }
                    },
                    RunAs::Invalid => {},
                }
            },
            CredentialOrigin::ServiceSelf => insert_credential_if_missing!(
                Cow::Owned(Credential::Service(
                    ServiceCredential::new(
                        HandleSet::Services(services.just_services()),
                        CredentialType::AppPrimary.into(),
                        None,
                        true,
                    )
                    .unwrap(),
                )),
                new_binding_service_credentials
            ),
            CredentialOrigin::ServiceSelfAll => {
                insert_credential_if_missing!(
                    Cow::Owned(Credential::Service(
                        ServiceCredential::new(
                            HandleSet::Services(services.just_services()),
                            if scheme.grant.contains(Token::CredentialMfa) {
                                CredentialType::AppAny.into()
                            } else {
                                CredentialType::AppPrimary.into()
                            },
                            None,
                            true,
                        )
                        .unwrap()
                    )),
                    new_binding_service_credentials
                )
            },
            CredentialOrigin::ServiceOther => if scheme.grant.contains(Token::CredentialMfa) {},
            CredentialOrigin::Mimic => if scheme.grant.contains(Token::CredentialMfa) {},
            CredentialOrigin::AnyActualUser => {
                // for the holder of the actual account update non-bindings with the services
            },
            CredentialOrigin::AnyPresentUsers => {
                // for all people who could touch the device, update non-bindings with all services
            },
            CredentialOrigin::NewUser => {
                // nothing to do tbh, we do not get anything of a new user - we might delete this entirely
            },
        }
    }
}

/**
Process a state while restricting the device that follows it.
**/
fn explore_state_for_device(
    target_device_handle: DeviceHandle,
    state_idx: NodeIndex,
    state_data: &Arc<AdversaryControlState>,
    configuration: &Arc<ScenarioConfiguration>,
    topology: &Arc<Topology>,
    report_channel: &crossbeam::channel::Sender<StateExplorationResult>,
) {
    let attacker_caps = state_data.attacker_capabilities();
    let os_creds = state_data.available_os_credentials();
    let binding_creds = state_data.available_binding_service_credentials();
    let non_binding_creds = state_data.available_non_binding_service_credentials();
    let satchel = ArtifactSatchel::with_filter(
        state_data.attacker_tokens().as_ref(),
        &attacker_caps,
        target_device_handle,
    );

    let available_actions = configuration.action_store().get_applicable_actions(&satchel);
    for (action_idx, scheme_idx, scheme) in available_actions.iter() {
        let (user_id, preferred_capabilities) = match satchel.compute_best_capabilities(scheme) {
            Ok((user_id, caps)) => (user_id, caps),
            Err(e) => {
                report_mpsc!(StateExplorationResult::Error(e), report_channel);
                continue;
            },
        };
        if matches!(&scheme.capabilities_required, Predicate::Empty) && preferred_capabilities.is_empty() {
            continue; // we don't have the required caps on a single account, can't proceed
        }
        let applicability_rules = configuration
            .applicability_matrix()
            .get(action_idx)
            .unwrap_or(&HandleSet::Services(SetOption::None));
        let mut services = match &scheme.enabler_required {
            None => {
                // scheme does not require enablers, we just ignore them if present
                applicability_rules.to_owned()
            },
            Some(template) => {
                // enabler is needed, filter only pairs that are applicable
                match applicability_rules {
                    HandleSet::Services(_) => {
                        // we can't have only services for this action
                        report_mpsc!(
                            StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                "Scheme requires an enabler but the applicability matrix only contains services."
                                    .into(),
                            )),
                            report_channel
                        );
                        HandleSet::Services(SetOption::None)
                    },
                    _ => {
                        let mut tmp = applicability_rules.to_owned();
                        match tmp.retain_if(|_, enabler| {
                            enabler.is_some()
                                && match configuration.enabler_store().get(*enabler.unwrap()) {
                                    Ok(enabler) => enabler.matches_template(template),
                                    Err(e) => {
                                        report_mpsc!(StateExplorationResult::Error(e), report_channel);
                                        false
                                    },
                                }
                        }) {
                            Ok(_) => tmp,
                            Err(e) => {
                                report_mpsc!(
                                    StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                                        format!("Could not filter service enabler pairs for required enabler: {e}.")
                                            .into()
                                    )),
                                    report_channel
                                );
                                HandleSet::Services(SetOption::None)
                            },
                        }
                    },
                }
            },
        };
        let target_device = topology.device_store().get(target_device_handle).unwrap(); // can unwrap: device ids come from the tree
        services
            .difference(
                target_device.prohibited_software(),
                Some(&configuration.service_store().handles()),
            )
            .unwrap(); // remove possibilities prohibited by the devices' role
        if services.is_empty() {
            return; // no usable service was found, we can't continue
        }
        let (preferred_credential_local, preferred_credential_global) = match scheme.given.contains(Token::Credential) {
            false => (None, None),
            true => {
                let all_creds = if scheme.privilege_origin == PrivilegeOrigin::Credential {
                    os_creds.iter().collect::<BTreeSet<_>>()
                } else {
                    os_creds
                        .iter()
                        .chain(binding_creds.iter())
                        .chain(non_binding_creds.iter().map(|(_, cred)| cred))
                        .collect::<BTreeSet<_>>()
                };
                let (mut local, mut global) = Credential::select_strongest(
                    all_creds,
                    state_data.device(),
                    &services,
                    scheme.privilege_origin == PrivilegeOrigin::Credential,
                    scheme.given.contains(Token::CredentialMfa),
                );
                if global.is_none() {
                    global = local.as_ref().map(|item| item.make_borrowed());
                }
                if local.is_none() || global.is_none() {
                    // realistically, either both are found or none of them
                    return; // credential needed but no appropriate found, we can't continue
                }
                (local, global)
            },
        };
        if &preferred_credential_global != &preferred_credential_local {
            let mut credential_filtered_services = services.clone();
            credential_filtered_services
                .intersection(
                    preferred_credential_global
                        .as_ref()
                        .unwrap()
                        .services(scheme.given.contains(Token::CredentialMfa))
                        .borrow(),
                )
                .unwrap();
            // only explore both if they are not equal
            compute_new_states(
                target_device_handle,
                target_device,
                credential_filtered_services,
                scheme,
                preferred_credential_global,
                user_id,
                state_idx,
                state_data,
                configuration,
                topology,
                report_channel,
                action_idx.to_owned(),
                scheme_idx.to_owned(),
            );
        }
        if preferred_credential_local.is_some() {
            services
                .intersection(
                    preferred_credential_local
                        .as_ref()
                        .unwrap()
                        .services(scheme.given.contains(Token::CredentialMfa))
                        .borrow(),
                )
                .unwrap();
        }
        compute_new_states(
            target_device_handle,
            target_device,
            services,
            scheme,
            preferred_credential_local,
            user_id,
            state_idx,
            state_data,
            configuration,
            topology,
            report_channel,
            action_idx.to_owned(),
            scheme_idx.to_owned(),
        );
    }
}

/**
A state exploration job. Spawned on the computes.
**/
fn explore_state(
    depth: Option<usize>,
    state_idx: NodeIndex,
    state_data: Arc<AdversaryControlState>,
    configuration: Arc<ScenarioConfiguration>,
    topology: Arc<Topology>,
    report_channel: crossbeam::channel::Sender<StateExplorationResult>,
) {
    if depth.is_none() {
        report_mpsc!(
            StateExplorationResult::Error(PaganError::TopologyError(
                "Depth must be set before processing state.".into(),
            )),
            report_channel
        );
        report_mpsc!(StateExplorationResult::Done, report_channel);
        return;
    }
    if !state_data.accepting_state() && depth.unwrap() <= configuration.max_action_count() {
        let successors_device = match topology.accesses().get(&state_data.device()) {
            None => {
                report_mpsc!(
                    StateExplorationResult::Error(PaganError::TopologyError("Invalid DeviceHandle".into(),)),
                    report_channel
                );
                report_mpsc!(StateExplorationResult::Done, report_channel);
                return;
            },
            Some(elems) => elems.iter().map(|item| item.0),
        }; // query all possible devices that might follow on the attack paths

        for target_device in successors_device {
            explore_state_for_device(
                target_device,
                state_idx,
                &state_data,
                &configuration,
                &topology,
                &report_channel,
            );
        }
    }
    report_mpsc!(StateExplorationResult::Done, report_channel);
}
