use std::{
    borrow::Cow,
    collections::{BTreeMap, BTreeSet, HashMap, VecDeque},
    pin::Pin,
    sync::{Arc},
};

use backend::{
    error::PaganError,
    primitives::{
        capabilities::AttackerCapability,
        credentials::Credential,
        person::Person,
        shared::{enums::Token, types::*, utils::HandleSet},
    },
};
use flagset::FlagSet;
use petgraph::{
    graph::{DiGraph, EdgeIndex, NodeIndex},
    visit::IntoNeighbors,
};

use crate::{report_mpsc, scenario::builder::StateExplorationResult};

macro_rules! set_depth_on_data {
    ($data:ident, $depth:expr) => {
        match $data.depth {
            None => {
                $data.set_depth(Option::from($depth));
                true
            },
            Some(old_depth) if old_depth > $depth => {
                $data.set_depth(Option::from($depth));
                true
            },
            Some(_) => false,
        }
    };
}


#[derive(PartialEq, Eq, Hash)]
pub struct AdversaryControlState {
    device: DeviceHandle,
    attacker_capabilities: &'static BTreeSet<AttackerCapability>,
    available_os_credentials: &'static BTreeSet<Cow<'static, Credential>>,
    available_binding_service_credentials: &'static BTreeSet<Cow<'static, Credential>>,
    available_non_binding_service_credentials: &'static BTreeMap<Option<&'static Person>, Cow<'static, Credential>>,
    attacker_tokens: &'static BTreeMap<Option<DeviceHandle>, FlagSet<Token>>,
    accepting_state: bool,
}

impl AdversaryControlState {
    pub fn new(
        device: DeviceHandle,
        attacker_capabilities: &'static BTreeSet<AttackerCapability>,
        available_os_credentials: &'static BTreeSet<Cow<'static, Credential>>,
        available_binding_service_credentials: &'static BTreeSet<Cow<Credential>>,
        available_non_binding_service_credentials: &'static BTreeMap<Option<&'static Person>, Cow<'static, Credential>>,
        attacker_tokens: &'static BTreeMap<Option<DeviceHandle>, FlagSet<Token>>,
        accepting_state: bool,
    ) -> Self {
        Self {
            device,
            attacker_capabilities,
            available_os_credentials,
            available_binding_service_credentials,
            available_non_binding_service_credentials,
            attacker_tokens,
            accepting_state,
        }
    }

    pub fn device(&self) -> DeviceHandle { self.device }

    pub fn attacker_capabilities(&self) -> Cow<'static, BTreeSet<AttackerCapability>> {
        Cow::Borrowed(&self.attacker_capabilities)
    }

    pub fn attacker_tokens(&self) -> Cow<'static, BTreeMap<Option<DeviceHandle>, FlagSet<Token>>> {
        Cow::Borrowed(self.attacker_tokens)
    }

    pub fn accepting_state(&self) -> bool { self.accepting_state }

    pub fn available_os_credentials(&self) -> Cow<'static, BTreeSet<Cow<'static, Credential>>> {
        Cow::Borrowed(self.available_os_credentials)
    }

    pub fn available_binding_service_credentials(&self) -> Cow<'static, BTreeSet<Cow<'static, Credential>>> {
        Cow::Borrowed(self.available_binding_service_credentials)
    }

    pub fn available_non_binding_service_credentials(
        &self,
    ) -> Cow<'static, BTreeMap<Option<&'static Person>, Cow<'static, Credential>>> {
        Cow::Borrowed(self.available_non_binding_service_credentials)
    }
}


pub struct EdgeData {
    action:                ActionHandle,
    scheme_index:          SchemeHandle,
    runas:                 RunAs,
    services_and_enablers: HandleSet,
    used_credentials:      Option<Cow<'static, Credential>>,
}

impl EdgeData {
    pub fn new(
        action: ActionHandle,
        scheme_index: SchemeHandle,
        runas: RunAs,
        services_and_enablers: HandleSet,
        used_credentials: Option<Cow<'static, Credential>>,
    ) -> Self {
        Self {
            action,
            scheme_index,
            runas,
            services_and_enablers,
            used_credentials,
        }
    }

    pub fn action(&self) -> ActionHandle { self.action }

    pub fn scheme_index(&self) -> SchemeHandle { self.scheme_index }

    pub fn runas(&self) -> &RunAs { &self.runas }

    pub fn services_and_enablers(&self) -> &HandleSet { &self.services_and_enablers }
}

pub enum StateHandle {
    New(NodeIndex),
    Existing(NodeIndex),
}

impl StateHandle {
    pub fn unseal(&self) -> NodeIndex {
        match self {
            StateHandle::New(inner) | StateHandle::Existing(inner) => inner.to_owned(),
        }
    }
}

struct VertexData {
    acs:   Arc<AdversaryControlState>,
    depth: Option<usize>,
}

impl VertexData {
    pub fn new(acs: Arc<AdversaryControlState>, depth: Option<usize>) -> Self { Self { acs, depth } }

    pub fn acs(&self) -> Arc<AdversaryControlState> { Arc::clone(&self.acs) }

    pub fn depth(&self) -> Option<usize> { self.depth }

    pub fn set_depth(&mut self, depth: Option<usize>) { self.depth = depth; }
}

#[derive(Default)]
pub struct ScenarioStateMachine {
    graph:                DiGraph<VertexData, Pin<Box<EdgeData>>>,
    reverse_data_map:     HashMap<Arc<AdversaryControlState>, NodeIndex>,
    nodes_for_correction: VecDeque<NodeIndex>,
}

impl ScenarioStateMachine {
    pub(crate) const ROOT: u32 = 0;

    /**
    Add a new vertex with the passed data or return the node with the same data if it exists.
    **/
    pub(crate) fn add_vertex(
        &mut self,
        data: Arc<AdversaryControlState>,
        depth: Option<usize>,
    ) -> PaganResult<StateHandle> {
        Ok(match self.reverse_data_map.get_key_value(&data) {
            None => {
                let idx = self.graph.add_node(VertexData::new(Arc::clone(&data), depth));
                self.reverse_data_map.insert(data, idx);
                StateHandle::New(idx)
            },
            Some((_, node_handle)) => StateHandle::Existing(node_handle.to_owned()),
        })
    }

    /**
    Connect two vertices, and mark the vertex for depth correction if needed.
    **/
    pub fn add_edge(&mut self, from: NodeIndex, to: NodeIndex, data: Box<EdgeData>) -> PaganResult<EdgeIndex> {
        let res = Ok(self.graph.add_edge(from, to, Box::into_pin(data)));
        let depth = self.get_depth(from).unwrap();
        if self.set_depth(to, depth + 1) {
            self.nodes_for_correction.push_back(to);
        }
        res
    }

    pub fn get_node(&self, idx: NodeIndex) -> PaganResult<Arc<AdversaryControlState>> {
        match self.graph.node_weight(idx) {
            None => Err(PaganError::AutomatonBuildingError("invalid node idx in query".into())),
            Some(data) => Ok(data.acs()),
        }
    }

    pub fn get_edge(&self, idx: EdgeIndex) -> PaganResult<&EdgeData> {
        match self.graph.edge_weight(idx) {
            None => Err(PaganError::AutomatonBuildingError("invalid edge idx in query".into())),
            Some(data) => Ok(data.as_ref().get_ref()),
        }
    }

    pub fn get_depth(&self, idx: NodeIndex) -> Option<usize> {
        self.graph.node_weight(idx).and_then(|data| data.depth())
    }

    pub fn correct_depths(
        &mut self,
        report_channel: crossbeam::channel::Sender<StateExplorationResult>,
        depth_limit: usize,
    ) {
        while let Some(node_idx) = self.nodes_for_correction.pop_front() {
            let depth = match self.get_depth(node_idx) {
                None => {
                    report_mpsc!(
                        StateExplorationResult::Error(PaganError::AutomatonBuildingError(
                            "Depth correction called on node with no depth".into(),
                        )),
                        report_channel
                    );
                    return;
                },
                Some(val) => val,
            };

            self.correct_subtree(node_idx, depth, &report_channel, depth_limit);
            report_mpsc!(StateExplorationResult::Done, report_channel);
        }
    }

    fn correct_subtree(
        &mut self,
        from_node: NodeIndex,
        from_node_depth: usize,
        report_channel: &crossbeam::channel::Sender<StateExplorationResult>,
        depth_limit: usize,
    ) {
        if let None = self.graph.neighbors(from_node).next() {
            // if we are at a leaf vertex
            if let Some(data) = self.graph.node_weight(from_node) {
                match data.depth {
                    Some(depth) if depth < depth_limit => {
                        // and the depth is not at the limit
                        if !data.acs.accepting_state {
                            // and it's not accepting
                            report_mpsc!(
                                StateExplorationResult::TryAgain {
                                    acs:       Arc::clone(&data.acs),
                                    idx:       from_node,
                                    new_depth: depth,
                                },
                                report_channel
                            ); // report for further exploration
                        }
                    },
                    _ => {},
                }
            }
        };
        let neighbors = self.graph.neighbors(from_node).collect::<Vec<_>>();
        for node in neighbors {
            if self.set_depth(node, from_node_depth + 1) {
                self.correct_subtree(node, from_node_depth + 1, report_channel, depth_limit);
            }
        }
    }

    fn set_depth(&mut self, index: NodeIndex, depth: usize) -> bool {
        self.graph
            .node_weight_mut(index)
            .map_or_else(|| false, |data| set_depth_on_data!(data, depth))
    }
}
