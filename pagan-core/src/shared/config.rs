use std::{
    borrow::Cow,
    collections::{BTreeMap, BTreeSet},
    pin::Pin,
};

use backend::{
    primitives::{
        capabilities::AttackerCapability,
        credentials::Credential,
        predicate::Predicate,
        shared::{
            enums::Token,
            types::{ActionHandle, DeviceHandle},
            utils::ApplicabilityMatrix,
        },
    },
    stores::{ActionStore, EnablerStore, ServiceStore},
};
use flagset::FlagSet;

pub struct Goal {
    device:    DeviceHandle,
    action:    ActionHandle,
    specifier: Predicate,
}

impl Goal {
    pub fn new(device: DeviceHandle, action: ActionHandle, specifier: Predicate) -> Self {
        Self {
            device,
            action,
            specifier,
        }
    }

    pub fn device(&self) -> DeviceHandle { self.device }

    pub fn action(&self) -> ActionHandle { self.action }

    pub fn specifier(&self) -> &Predicate { &self.specifier }

    pub fn conforming(
        &self,
        action: ActionHandle,
        device: DeviceHandle,
        tokens: FlagSet<Token>,
        capabilities: &BTreeSet<AttackerCapability>,
    ) -> bool {
        if self.action != action {
            return false;
        }
        if self.device != device {
            return false;
        }
        self.specifier.eval_state(
            tokens,
            capabilities.iter().collect::<Vec<&AttackerCapability>>().as_slice(),
        )
    }
}

pub struct ScenarioConfiguration {
    service_store: ServiceStore,
    enabler_store: EnablerStore,
    action_store:  ActionStore,

    applicability_matrix: ApplicabilityMatrix,

    // @Starting artifacts
    available_tokens:      BTreeMap<Option<DeviceHandle>, FlagSet<Token>>,
    available_credentials: BTreeSet<Cow<'static, Credential>>,

    goal: Goal,

    max_devices_touched: i32,
    max_action_count:    usize,
}

impl ScenarioConfiguration {
    pub fn new(
        service_store: ServiceStore,
        enabler_store: EnablerStore,
        action_store: ActionStore,
        applicability_matrix: ApplicabilityMatrix,
        available_tokens: BTreeMap<Option<DeviceHandle>, FlagSet<Token>>,
        available_credentials: BTreeSet<Cow<'static, Credential>>,
        goal: Goal,
        max_devices_touched: i32,
        max_action_count: usize,
    ) -> Self {
        Self {
            service_store,
            enabler_store,
            action_store,
            applicability_matrix,
            available_tokens,
            available_credentials,
            goal,
            max_devices_touched,
            max_action_count,
        }
    }

    pub fn service_store(&self) -> &ServiceStore { &self.service_store }

    pub fn enabler_store(&self) -> &EnablerStore { &self.enabler_store }

    // pub fn device_store(&self) -> &DeviceStore {
    //     &self.device_store
    // }
    pub fn action_store(&self) -> &ActionStore { &self.action_store }

    pub fn available_tokens(&self) -> &BTreeMap<Option<DeviceHandle>, FlagSet<Token>> { &self.available_tokens }

    pub fn available_credentials(&self) -> &BTreeSet<Cow<'static, Credential>> { &self.available_credentials }

    pub fn goal(&self) -> &Goal { &self.goal }

    pub fn max_devices_touched(&self) -> i32 { self.max_devices_touched }

    pub fn max_action_count(&self) -> usize { self.max_action_count }

    pub fn applicability_matrix(&self) -> &ApplicabilityMatrix { &self.applicability_matrix }
}
