use std::{collections::HashMap, hash::Hash, pin::Pin};


#[derive(Debug)]
pub(crate) struct CustomHashSetWithImmutableAndImmovableElements<T: Hash + Eq> {
    base: HashMap<Pin<Box<T>>, ()>,
}

impl<T: Hash + Eq> Default for CustomHashSetWithImmutableAndImmovableElements<T> {
    fn default() -> Self { Self { base: HashMap::new() } }
}

impl<'lf, T: Hash + Eq> CustomHashSetWithImmutableAndImmovableElements<T> {
    pub fn insert(&mut self, value: T) -> bool { self.base.insert(Box::pin(value), ()).is_none() }

    // pub fn get(&self, query: &T) -> Option<&T> { self.base.get_key_value(query).and_then(|(k, _v)| Some(k)) }

    pub fn get_or_insert<'f>(&'f mut self, value: T) -> &'lf T {
        let pinned = Box::pin(value);
        // YEEEEEEEEEEEE  HAW # 1
        unsafe {
            std::mem::transmute::<&'f T, &'lf T>(
                &*self
                    .base
                    .raw_entry_mut()
                    .from_key(&pinned)
                    .or_insert(pinned, ())
                    .0
                    .as_ref()
                    .get_ref(),
            )
        }
    }
}
