#![allow(clippy::module_name_repetitions)]
use std::{
    self,
    borrow::Cow,
    fmt::{Debug, Display, Formatter},
    sync,
};


pub enum PaganError {
    StrumParse(strum::ParseError),
    StopIteration,
    InvalidBits(flagset::InvalidBits),
    MalformedPredicate,
    InvalidPortNumber,
    NoSuchPrimaryKey(Cow<'static, str>),
    AutomatonBuildingError(Cow<'static, str>),
    TopologyError(Cow<'static, str>),
    ThreadError(rayon::ThreadPoolBuildError),
    InvalidCallError(Cow<'static, str>),
    LockError(Cow<'static, str>),
}

impl From<strum::ParseError> for PaganError {
    fn from(value: strum::ParseError) -> Self { Self::StrumParse(value) }
}

impl From<flagset::InvalidBits> for PaganError {
    fn from(value: flagset::InvalidBits) -> Self { Self::InvalidBits(value) }
}

impl From<rayon::ThreadPoolBuildError> for PaganError {
    fn from(value: rayon::ThreadPoolBuildError) -> Self { Self::ThreadError(value) }
}

impl<T> From<sync::PoisonError<T>> for PaganError {
    fn from(value: sync::PoisonError<T>) -> Self { Self::LockError(value.to_string().into()) }
}

impl Display for PaganError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::StrumParse(e) => {
                write!(f, "{e}")
            },
            Self::StopIteration => {
                write!(f, "Stop the Iteration")
            },
            Self::InvalidBits(e) => {
                write!(f, "{e}")
            },
            Self::MalformedPredicate => {
                write!(f, "The predicate is invalid")
            },
            Self::InvalidPortNumber => {
                write!(f, "The port number is invalid")
            },
            Self::NoSuchPrimaryKey(reason) => {
                write!(f, "Primary key not present; {reason}")
            },
            Self::AutomatonBuildingError(reason) => {
                write!(f, "Automaton building failed; {reason}")
            },
            Self::TopologyError(reason) => {
                write!(f, "Topology inconsistency; {reason}")
            },
            Self::ThreadError(e) => {
                write!(f, "{e}")
            },
            Self::InvalidCallError(reason) => {
                write!(f, "{reason}")
            },
            Self::LockError(e) => {
                write!(f, "Poisoned RwLock: {e}")
            },
        }
    }
}

impl Debug for PaganError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result { (self as &dyn Display).fmt(f) }
}


impl std::error::Error for PaganError {}
