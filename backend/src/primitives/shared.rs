pub mod macros {
    #[macro_export]
    macro_rules! iter_or_empty {
        ($from_set:expr) => {
            match $from_set.as_iterator(None) {
                Ok(iter) => iter,
                Err(_e) => Box::new(std::iter::empty()),
            }
        };
    }

    #[macro_export]
    macro_rules! default_matching_handleset {
        ($base:expr) => {
            match &$expr {
                HandleSet::Services(_) => HandleSet::Services(SetOption::None),
                HandleSet::ServicesAndEnablers(_) => HandleSet::ServicesAndEnablers(SetOption::None),
            }
        };
    }

    #[macro_export]
    macro_rules! services_only {
        ($collection:expr) => {
            iter_or_empty!($collection)
                .map(|(service, _)| service)
                .collect::<SetOption<ServiceHandle>>()
        };
    }

    #[macro_export]
    macro_rules! unoptionalize_enablers {
        ($collection:expr) => {
            &(iter_or_empty!($collection)
                .filter_map(|(service, enabler)| {
                    if let Some(enabler_ins) = enabler {
                        Some((service.to_owned(), enabler_ins))
                    } else {
                        None
                    }
                })
                .collect::<SetOption<(ServiceHandle, EnablerHandle)>>())
        };
    }

    #[macro_export]
    macro_rules! optionalize_enablers {
        ($collection:expr) => {
            &(iter_or_empty!($collection)
                .map(|(service, enabler)| (service.to_owned(), Option::from(enabler.to_owned())))
                .collect::<SetOption<(ServiceHandle, Option<EnablerHandle>)>>())
        };
    }

    #[macro_export]
    macro_rules! make_borrowed_cow {
        ($orig:expr) => {
            match $orig {
                Cow::Borrowed(val) => Cow::Borrowed(*val),
                Cow::Owned(val) => Cow::Borrowed(val),
            }
        };
    }

    /**
    Makes &T from Pin<Box<T>>
     **/
    #[macro_export]
    macro_rules! pinptr_inner_ref {
        ($pinptr:expr) => {
            $pinptr.as_ref().get_ref()
        };
    }
}



pub mod traits {
    use std::borrow::Cow;

    use crate::primitives::shared::types::DeviceHandle;

    pub trait PrimitiveStrength {
        fn strength(&self) -> u64;
    }

    pub trait ComplexStrength {
        fn strength(&self, target: Option<DeviceHandle>) -> u64;
    }

    pub trait IntoBorrowedCow<'inner, T: Clone> {
        fn make_borrowed(&self) -> Cow<'inner, T>;
    }

    impl<'inner, T: Clone> IntoBorrowedCow<'inner, T> for Cow<'inner, T> {
        fn make_borrowed(&self) -> Self {
            match self {
                Cow::Borrowed(referenced) => {
                    Cow::Borrowed(unsafe { std::mem::transmute::<&T, &'inner T>(*referenced) })
                },
                Cow::Owned(owned) => Cow::Borrowed(unsafe { std::mem::transmute::<&T, &'inner T>(owned) }),
            }
        }
    }
}

pub mod enums {
    use flagset::{flags, FlagSet};
    use gringotts::{DbPrivilegeLevel, EnablerImpactType};
    use rand::seq::SliceRandom;
    use strum::IntoEnumIterator;
    use strum_macros::{Display, EnumIter, EnumString};

    use crate::{error::PaganError, primitives::shared::traits::PrimitiveStrength};

    flags! {
        #[repr(u64)]
        #[derive(Hash, EnumString, EnumIter, serde::Serialize, serde::Deserialize)]
        pub enum Token : u64 {
            None = 0,

            Credential = 1 << 0,
            CredentialMfa = 1 << 1,

            Capability = 1 << 2,
            CapabilityAll = 1 << 2 | 1 << 3,

            Data = 1 << 4,
            ImpactDenialOfService = 1 << 5,

            AccessViaNetwork = 1 << 6,
            AccessViaSharedFilesys = 1 << 7,
            AccessViaRemovableMedia = 1 << 8,

            Session = 1 << 9,

            PreAttackBackdoor = 1 << 10,
            ExternalPhishingMail = 1 << 11,

            TaintedSharedContent = 1 << 12,
            InfectedRemovableMedia = 1 << 13,
            InternalPhishingMail = 1 <<14,
            MaliciousDomainConfigurationObject = 1 << 15,

            PhysicalAccess = 1 << 16,

            AccessInternalNetwork = 1 << 17,
            AccessDmzSegment = 1 << 18,
            Virtualized =  1 << 19,
            NoUsbAccess = 1 << 20,
            NoMfaAllowed = 1 << 21
        }

        #[repr(u8)]
        #[derive(EnumString, serde::Serialize, serde::Deserialize, Hash, Ord, PartialOrd)]
        pub enum CredentialType: u32 {
            None = 1 << 0,
            Os = 1 << 1,
            AppPrimary = 1 << 2,
            MfaPassable = 1 << 3,
            MfaSecure = 1 << 4,
            Mfa = (CredentialType::MfaPassable | CredentialType::MfaSecure).bits(),
            AnyPrimary = (CredentialType::Os | CredentialType::AppPrimary).bits(),
            AppAnyPassable = (CredentialType::AppPrimary | CredentialType::MfaPassable).bits(),
            AppAny = (CredentialType::AppPrimary | CredentialType::Mfa).bits(),
            Unspec = (CredentialType::AnyPrimary | CredentialType::Mfa).bits()
        }

        #[repr(u8)]
        #[derive(EnumIter, Hash)]
        pub enum ServiceImpact: u32 {
            ExecuteCode,
            ReadFile,
            UseNetwork,
            WriteFile,
        }

        #[repr(u8)]
        #[derive(EnumIter, Hash, serde::Serialize, serde::Deserialize)]
        pub enum EnablerImpact: u32 {
            Allow,
            ArbitraryCodeExecution,
            AuthManipulation,
            AvailabilityViolation,
            CredentialLeak,
            DataDisclosure,
            DataManipulation,
            MachineConfigurationTampering,
        }

    }

    #[must_use]
    pub fn service_impacts_as_capabilities(service_impacts: FlagSet<ServiceImpact>) -> Vec<CapabilityType> {
        let mut res = Vec::new();
        for impact in ServiceImpact::iter() {
            if service_impacts.contains(impact) {
                match impact {
                    ServiceImpact::ExecuteCode => res.extend_from_slice(&[
                        CapabilityType::AccessFileSysGeneric,
                        CapabilityType::AccessFileSysElevated,
                        CapabilityType::AccessFileSysShared,
                        CapabilityType::ReadFiles,
                        CapabilityType::WriteFiles,
                        CapabilityType::AccessNetworkAsDomain,
                        CapabilityType::AddUser,
                        CapabilityType::EditUser,
                        CapabilityType::DeleteUser,
                        CapabilityType::PersistLogOut,
                        CapabilityType::CodeExecution,
                        CapabilityType::Shutdown,
                        CapabilityType::DomainLogin,
                        CapabilityType::Impersonate,
                        CapabilityType::ControlDomain,
                        CapabilityType::ConfigLocalMachine,
                        CapabilityType::ConfigCurrentUser,
                        CapabilityType::UacElevate,
                    ]),
                    ServiceImpact::ReadFile => res.extend_from_slice(&[
                        CapabilityType::AccessFileSysGeneric,
                        CapabilityType::AccessFileSysElevated,
                        CapabilityType::AccessFileSysShared,
                        CapabilityType::ReadFiles,
                    ]),
                    ServiceImpact::UseNetwork => {},
                    ServiceImpact::WriteFile => res.extend_from_slice(&[
                        CapabilityType::AccessFileSysGeneric,
                        CapabilityType::AccessFileSysElevated,
                        CapabilityType::AccessFileSysShared,
                        CapabilityType::WriteFiles,
                    ]),
                }
            }
        }
        res
    }

    #[must_use]
    pub fn enabler_impacts_as_capabilities(enabler_impacts: FlagSet<EnablerImpact>) -> Vec<CapabilityType> {
        let mut res = Vec::new();
        for impact in EnablerImpact::iter() {
            if enabler_impacts.contains(impact) {
                match impact {
                    EnablerImpact::ArbitraryCodeExecution => res.extend_from_slice(&[
                        CapabilityType::AccessFileSysGeneric,
                        CapabilityType::AccessFileSysElevated,
                        CapabilityType::AccessFileSysShared,
                        CapabilityType::ReadFiles,
                        CapabilityType::WriteFiles,
                        CapabilityType::AccessNetworkAsDomain,
                        CapabilityType::AddUser,
                        CapabilityType::EditUser,
                        CapabilityType::DeleteUser,
                        CapabilityType::PersistLogOut,
                        CapabilityType::CodeExecution,
                        CapabilityType::Shutdown,
                        CapabilityType::DomainLogin,
                        CapabilityType::Impersonate,
                        CapabilityType::ControlDomain,
                        CapabilityType::ConfigLocalMachine,
                        CapabilityType::ConfigCurrentUser,
                        CapabilityType::UacElevate,
                    ]),
                    EnablerImpact::AuthManipulation => res.extend_from_slice(&[
                        CapabilityType::DeleteUser,
                        CapabilityType::AddUser,
                        CapabilityType::EditUser,
                    ]),
                    EnablerImpact::DataDisclosure => res.extend_from_slice(&[
                        CapabilityType::AccessFileSysElevated,
                        CapabilityType::AccessFileSysGeneric,
                        CapabilityType::AccessFileSysShared,
                        CapabilityType::ReadFiles,
                    ]),
                    EnablerImpact::DataManipulation => res.extend_from_slice(&[
                        CapabilityType::AccessFileSysElevated,
                        CapabilityType::AccessFileSysGeneric,
                        CapabilityType::AccessFileSysShared,
                        CapabilityType::WriteFiles,
                    ]),
                    EnablerImpact::MachineConfigurationTampering => {
                        res.extend_from_slice(&[CapabilityType::ConfigCurrentUser, CapabilityType::ConfigLocalMachine])
                    },
                    _ => {},
                }
            }
        }
        res
    }



    impl From<EnablerImpactType> for EnablerImpact {
        fn from(value: EnablerImpactType) -> Self {
            match &value {
                EnablerImpactType::Allow => Self::Allow,
                EnablerImpactType::ArbitraryCodeExecution => Self::ArbitraryCodeExecution,
                EnablerImpactType::AuthManipulation => Self::AuthManipulation,
                EnablerImpactType::AvailabilityViolation => Self::AvailabilityViolation,
                EnablerImpactType::CredentialLeak => Self::CredentialLeak,
                EnablerImpactType::DataDisclosure => Self::DataDisclosure,
                EnablerImpactType::DataManipulation => Self::DataManipulation,
                EnablerImpactType::MachineConfigurationTampering => Self::MachineConfigurationTampering,
            }
        }
    }

    impl PrimitiveStrength for FlagSet<CredentialType> {
        fn strength(&self) -> u64 {
            let mut res = 0u64;

            if !self.is_disjoint(CredentialType::Os) {
                res += 6;
            }

            if !self.is_disjoint(CredentialType::AppPrimary) {
                res += 3;
            }

            if !self.is_disjoint(CredentialType::MfaPassable) {
                res += 1;
            }

            if !self.is_disjoint(CredentialType::MfaSecure) {
                res += 0;
            }

            res
        }
    }

    #[derive(
        EnumString,
        Clone,
        PartialEq,
        Hash,
        Eq,
        EnumIter,
        serde::Serialize,
        serde::Deserialize,
        Debug,
        Copy,
        Ord,
        PartialOrd,
    )]
    pub enum CapabilityType {
        AccessFileSysGeneric,
        AccessFileSysElevated,
        AccessFileSysShared,
        ReadFiles,
        WriteFiles,
        AccessNetworkAsDomain,
        AddUser,
        EditUser,
        DeleteUser,
        PersistLogOut,
        CodeExecution,
        Shutdown,
        DomainLogin,
        Persist,
        Impersonate, // impersonation in email, o365, chat systems connected to AD, etc.
        ControlDomain,
        ConfigLocalMachine,
        ConfigCurrentUser,
        UacElevate,
    }

    #[derive(
        EnumString,
        Clone,
        PartialEq,
        Hash,
        Eq,
        EnumIter,
        serde::Serialize,
        serde::Deserialize,
        Debug,
        Copy,
        Ord,
        PartialOrd,
    )]
    pub enum Specials {
        SeDebug,
        SeLoadDriver,
        SeCreateSecurityToken,
        SeImpersonateToken,
    }

    impl PrimitiveStrength for CapabilityType {
        fn strength(&self) -> u64 {
            match self {
                Self::AccessFileSysGeneric => 15,
                Self::AccessFileSysElevated | Self::UacElevate => 20,
                Self::AccessFileSysShared => 18,
                Self::ReadFiles => 30,
                Self::WriteFiles => 35,
                Self::AccessNetworkAsDomain | Self::Shutdown | Self::PersistLogOut => 10,
                Self::AddUser | Self::DeleteUser | Self::EditUser | Self::ConfigCurrentUser => 25,
                Self::Impersonate | Self::Persist => 5,
                Self::ControlDomain => 80,
                Self::ConfigLocalMachine | Self::CodeExecution | Self::DomainLogin => 40,
            }
        }
    }

    #[derive(EnumString, Clone, PartialEq, Hash, Eq, serde::Serialize, serde::Deserialize, Debug, Copy)]
    pub enum LimitType {
        Minimum,
        Exact,
        Maximum,
    }

    #[derive(
        Ord,
        PartialOrd,
        PartialEq,
        Eq,
        EnumString,
        Clone,
        Hash,
        serde::Serialize,
        serde::Deserialize,
        Debug,
        Copy,
        Display,
    )]
    pub enum PrivilegeLevel {
        None,
        Service,
        User,
        Administrator,
        LocalSystem,
    }

    impl PrimitiveStrength for PrivilegeLevel {
        fn strength(&self) -> u64 { self.to_owned() as u64 }
    }

    impl Default for PrivilegeLevel {
        fn default() -> Self { Self::None }
    }

    #[derive(Ord, PartialOrd, PartialEq, Eq, EnumString, Clone, Hash, serde::Serialize, serde::Deserialize, Copy)]
    pub enum ProcessIntegrityLevel {
        Low,
        Medium,
        High,
        System,
        Protected,
    }

    impl PrimitiveStrength for ProcessIntegrityLevel {
        fn strength(&self) -> u64 { *self as u64 }
    }

    impl TryFrom<PrivilegeLevel> for ProcessIntegrityLevel {
        type Error = PaganError;

        fn try_from(value: PrivilegeLevel) -> Result<Self, Self::Error> {
            match value {
                PrivilegeLevel::None => Err(PaganError::AutomatonBuildingError(
                    "Can not convert PrivilegeLevel::None to ProcessIntegrityLevel".into(),
                )),
                PrivilegeLevel::Service => Ok(Self::Low),
                PrivilegeLevel::User => Ok(Self::Medium),
                PrivilegeLevel::Administrator => Ok(Self::High),
                PrivilegeLevel::LocalSystem => Ok(Self::System),
            }
        }
    }

    impl From<Option<DbPrivilegeLevel>> for PrivilegeLevel {
        fn from(value: Option<DbPrivilegeLevel>) -> Self {
            value.map_or(Self::None, |val| match val {
                DbPrivilegeLevel::Administrator => Self::Administrator,
                DbPrivilegeLevel::LocalSystem => Self::LocalSystem,
                DbPrivilegeLevel::None => Self::None,
                DbPrivilegeLevel::Service => Self::Service,
                DbPrivilegeLevel::User => Self::User,
            })
        }
    }

    #[derive(PartialEq, Eq, EnumString, Clone, serde::Serialize, serde::Deserialize, Copy)]
    pub enum PrivilegeOrigin {
        None,
        Service,      // the exploited processes owners'
        Credential,   // the accounts' to which the credential belongs to
        RandomUser,   // a random interactive users'
        NewUser,      // same as inherit, just showing the difference in techniques
        InheritUser,  // same user as we are acting as, but maybe on another device (domain accounts only)
        ElevatedUser, // UAC elevated account
        Fixed(PrivilegeLevel),
    }

    #[derive(PartialEq, Eq, EnumString, Clone, serde::Serialize, serde::Deserialize, Copy)]
    pub enum CapabilitySpecifier {
        None,
        Service,
        Enabler,
        Action,
    }

    #[derive(PartialEq, Eq, EnumString, Clone, serde::Serialize, serde::Deserialize, Copy)]
    pub enum CredentialOrigin {
        None,
        OsAllLocal, // all local accounts - on MS everything in the SAM DB, on the DC all domain accounts as well
        OsCached,   // lsass cache, virtually any account that can touch the device
        OsActiveAccount, // the accounts' whose capabilities we are using momentarily
        ServiceSelf, // app creds for the exploited software, only main cred
        ServiceSelfAll, // same, but mfa creds as well
        ServiceOther, // MFA for other services, used for exploiting something like Google Authenticator, for the user whose capabilities we are using (this is always a local action)
        Mimic,        // MFA for any service, for the user who the used credential belongs to
        AnyActualUser, // any credential of the account whose capabilities we are using (userspace keylogger)
        AnyPresentUsers, // any credential of any user who is able to use the device (kernel keylogger)
        NewUser,      // not needed for scenario building, but helps with technique design
    }

    #[derive(PartialEq, Eq, Clone, serde::Serialize, serde::Deserialize, Copy)]
    pub enum AccessType {
        Network,
        Media,
        SharedFileSystem,
        LocalHost, // basically network but no firewall rules apply
    }

    #[derive(
        PartialEq, Clone, serde::Serialize, serde::Deserialize, EnumString, Copy, Eq, Hash, Debug, Ord, PartialOrd,
    )]
    pub enum OperatingSystem {
        WindowsServer,
        WindowsDesktop,
        NixServer,
        NixDesktop,
        SpecialNetwork,
        SpecialEmbedded,
        Mock,
    }

    impl OperatingSystem {
        #[must_use]
        pub fn random_server() -> Self {
            let mut rng = rand::thread_rng();

            [Self::WindowsServer, Self::NixServer]
                .choose(&mut rng)
                .unwrap()
                .to_owned()
        }

        #[must_use]
        pub fn random_desktop() -> Self {
            let mut rng = rand::thread_rng();

            [Self::WindowsDesktop, Self::NixDesktop]
                .choose(&mut rng)
                .unwrap()
                .to_owned()
        }
    }

    #[derive(
        PartialEq, Clone, serde::Serialize, serde::Deserialize, EnumString, Copy, Eq, Hash, Debug, Ord, PartialOrd,
    )]
    pub enum DeviceRole {
        Workstation,
        MailServer,
        WebServer,
        DomainController,
        NetworkShare,
        BackupServer,
        Database,
        OperationalTechnology,
        NetworkAppliance,
        RemoteAccessGateway,
    }

    impl PrimitiveStrength for DeviceRole {
        fn strength(&self) -> u64 {
            match self {
                Self::Workstation => 15,
                Self::MailServer | Self::WebServer => 25,
                Self::DomainController => 100,
                Self::NetworkShare => 55,
                Self::BackupServer => 85,
                Self::Database => 45,
                Self::OperationalTechnology => 35,
                Self::NetworkAppliance => 20,
                Self::RemoteAccessGateway => 30,
            }
        }
    }

    #[derive(PartialEq, Clone, serde::Serialize, serde::Deserialize, Copy, Eq, Hash, Debug, Ord, PartialOrd)]
    pub enum SegmentType {
        Internal,
        Dmz,
        Attacker,
        Partner,
        Internet,
    }

    impl From<SegmentType> for Token {
        fn from(value: SegmentType) -> Self {
            match &value {
                SegmentType::Internal => Self::AccessInternalNetwork,
                SegmentType::Dmz => Self::AccessDmzSegment,
                _ => Self::None,
            }
        }
    }

    #[derive(EnumString, Eq, PartialEq, serde::Serialize, serde::Deserialize, Debug, Clone, Copy)]
    pub enum EnablerLocality {
        Local,
        Remote,
    }
}

pub mod utils {
    use std::{
        borrow::Borrow,
        collections::{BTreeSet, HashMap},
        hash::Hash,
        ops::Range,
    };

    use crate::{
        error::PaganError,
        iter_or_empty,
        optionalize_enablers,
        primitives::shared::types::{ActionHandle, EnablerHandle, PaganResult, ServiceHandle},
        services_only,
        unoptionalize_enablers,
    };

    #[derive(Default, Debug)]
    pub struct Counter {
        ct: usize,
    }

    impl Counter {
        pub fn give(&mut self) -> usize {
            self.ct += 1;
            self.ct
        }
    }

    pub trait SetOptionAtomicOps<T> {
        fn contains(&self, atom: impl Borrow<T>) -> bool;
        fn insert(&mut self, atom: T);
    }

    pub trait SetOptionCombinatorOps {
        fn union(&mut self, rhs: &Self) -> PaganResult<()>;
        fn intersection(&mut self, rhs: &Self) -> PaganResult<()>;
        fn difference(&mut self, rhs: &Self, all: Option<&BTreeSet<ServiceHandle>>) -> PaganResult<()>;
        fn retain_if_service<F>(&mut self, r#fn: F, all: Option<&BTreeSet<ServiceHandle>>) -> PaganResult<()>
        where
            F: FnMut(&ServiceHandle) -> bool;
        fn retain_if<F>(&mut self, r#fn: F) -> PaganResult<()>
        where
            F: FnMut(&ServiceHandle, Option<&EnablerHandle>) -> bool;
        fn has_intersection(&self, rhs: &Self) -> bool;

        fn intersection_cardinality(&self, rhs: &Self) -> usize;
    }

    #[derive(serde::Serialize, serde::Deserialize, Debug, Clone, PartialEq, Hash, Eq, Ord, PartialOrd)]
    pub enum SetOption<T: Hash + Eq + Ord> {
        None,
        All,
        Some(BTreeSet<T>),
    }


    impl<T: Hash + Eq + Ord + Clone> SetOption<T> {
        #[must_use]
        pub fn is_superset(&self, rhs: &Self) -> bool {
            match self {
                Self::None => {
                    matches!(rhs, Self::None)
                },
                Self::All => true,
                Self::Some(my_s) => match rhs {
                    Self::None => true,
                    Self::All => false,
                    Self::Some(other_s) => my_s.is_superset(other_s),
                },
            }
        }

        pub fn contains(&self, atom: impl Borrow<T>) -> bool {
            match &self {
                Self::None => false,
                Self::All => true,
                Self::Some(set) => set.contains(atom.borrow()),
            }
        }

        pub fn insert(&mut self, atom: T) {
            match self {
                Self::None => {
                    let mut tmp = BTreeSet::new();

                    tmp.insert(atom);

                    let _ = std::mem::replace(self, Self::Some(tmp));
                },
                Self::All => (),
                Self::Some(set) => {
                    set.insert(atom);
                },
            }
        }

        pub fn union(&mut self, rhs: &Self) {
            match self {
                Self::None => {
                    let _ = std::mem::replace(self, rhs.clone());
                },
                Self::All => {},
                Self::Some(s) => match rhs {
                    Self::None => {},
                    Self::All => {
                        let _ = std::mem::replace(self, rhs.clone());
                    },
                    Self::Some(o) => s.extend(o.iter().cloned()),
                },
            }
        }

        pub fn intersection(&mut self, rhs: &Self) {
            match self {
                Self::None => {},
                Self::All => {
                    let _ = std::mem::replace(self, rhs.clone());
                },
                Self::Some(s) => match rhs {
                    Self::None => {
                        let _ = std::mem::replace(self, rhs.clone());
                    },
                    Self::All => {},
                    Self::Some(o) => s.retain(|elem| o.contains(elem)),
                },
            }
        }

        #[must_use]
        pub fn has_intersection(&self, rhs: &Self) -> bool {
            match self {
                Self::None => false,
                Self::All => matches!(rhs, Self::All | Self::Some(_)),
                Self::Some(self_set) => match rhs {
                    Self::None => false,
                    Self::All => true,
                    Self::Some(rhs_set) => self_set.iter().any(|item| rhs_set.contains(item)),
                },
            }
        }

        #[must_use]
        pub fn intersection_cardinality(&self, rhs: &Self) -> usize {
            match self {
                Self::None => 0,
                Self::All => match rhs {
                    Self::None => 0,
                    Self::All => usize::MAX,
                    Self::Some(s) => s.len(),
                },
                Self::Some(self_set) => match rhs {
                    Self::None => 0,
                    Self::All => self_set.len(),
                    Self::Some(rhs_set) => self_set.intersection(rhs_set).count(),
                },
            }
        }

        pub fn retain<F>(&mut self, mut r#fn: F, all: Option<&BTreeSet<T>>) -> PaganResult<()>
        where
            F: FnMut(&T) -> bool,
        {
            match self {
                Self::None => Ok(()),
                Self::All => all.map_or_else(
                    || {
                        Err(PaganError::InvalidCallError(
                            "SetOption::retain called on Setoption::All without enumerating possibilities.".into(),
                        ))
                    },
                    |all_set| {
                        let _ = std::mem::replace(self, all_set.iter().filter(|elem| r#fn(*elem)).collect::<Self>());
                        Ok(())
                    },
                ),
                Self::Some(set) => {
                    set.retain(r#fn);
                    Ok(())
                },
            }
        }

        pub fn difference(&mut self, rhs: &Self, all: Option<&BTreeSet<T>>) -> PaganResult<()> {
            match self {
                Self::None => Ok(()),
                Self::All => match rhs {
                    Self::None => {
                        let _ = std::mem::replace(self, Self::All);
                        Ok(())
                    },
                    Self::All => {
                        let _ = std::mem::replace(self, Self::None);
                        Ok(())
                    },
                    Self::Some(rhs_s) => all.map_or_else(
                        || {
                            Err(PaganError::InvalidCallError(
                                "Cannot call SetOption::without() on SetOption::All and SetOption::Some without \
                                 providing the `all` argument"
                                    .into(),
                            ))
                        },
                        |enumeration| {
                            let _ = std::mem::replace(
                                self,
                                Self::Some(enumeration.difference(rhs_s).cloned().collect::<BTreeSet<_>>()),
                            );
                            Ok(())
                        },
                    ),
                },
                Self::Some(my_s) => match rhs {
                    Self::None => Ok(()),
                    Self::All => {
                        let _ = std::mem::replace(self, Self::None);
                        Ok(())
                    },
                    Self::Some(other_s) => {
                        my_s.retain(|elem| !other_s.contains(elem));
                        Ok(())
                    },
                },
            }
        }

        pub fn as_iterator<'slf, 'store: 'slf>(
            &'slf self,
            all: Option<&'store BTreeSet<T>>,
        ) -> PaganResult<Box<dyn Iterator<Item = T> + 'slf>> {
            match self {
                Self::None => Ok(Box::new(std::iter::empty())),
                Self::All => match all {
                    None => Err(PaganError::InvalidCallError(
                        "Setoption::iter() on SetOption::All must provide the `all` argument".into(),
                    )),
                    Some(all_set) => Ok(Box::new(all_set.iter().cloned())),
                },
                Self::Some(s) => Ok(Box::new(s.iter().cloned())),
            }
        }
    }

    impl<'a, T: Ord + Hash + Clone> FromIterator<&'a T> for SetOption<T> {
        fn from_iter<I: IntoIterator<Item = &'a T>>(iter: I) -> Self {
            let mut peekable = iter.into_iter().peekable();
            if peekable.peek().is_some() {
                Self::Some(peekable.cloned().collect::<BTreeSet<_>>())
            } else {
                Self::None
            }
        }
    }

    impl<T: Ord + Hash> FromIterator<T> for SetOption<T> {
        fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
            let mut peekable = iter.into_iter().peekable();
            if peekable.peek().is_some() {
                Self::Some(peekable.collect::<BTreeSet<_>>())
            } else {
                Self::None
            }
        }
    }



    #[derive(Debug, PartialEq, Hash, Eq, Clone, serde::Serialize, serde::Deserialize, Ord, PartialOrd)]
    pub enum HandleSet {
        Services(SetOption<ServiceHandle>),
        ServicesAndEnablers(SetOption<(ServiceHandle, EnablerHandle)>),
        ServicesMaybeEnablers(SetOption<(ServiceHandle, Option<EnablerHandle>)>),
    }

    impl HandleSet {
        #[must_use]
        pub fn just_services(&self) -> SetOption<ServiceHandle> {
            match self {
                Self::Services(s) => s.to_owned(),
                Self::ServicesAndEnablers(se) => services_only!(se),
                Self::ServicesMaybeEnablers(se) => services_only!(se),
            }
        }

        #[must_use]
        pub fn is_empty(&self) -> bool {
            match self {
                Self::Services(s) => s == &SetOption::None,
                Self::ServicesAndEnablers(s) => s == &SetOption::None,
                Self::ServicesMaybeEnablers(s) => s == &SetOption::None,
            }
        }

        #[must_use]
        pub fn as_iterator<'slf, 'paren: 'slf>(
            &'slf self,
            all: Option<&'paren BTreeSet<ServiceHandle>>,
        ) -> Box<dyn Iterator<Item = (ServiceHandle, Option<EnablerHandle>)> + 'slf> {
            match self {
                Self::Services(s) => all.map_or_else(
                    || Box::new(iter_or_empty!(s).zip(std::iter::repeat(None))),
                    |all_s| Box::new(s.as_iterator(Some(all_s)).unwrap().zip(std::iter::repeat(None))),
                ),
                Self::ServicesAndEnablers(s) => Box::new(
                    iter_or_empty!(s).map(|(service, enabler)| (service.to_owned(), Option::from(enabler.to_owned()))),
                ),
                Self::ServicesMaybeEnablers(s) => Box::new(iter_or_empty!(s).map(|item| item.to_owned())),
            }
        }
    }

    impl SetOptionAtomicOps<ServiceHandle> for HandleSet {
        fn contains(&self, atom: impl Borrow<ServiceHandle>) -> bool {
            match self {
                Self::Services(s) => s.contains(atom),
                Self::ServicesAndEnablers(_) => false,
                Self::ServicesMaybeEnablers(s) => iter_or_empty!(s)
                    .find(|(service, _)| service == atom.borrow())
                    .is_some(),
            }
        }

        fn insert(&mut self, atom: ServiceHandle) {
            match self {
                Self::Services(s) => s.insert(atom),
                Self::ServicesAndEnablers(_) => {},
                Self::ServicesMaybeEnablers(s) => s.insert((atom, None)),
            }
        }
    }

    impl SetOptionAtomicOps<(ServiceHandle, EnablerHandle)> for HandleSet {
        fn contains(&self, atom: impl Borrow<(ServiceHandle, EnablerHandle)>) -> bool {
            match self {
                Self::ServicesAndEnablers(s) => s.contains(atom),
                Self::Services(_) => false,
                Self::ServicesMaybeEnablers(s) => {
                    let (service, enabler) = atom.borrow();
                    s.contains((service.to_owned(), Option::from(enabler.to_owned())))
                },
            }
        }

        fn insert(&mut self, atom: (ServiceHandle, EnablerHandle)) {
            match self {
                Self::Services(_) => {},
                Self::ServicesAndEnablers(s) => s.insert(atom),
                Self::ServicesMaybeEnablers(s) => {
                    let (service, enabler) = atom.borrow();
                    s.insert((service.to_owned(), Option::from(enabler.to_owned())));
                },
            }
        }
    }

    impl SetOptionAtomicOps<(ServiceHandle, Option<EnablerHandle>)> for HandleSet {
        fn contains(&self, atom: impl Borrow<(ServiceHandle, Option<EnablerHandle>)>) -> bool {
            match self {
                Self::ServicesAndEnablers(s) => {
                    if atom.borrow().1.is_some() {
                        s.contains((atom.borrow().0, atom.borrow().1.unwrap()))
                    } else {
                        false
                    }
                },
                Self::Services(_) => false,
                Self::ServicesMaybeEnablers(s) => s.contains(atom),
            }
        }

        fn insert(&mut self, atom: (ServiceHandle, Option<EnablerHandle>)) {
            match self {
                Self::Services(_) => {},
                Self::ServicesAndEnablers(s) => {
                    if atom.1.is_some() {
                        s.insert((atom.0, atom.1.unwrap()));
                    }
                },
                Self::ServicesMaybeEnablers(s) => s.insert(atom),
            }
        }
    }

    impl SetOptionCombinatorOps for HandleSet {
        fn union(&mut self, rhs: &Self) -> PaganResult<()> {
            match self {
                Self::Services(my_s) => match rhs {
                    Self::Services(other_s) => {
                        my_s.union(other_s);
                        Ok(())
                    },
                    Self::ServicesAndEnablers(_) => Err(PaganError::InvalidCallError(
                        "HandleSet::union called with divergent enum variants".into(),
                    )),
                    Self::ServicesMaybeEnablers(other_s) => {
                        iter_or_empty!(other_s).for_each(|(service, _)| my_s.insert(service.to_owned()));
                        Ok(())
                    },
                },
                Self::ServicesAndEnablers(my_s) => match rhs {
                    Self::Services(_) => Err(PaganError::InvalidCallError(
                        "HandleSet::union called with divergent enum variants".into(),
                    )),
                    Self::ServicesAndEnablers(other_s) => {
                        my_s.union(other_s);
                        Ok(())
                    },
                    Self::ServicesMaybeEnablers(other_s) => {
                        iter_or_empty!(other_s)
                            .filter(|(_service, enabler): &(_, Option<EnablerHandle>)| enabler.is_some())
                            .for_each(|(service, enabler)| my_s.insert((service.to_owned(), enabler.unwrap())));
                        Ok(())
                    },
                },
                Self::ServicesMaybeEnablers(my_s) => match rhs {
                    Self::Services(other_s) => {
                        iter_or_empty!(other_s).for_each(|service| my_s.insert((service.to_owned(), None)));
                        Ok(())
                    },
                    Self::ServicesAndEnablers(other_s) => {
                        iter_or_empty!(other_s).for_each(|(service, enabler)| {
                            my_s.insert((service.to_owned(), Option::from(enabler.to_owned())))
                        });
                        Ok(())
                    },
                    Self::ServicesMaybeEnablers(other_s) => {
                        my_s.union(other_s);
                        Ok(())
                    },
                },
            }
        }

        fn intersection(&mut self, rhs: &Self) -> PaganResult<()> {
            match self {
                Self::Services(my_s) => match rhs {
                    Self::Services(other_s) => {
                        my_s.intersection(other_s);
                        Ok(())
                    },
                    Self::ServicesAndEnablers(other_s) => {
                        my_s.intersection(&services_only!(other_s));
                        Ok(())
                    },
                    Self::ServicesMaybeEnablers(other_s) => {
                        my_s.intersection(&services_only!(other_s));
                        Ok(())
                    },
                },
                Self::ServicesAndEnablers(my_s) => match rhs {
                    Self::Services(other_s) => my_s.retain(|(service, _)| other_s.contains(service), None),
                    Self::ServicesAndEnablers(other_s) => {
                        my_s.intersection(other_s);
                        Ok(())
                    },
                    Self::ServicesMaybeEnablers(other_s) => {
                        my_s.intersection(unoptionalize_enablers!(other_s));
                        Ok(())
                    },
                },
                Self::ServicesMaybeEnablers(my_s) => match rhs {
                    Self::Services(other_s) => my_s.retain(|(service, _enabler)| other_s.contains(service), None),
                    Self::ServicesAndEnablers(other_s) => {
                        my_s.intersection(optionalize_enablers!(other_s));
                        Ok(())
                    },
                    Self::ServicesMaybeEnablers(other_s) => {
                        my_s.intersection(other_s);
                        Ok(())
                    },
                },
            }
        }

        fn difference(&mut self, rhs: &Self, all: Option<&BTreeSet<ServiceHandle>>) -> PaganResult<()> {
            match self {
                Self::Services(my_s) => match rhs {
                    Self::Services(other_s) => my_s.difference(other_s, all),
                    Self::ServicesAndEnablers(other_s) => my_s.difference(&services_only!(other_s), all),
                    Self::ServicesMaybeEnablers(other_s) => my_s.difference(&services_only!(other_s), all),
                },
                Self::ServicesAndEnablers(my_s) => match rhs {
                    Self::Services(other_s) => my_s.retain(|(service, _)| other_s.contains(service), None),
                    Self::ServicesAndEnablers(other_s) => my_s.difference(other_s, None),
                    Self::ServicesMaybeEnablers(other_s) => my_s.difference(unoptionalize_enablers!(other_s), None),
                },
                Self::ServicesMaybeEnablers(my_s) => match rhs {
                    Self::Services(other_s) => my_s.retain(|(service, _)| !other_s.contains(service), None),
                    Self::ServicesAndEnablers(other_s) => my_s.difference(optionalize_enablers!(other_s), None),
                    Self::ServicesMaybeEnablers(other_s) => my_s.difference(other_s, None),
                },
            }
        }

        fn retain_if_service<F>(&mut self, mut r#fn: F, all: Option<&BTreeSet<ServiceHandle>>) -> PaganResult<()>
        where
            F: FnMut(&ServiceHandle) -> bool,
        {
            match self {
                Self::Services(my_s) => my_s.retain(r#fn, all),
                Self::ServicesAndEnablers(my_s) => my_s.retain(|(service, _)| r#fn(service), None),
                Self::ServicesMaybeEnablers(my_s) => my_s.retain(|(service, _)| r#fn(service), None),
            }
        }

        fn retain_if<F>(&mut self, mut r#fn: F) -> PaganResult<()>
        where
            F: FnMut(&ServiceHandle, Option<&EnablerHandle>) -> bool,
        {
            match self {
                Self::Services(s) => s.retain(|service| r#fn(service, None), None),
                Self::ServicesAndEnablers(s) => {
                    s.retain(|(service, enabler)| r#fn(service, Option::from(enabler)), None)
                },
                Self::ServicesMaybeEnablers(s) => s.retain(|(service, enabler)| r#fn(service, enabler.as_ref()), None),
            }
        }

        fn has_intersection(&self, rhs: &Self) -> bool {
            match self {
                Self::Services(my_s) => match rhs {
                    Self::Services(other_s) => my_s.has_intersection(other_s),
                    Self::ServicesAndEnablers(other_s) => my_s.has_intersection(&services_only!(other_s)),
                    Self::ServicesMaybeEnablers(other_s) => my_s.has_intersection(&services_only!(other_s)),
                },
                Self::ServicesAndEnablers(my_s) => match rhs {
                    Self::Services(other_s) => iter_or_empty!(my_s).any(|(service, _)| other_s.contains(service)),
                    Self::ServicesAndEnablers(other_s) => my_s.has_intersection(other_s),
                    Self::ServicesMaybeEnablers(other_s) => my_s.has_intersection(unoptionalize_enablers!(other_s)),
                },
                Self::ServicesMaybeEnablers(my_s) => match rhs {
                    Self::Services(other_s) => iter_or_empty!(my_s).any(|(service, _)| other_s.contains(service)),
                    Self::ServicesAndEnablers(other_s) => my_s.has_intersection(optionalize_enablers!(other_s)),
                    Self::ServicesMaybeEnablers(other_s) => my_s.has_intersection(other_s),
                },
            }
        }

        fn intersection_cardinality(&self, rhs: &Self) -> usize {
            match self {
                Self::Services(my_s) => match rhs {
                    Self::Services(other_s) => my_s.intersection_cardinality(other_s),
                    Self::ServicesAndEnablers(other_s) => my_s.intersection_cardinality(&services_only!(other_s)),
                    Self::ServicesMaybeEnablers(other_s) => my_s.intersection_cardinality(&services_only!(other_s)),
                },
                Self::ServicesAndEnablers(my_s) => match rhs {
                    Self::Services(other_s) => iter_or_empty!(my_s)
                        .filter(|(service, _)| other_s.contains(service))
                        .count(),
                    Self::ServicesAndEnablers(other_s) => my_s.intersection_cardinality(other_s),
                    Self::ServicesMaybeEnablers(other_s) => {
                        my_s.intersection_cardinality(unoptionalize_enablers!(other_s))
                    },
                },
                Self::ServicesMaybeEnablers(my_s) => match rhs {
                    Self::Services(other_s) => iter_or_empty!(my_s)
                        .filter(|(service, _)| other_s.contains(service))
                        .count(),
                    Self::ServicesAndEnablers(other_s) => my_s.intersection_cardinality(optionalize_enablers!(other_s)),
                    Self::ServicesMaybeEnablers(other_s) => my_s.intersection_cardinality(other_s),
                },
            }
        }
    }

    pub type ApplicabilityMatrix = HashMap<ActionHandle, HandleSet>;

    #[derive(PartialEq, Eq, Hash, Debug, serde::Serialize, serde::Deserialize, Ord, PartialOrd)]
    pub struct Port(u32);

    impl TryFrom<u32> for Port {
        type Error = PaganError;

        fn try_from(value: u32) -> Result<Self, Self::Error> {
            if value > 65535 {
                return Err(PaganError::InvalidPortNumber);
            }

            Ok(Self(value))
        }
    }

    pub struct PortSet(Vec<Port>);

    impl TryFrom<Range<u32>> for PortSet {
        type Error = PaganError;

        fn try_from(value: Range<u32>) -> Result<Self, Self::Error> {
            let mut me = Self(Vec::with_capacity(value.len()));

            for i in value {
                me.0.push(Port::try_from(i)?);
            }

            Ok(me)
        }
    }

    impl TryFrom<u32> for PortSet {
        type Error = PaganError;

        fn try_from(value: u32) -> Result<Self, Self::Error> { Ok(Self(vec![Port::try_from(value)?])) }
    }

    impl<const N: usize> TryFrom<[u32; N]> for PortSet {
        type Error = PaganError;

        fn try_from(value: [u32; N]) -> Result<Self, Self::Error> {
            let mut me = Self(Vec::with_capacity(value.len()));

            for i in value {
                me.0.push(Port::try_from(i)?);
            }

            Ok(me)
        }
    }

    impl TryFrom<Vec<u32>> for PortSet {
        type Error = PaganError;

        fn try_from(value: Vec<u32>) -> Result<Self, Self::Error> {
            Ok(Self(
                value
                    .into_iter()
                    .map(Port::try_from)
                    .collect::<PaganResult<Vec<Port>>>()?,
            ))
        }
    }
}

pub mod types {
    use std::{
        cmp::{Ordering, PartialOrd},
        collections::BTreeSet,
        hash::{Hash, Hasher},
        ops::{BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Not, Rem, RemAssign, Sub, SubAssign},
    };

    use flagset::{FlagSet, Flags, InvalidBits};
    use serde::Serializer;

    use crate::{
        error::PaganError,
        primitives::{accounts::Account, capabilities::AttackerCapability, shared::enums::PrivilegeLevel},
    };

    pub type DeviceHandle = usize;

    pub type ActionHandle = usize;

    pub type SchemeHandle = usize;

    pub type ServiceHandle = usize;

    pub type EnablerHandle = usize;

    pub type AccountHandle = usize;

    pub type CredentialHandle = usize;

    pub type PersonHandle = usize;

    pub type PaganResult<T> = Result<T, PaganError>;

    pub type ImpactRepr = u32;




    #[derive(PartialEq, Eq, Clone, Copy, Debug, Default)]
    pub struct OrdFlagSet<T: Flags>(FlagSet<T>);

    impl<T: Flags> serde::Serialize for OrdFlagSet<T>
    where
        <T as Flags>::Type: serde::Serialize,
    {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            self.0.serialize(serializer)
        }
    }

    impl<T: Flags + Hash> Hash for OrdFlagSet<T>
    where
        <T as Flags>::Type: Hash,
    {
        fn hash<H: Hasher>(&self, state: &mut H) { self.0.hash(state) }
    }

    impl<T: Flags> PartialOrd<Self> for OrdFlagSet<T>
    where
        T::Type: PartialOrd,
    {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> { self.as_inner().partial_cmp(&other.as_inner()) }
    }

    impl<T: Flags> Ord for OrdFlagSet<T>
    where
        T::Type: Ord,
    {
        fn cmp(&self, other: &Self) -> Ordering { self.as_inner().cmp(&other.as_inner()) }
    }

    pub struct OrdFlagSetIter<T: Flags>(Box<dyn Iterator<Item = T>>);
    impl<T: Flags> Iterator for OrdFlagSetIter<T> {
        type Item = T;

        fn next(&mut self) -> Option<Self::Item> { self.0.next() }
    }

    impl<T: Flags, F: Into<FlagSet<T>>> From<F> for OrdFlagSet<T> {
        fn from(value: F) -> Self { Self(value.into()) }
    }

    impl<T: Flags, O: Into<Self>> BitAnd<O> for OrdFlagSet<T> {
        type Output = Self;

        fn bitand(self, rhs: O) -> Self::Output { self.0.bitand(rhs.into().0).into() }
    }

    impl<T: Flags, O: Into<Self>> BitAndAssign<O> for OrdFlagSet<T> {
        fn bitand_assign(&mut self, rhs: O) { let _ = std::mem::replace(self, (*self).bitand(rhs)); }
    }

    impl<T: Flags, O: Into<Self>> BitOr<O> for OrdFlagSet<T> {
        type Output = Self;

        fn bitor(self, rhs: O) -> Self::Output { self.0.bitor(rhs.into().0).into() }
    }

    impl<T: Flags, O: Into<Self>> BitOrAssign<O> for OrdFlagSet<T> {
        fn bitor_assign(&mut self, rhs: O) { let _ = std::mem::replace(self, (*self).bitor(rhs)); }
    }

    impl<T: Flags, O: Into<Self>> BitXor<O> for OrdFlagSet<T> {
        type Output = Self;

        fn bitxor(self, rhs: O) -> Self::Output { self.0.bitxor(rhs.into().0).into() }
    }

    impl<T: Flags, O: Into<Self>> BitXorAssign<O> for OrdFlagSet<T> {
        fn bitxor_assign(&mut self, rhs: O) { let _ = std::mem::replace(self, (*self).bitxor(rhs)); }
    }

    // impl<T> IntoIterator for OrdFlagSet<T> {
    //     type Item = T;
    //     type IntoIter = OrdFlagSetIter<T>;
    //
    //     fn into_iter(self) -> Self::IntoIter { OrdFlagSetIter(Box::new(self.0.into_iter())) }
    // }

    impl<T: Flags> Not for OrdFlagSet<T> {
        type Output = Self;

        fn not(self) -> Self::Output { (!self.0).into() }
    }

    impl<T: Flags, O: Into<Self>> Rem<O> for OrdFlagSet<T> {
        type Output = Self;

        fn rem(self, rhs: O) -> Self::Output { self.0.rem(rhs.into().0).into() }
    }

    impl<T: Flags, O: Into<Self>> RemAssign<O> for OrdFlagSet<T> {
        fn rem_assign(&mut self, rhs: O) { let _ = std::mem::replace(self, (*self).rem(rhs)); }
    }

    impl<T: Flags, O: Into<Self>> Sub<O> for OrdFlagSet<T> {
        type Output = Self;

        fn sub(self, rhs: O) -> Self::Output { self.0.sub(rhs.into().0).into() }
    }

    impl<T: Flags, O: Into<Self>> SubAssign<O> for OrdFlagSet<T> {
        fn sub_assign(&mut self, rhs: O) { let _ = std::mem::replace(self, (*self).sub(rhs)); }
    }

    impl<T: Flags> OrdFlagSet<T> {
        pub fn bits(self) -> T::Type { self.0.bits() }

        pub fn clear(&mut self) { self.0.clear() }

        pub fn contains(self, rhs: impl Into<Self>) -> bool { self.0.contains(rhs.into().0) }

        pub fn drain(&mut self) -> impl Iterator<Item = T> { self.0.drain() }

        #[must_use]
        pub fn full() -> Self { Self(FlagSet::<T>::full()) }

        pub fn is_disjoint(self, rhs: impl Into<Self>) -> bool { self.0.is_disjoint(rhs.into().0) }

        pub fn is_empty(self) -> bool { self.0.is_empty() }

        pub fn is_full(self) -> bool { self.0.is_full() }

        pub fn new(bits: T::Type) -> Result<Self, InvalidBits> { Ok(Self(FlagSet::new(bits)?)) }

        pub fn new_truncated(bits: T::Type) -> Self { Self(FlagSet::new_truncated(bits)) }

        pub fn retain(&mut self, func: impl Fn(T) -> bool) { self.0.retain(func) }

        pub fn as_inner(&self) -> T::Type { self.0.bits() }
    }




    #[derive(Hash, PartialEq, Eq, Clone, Copy, Debug, serde::Serialize, Ord, PartialOrd)]
    pub enum RunAs {
        Invalid,
        Account(&'static Account),
        AccountHandle(AccountHandle),
        Privilege(PrivilegeLevel),
    }

    impl RunAs {
        pub fn inherit_capabilities(&self, device: DeviceHandle) -> PaganResult<BTreeSet<AttackerCapability>> {
            let mut wrapped = BTreeSet::new();
            if let Self::Account(acc) = self {
                if let Some(caps) = acc.capabilities().get(&device) {
                    for capability in caps {
                        wrapped.insert(AttackerCapability::new_inherited(
                            (*capability).as_ref(),
                            acc.sid()
                                .ok_or(PaganError::TopologyError("Account without a sid detected.".into()))?,
                        ));
                    }
                }
            };
            Ok(wrapped)
        }
    }
}
