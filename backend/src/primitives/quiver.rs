use std::collections::HashMap;

use once_cell::sync::Lazy;

use crate::primitives::{
    adversary::{ActionDescription, UsageScheme},
    capabilities::CapabilityTemplate,
    enablers::EnablerTemplate,
    predicate::{predicate_builders, predicate_builders::Builder, Predicate},
    shared::{
        enums::{
            CapabilitySpecifier,
            CapabilityType,
            CredentialOrigin,
            CredentialType,
            EnablerImpact,
            EnablerLocality,
            LimitType,
            PrivilegeLevel,
            PrivilegeOrigin,
            Specials,
            Token,
        },
        types::ActionHandle,
        utils::Counter,
    },
};

pub static ADVERSARY_ACTION_MAP: Lazy<HashMap<ActionHandle, ActionDescription>> = Lazy::new(|| {
    let mut map = HashMap::new();

    let mut counter = Counter::default();

    /* <InitialCompromise> */

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Discover accessible devices and services".to_string(),
            aliases: vec!["T1595".to_string(), "T1592".to_string(), "T1590".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::AccessViaNetwork.into(),
                capabilities_required: predicate_builders::All::build_from(vec![CapabilityTemplate {
                    to:              CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::Service,
                    limit:           LimitType::Minimum,
                    device:          None,
                }]),
                capabilities_provided: vec![],
                enabler_required:      None,
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::None,
                capability_specifier:  CapabilitySpecifier::None,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Smuggle infected media".to_string(),
            aliases: vec![],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::PhysicalAccess.into(),
                grant:                 Token::InfectedRemovableMedia.into(),
                capabilities_required: predicate_builders::Empty::build_from(()),
                capabilities_provided: vec![],
                enabler_required:      None,
                except_if:             predicate_builders::All::build_from([Token::AccessInternalNetwork].as_slice()),
                privilege_origin:      PrivilegeOrigin::None,
                capability_specifier:  CapabilitySpecifier::None,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Exploit open application".to_string(),
            aliases: vec!["T1190".to_string()],
            remote:  true,
            uses:    vec![
                UsageScheme {
                    given:                 Token::AccessViaNetwork.into(),
                    grant:                 Token::Session | Token::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([EnablerImpact::ArbitraryCodeExecution].as_slice()),
                    )),
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::None,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::AccessViaNetwork.into(),
                    grant:                 Token::Capability.into(),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from(vec![
                            predicate_builders::Not::build_from(EnablerImpact::ArbitraryCodeExecution),
                            predicate_builders::NotJust::build_from([EnablerImpact::Allow].as_slice()),
                        ]),
                    )),
                    except_if:             Predicate::Empty,
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::Enabler,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Exploit authed application".to_string(),
            aliases: vec!["T1190".to_string()],
            remote:  true,
            uses:    vec![
                UsageScheme {
                    given:                 Token::AccessViaNetwork | Token::Credential,
                    grant:                 Token::Session | Token::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([EnablerImpact::ArbitraryCodeExecution].as_slice()),
                    )),
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::None,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::AccessViaNetwork | Token::Credential,
                    grant:                 Token::Capability.into(),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from(vec![
                            predicate_builders::Not::build_from(EnablerImpact::ArbitraryCodeExecution),
                            predicate_builders::NotJust::build_from([EnablerImpact::Allow].as_slice()),
                        ]),
                    )),
                    except_if:             Predicate::Empty,
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::Enabler,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Drive-by compromise".to_string(),
            aliases: vec!["T1190".to_string()],
            remote:  true,
            uses:    vec![
                UsageScheme {
                    given:                 Token::None.into(),
                    grant:                 Token::Session | Token::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([EnablerImpact::ArbitraryCodeExecution].as_slice()),
                    )),
                    except_if:             predicate_builders::Any::build_from(
                        [Token::AccessInternalNetwork].as_slice(),
                    ),
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::None,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::None.into(),
                    grant:                 Token::Capability.into(),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from(vec![
                            predicate_builders::Not::build_from(EnablerImpact::ArbitraryCodeExecution),
                            predicate_builders::NotJust::build_from([EnablerImpact::Allow].as_slice()),
                        ]),
                    )),
                    except_if:             predicate_builders::Any::build_from(
                        [Token::AccessInternalNetwork].as_slice(),
                    ),
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::Enabler,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Malware phishing".to_string(),
            aliases: vec!["T1566".to_string()],
            remote:  true,
            uses:    vec![
                UsageScheme {
                    given:                 Token::ExternalPhishingMail.into(),
                    grant:                 Token::Session | Token::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      None,
                    except_if:             predicate_builders::Any::build_from(
                        [Token::AccessInternalNetwork, Token::AccessDmzSegment].as_slice(),
                    ),
                    privilege_origin:      PrivilegeOrigin::RandomUser,
                    capability_specifier:  CapabilitySpecifier::None,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::InternalPhishingMail.into(),
                    grant:                 Token::Session | Token::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      None,
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::RandomUser,
                    capability_specifier:  CapabilitySpecifier::None,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Supply chain compromise".to_string(),
            aliases: vec!["T1200".to_string(), "T1090".to_string(), "T1195".to_string()],
            remote:  true,
            uses:    vec![UsageScheme {
                given:                 Token::PreAttackBackdoor.into(),
                grant:                 Token::Session | Token::CapabilityAll,
                capabilities_required: predicate_builders::Empty::build_from(()),
                capabilities_provided: vec![],
                enabler_required:      None,
                except_if:             predicate_builders::Any::build_from([Token::AccessInternalNetwork].as_slice()),
                privilege_origin:      PrivilegeOrigin::Service,
                capability_specifier:  CapabilitySpecifier::None,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Valid accounts".to_string(),
            aliases: vec!["T1133".to_string(), "T1078".to_string()],
            remote:  true,
            uses:    vec![
                UsageScheme {
                    given:                 Token::AccessViaNetwork | Token::Credential,
                    grant:                 Token::Session | Token::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      None,
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Credential,
                    capability_specifier:  CapabilitySpecifier::None,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::AccessViaNetwork | Token::Credential | Token::CredentialMfa,
                    grant:                 Token::Session | Token::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      None,
                    except_if:             predicate_builders::Any::build_from([Token::NoMfaAllowed].as_slice()),
                    privilege_origin:      PrivilegeOrigin::Credential,
                    capability_specifier:  CapabilitySpecifier::None,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::AccessViaNetwork.into(),
                    grant:                 Token::Capability.into(),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::NotJust::build_from([EnablerImpact::Allow].as_slice()),
                    )),
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Credential,
                    capability_specifier:  CapabilitySpecifier::Enabler,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    /* </InitialCompromise> */

    /* <Persistence> */

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Persistence with backdoor".to_string(),
            aliases: vec!["T1053".to_string(), "T1547".to_string(), "T1037".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::Capability.into(),
                capabilities_required: predicate_builders::All::build_from(
                    [CapabilityTemplate {
                        to:              CapabilityType::CodeExecution,
                        privilege_level: PrivilegeLevel::Service,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }]
                    .as_slice(),
                ),
                capabilities_provided: vec![CapabilityTemplate {
                    to:              CapabilityType::Persist,
                    privilege_level: PrivilegeLevel::Service,
                    limit:           LimitType::Minimum,
                    device:          None,
                }],
                enabler_required:      None,
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::InheritUser,
                capability_specifier:  CapabilitySpecifier::Action,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Persistence with user".to_string(),
            aliases: vec!["T1098".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::CapabilityAll | Token::Credential,
                capabilities_required: predicate_builders::All::build_from(
                    [CapabilityTemplate {
                        to:              CapabilityType::AddUser,
                        privilege_level: PrivilegeLevel::User,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }]
                    .as_slice(),
                ),
                capabilities_provided: vec![CapabilityTemplate {
                    to:              CapabilityType::Persist,
                    privilege_level: PrivilegeLevel::User,
                    limit:           LimitType::Minimum,
                    device:          None,
                }],
                enabler_required:      None,
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::InheritUser,
                capability_specifier:  CapabilitySpecifier::None,
                credential_origin:     CredentialOrigin::NewUser,
                credential_type:       CredentialType::Os,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    /* </Persistence> */

    /* <PrivilegeEscalation> */

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Abuse Elevation Control Mechanism: Setuid and Setgid".to_string(),
            aliases: vec!["T1548.001".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::Capability.into(),
                capabilities_required: predicate_builders::All::build_from(
                    [CapabilityTemplate {
                        to:              CapabilityType::CodeExecution,
                        privilege_level: PrivilegeLevel::Service,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }]
                    .as_slice(),
                ),
                capabilities_provided: vec![],
                enabler_required:      Some(EnablerTemplate::new(
                    EnablerLocality::Local,
                    predicate_builders::All::build_from([EnablerImpact::Allow].as_slice()),
                )),
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::Service,
                capability_specifier:  CapabilitySpecifier::Service,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Abuse Elevation Control Mechanism: Bypass User Account Control".to_string(),
            aliases: vec!["T1548.002".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::Capability.into(),
                capabilities_required: predicate_builders::All::build_from(vec![
                    CapabilityTemplate {
                        to:              CapabilityType::CodeExecution,
                        privilege_level: PrivilegeLevel::User,
                        limit:           LimitType::Exact,
                        device:          None,
                    },
                    CapabilityTemplate {
                        to:              CapabilityType::UacElevate,
                        privilege_level: PrivilegeLevel::User,
                        limit:           LimitType::Exact,
                        device:          None,
                    },
                ]),
                capabilities_provided: vec![],
                enabler_required:      Some(EnablerTemplate::new(
                    EnablerLocality::Local,
                    predicate_builders::Any::build_from(vec![
                        EnablerImpact::ArbitraryCodeExecution,
                        EnablerImpact::DataManipulation,
                        EnablerImpact::MachineConfigurationTampering,
                    ]),
                )),
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::ElevatedUser,
                capability_specifier:  CapabilitySpecifier::Enabler,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   Some(
                r#"
                Usages are based on the https://github.com/hfiref0x/UACME techniques (click "Keys" in readme)
                * case A: application shimming requires admin priv to create shim, aka no effective privesc
                * case B: Dll Hijack -> see component hijack on filesys
                * case C: shell API - again depends on a high priv or autoElevating process to grab data/run
                * command from user controlled registry/env -> same as component hijack path
                * case D: Exposed elevated COM interface - the only standalone, implemented in this action
                "#
                .to_string(),
            ),
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
                name: "Abuse Elevation Control Mechanism: Sudoers".to_string(),
                aliases: vec!["T1548.003".to_string()],
                remote: false,
                uses: vec![
                    UsageScheme {
                        given: Token::Session | Token::Capability,
                        grant: Token::Capability.into(),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::CodeExecution,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Exact,
                                device: None,
                            }
                        ].as_slice()),
                        capabilities_provided: vec![],
                        enabler_required: Some(
                            EnablerTemplate::new(
                                EnablerLocality::Local,
                                predicate_builders::NotJust::build_from([EnablerImpact::Allow].as_slice())
                            )
                        ),
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::ElevatedUser,
                        capability_specifier: CapabilitySpecifier::Enabler,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                        specials: vec![],
                    },
                    UsageScheme {
                        given: Token::Session | Token::Capability,
                        grant: Token::CapabilityAll.into(),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::CodeExecution,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Exact,
                                device: None,
                            }
                        ].as_slice()),
                        capabilities_provided: vec![],
                        enabler_required: Some(
                            EnablerTemplate::new(
                                EnablerLocality::Local,
                                predicate_builders::All::build_from([EnablerImpact::Allow].as_slice())
                            )
                        ),
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                        capability_specifier: CapabilitySpecifier::None,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                        specials: vec![],
                    }
                ],
                notes: Some(r#"
                * Case A & B: user has sudo privileges without password prompt or tty isolation is turned off and attacker is lucky,
                * Case C: attacker can tamper with the sudoers file
                "#.to_string()),
            },
        );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Access Token Manipulation: Theft, Impersonation, PPID spoofing".to_string(),
            aliases: vec![
                "T1134.001".to_string(),
                "T1134.002".to_string(),
                "T1134.003".to_string(),
            ],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::CapabilityAll.into(),
                capabilities_required: predicate_builders::All::build_from(vec![CapabilityTemplate {
                    to:              CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::User,
                    limit:           LimitType::Minimum,
                    device:          None,
                }]),
                capabilities_provided: vec![],
                enabler_required:      None,
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                capability_specifier:  CapabilitySpecifier::None,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![
                    (Specials::SeCreateSecurityToken, PrivilegeLevel::Administrator),
                    (Specials::SeImpersonateToken, PrivilegeLevel::Administrator),
                ],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Privileged autoexecution".to_string(),
            aliases: vec!["T1547".to_string()],
            remote:  false,
            uses:    vec![
                UsageScheme {
                    given:                 Token::Session | Token::Capability,
                    grant:                 Token::CapabilityAll.into(),
                    capabilities_required: predicate_builders::Any::build_from(vec![
                        predicate_builders::Any::build_from(vec![
                            CapabilityTemplate {
                                to:              CapabilityType::CodeExecution,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit:           LimitType::Minimum,
                                device:          None,
                            },
                            CapabilityTemplate {
                                to:              CapabilityType::ConfigLocalMachine,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit:           LimitType::Minimum,
                                device:          None,
                            },
                        ]),
                        predicate_builders::All::build_from(vec![
                            CapabilityTemplate {
                                to:              CapabilityType::AccessFileSysElevated,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit:           LimitType::Minimum,
                                device:          None,
                            },
                            CapabilityTemplate {
                                to:              CapabilityType::WriteFiles,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit:           LimitType::Minimum,
                                device:          None,
                            },
                        ]),
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to:              CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::LocalSystem,
                        limit:           LimitType::Maximum,
                        device:          None,
                    }],
                    enabler_required:      None,
                    except_if:             Predicate::Empty,
                    privilege_origin:      PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                    capability_specifier:  CapabilitySpecifier::Action,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::Session | Token::Capability,
                    grant:                 Token::CapabilityAll.into(),
                    capabilities_required: predicate_builders::All::build_from(
                        [CapabilityTemplate {
                            to:              CapabilityType::CodeExecution,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit:           LimitType::Minimum,
                            device:          None,
                        }]
                        .as_slice(),
                    ),
                    capabilities_provided: vec![CapabilityTemplate {
                        to:              CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::LocalSystem,
                        limit:           LimitType::Maximum,
                        device:          None,
                    }],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Local,
                        predicate_builders::All::build_from([EnablerImpact::Allow].as_slice()),
                    )),
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                    capability_specifier:  CapabilitySpecifier::Action,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Create or Modify System Process, Event Triggered Execution: Accessibility Features, Image File \
                      Execution Options Injection"
                .to_string(),
            aliases: vec!["T1543".to_string(), "T1546.012".to_string(), "T1546.008".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::CapabilityAll.into(),
                capabilities_required: predicate_builders::All::build_from(
                    [CapabilityTemplate {
                        to:              CapabilityType::ConfigLocalMachine,
                        privilege_level: PrivilegeLevel::Administrator,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }]
                    .as_slice(),
                ),
                capabilities_provided: vec![CapabilityTemplate {
                    to:              CapabilityType::Persist,
                    privilege_level: PrivilegeLevel::Administrator,
                    limit:           LimitType::Minimum,
                    device:          None,
                }],
                enabler_required:      None,
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                capability_specifier:  CapabilitySpecifier::Action,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Exploitation for privilege escalation".to_string(),
            aliases: vec!["T1068".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::Capability.into(),
                capabilities_required: predicate_builders::All::build_from(
                    [CapabilityTemplate {
                        to:              CapabilityType::CodeExecution,
                        privilege_level: PrivilegeLevel::Service,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }]
                    .as_slice(),
                ),
                capabilities_provided: vec![],
                enabler_required:      Some(EnablerTemplate::new(
                    EnablerLocality::Local,
                    predicate_builders::Any::build_from(vec![
                        EnablerImpact::ArbitraryCodeExecution,
                        EnablerImpact::MachineConfigurationTampering,
                        EnablerImpact::DataManipulation,
                    ]),
                )),
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::Service,
                capability_specifier:  CapabilitySpecifier::Enabler,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Exploitation for Privilege Escalation - Bring Your Own Vulnerable Driver".to_string(),
            aliases: vec!["T1068".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::Capability.into(),
                capabilities_required: predicate_builders::All::build_from(vec![CapabilityTemplate {
                    to:              CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::Administrator,
                    limit:           LimitType::Minimum,
                    device:          None,
                }]),
                capabilities_provided: vec![CapabilityTemplate {
                    to:              CapabilityType::Persist,
                    privilege_level: PrivilegeLevel::LocalSystem,
                    limit:           LimitType::Maximum,
                    device:          None,
                }],
                enabler_required:      Some(EnablerTemplate::new(
                    EnablerLocality::Local,
                    predicate_builders::Any::build_from(vec![
                        EnablerImpact::ArbitraryCodeExecution,
                        EnablerImpact::MachineConfigurationTampering,
                        EnablerImpact::DataManipulation,
                    ]),
                )),
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                capability_specifier:  CapabilitySpecifier::Enabler,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![(Specials::SeLoadDriver, PrivilegeLevel::Administrator)],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Process Injection".to_string(),
            aliases: vec!["T1055".to_string(), "CAPEC-640".to_string()],
            remote:  false,
            uses:    vec![UsageScheme {
                given:                 Token::Session | Token::Capability,
                grant:                 Token::CapabilityAll.into(),
                capabilities_required: predicate_builders::All::build_from(vec![CapabilityTemplate {
                    to:              CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::User,
                    limit:           LimitType::Minimum,
                    device:          None,
                }]),
                capabilities_provided: vec![],
                enabler_required:      None,
                except_if:             predicate_builders::Empty::build_from(()),
                privilege_origin:      PrivilegeOrigin::Service,
                capability_specifier:  CapabilitySpecifier::None,
                credential_origin:     CredentialOrigin::None,
                credential_type:       CredentialType::None,
                specials:              vec![(Specials::SeDebug, PrivilegeLevel::Administrator)],
            }],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Hijacking Components (Dlls, executables) on the filesystem, Unquoted component paths".to_string(),
            aliases: vec![
                "T1574.001".to_string(),
                "T1574.002".to_string(),
                "T1574.008".to_string(),
                "T1574.009".to_string(),
                "T1574.005".to_string(),
            ],
            remote:  false,
            uses:    vec![
                UsageScheme {
                    given:                 Token::Session | Token::Capability,
                    grant:                 Token::CapabilityAll.into(),
                    capabilities_required: predicate_builders::All::build_from(vec![
                        CapabilityTemplate {
                            to:              CapabilityType::AccessFileSysElevated,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                        CapabilityTemplate {
                            to:              CapabilityType::WriteFiles,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to:              CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::Service,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }],
                    enabler_required:      None,
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                    capability_specifier:  CapabilitySpecifier::Action,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::Session | Token::Capability,
                    grant:                 Token::CapabilityAll.into(),
                    capabilities_required: predicate_builders::All::build_from(vec![
                        CapabilityTemplate {
                            to:              CapabilityType::AccessFileSysGeneric,
                            privilege_level: PrivilegeLevel::User,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                        CapabilityTemplate {
                            to:              CapabilityType::WriteFiles,
                            privilege_level: PrivilegeLevel::User,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to:              CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::Service,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Local,
                        predicate_builders::All::build_from([EnablerImpact::ArbitraryCodeExecution].as_slice()),
                    )),
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::Action,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    map.insert(
        counter.give(),
        ActionDescription {
            name:    "Hijack component path or search order, AppCert and AppInit DLL spoofing".to_string(),
            aliases: vec![
                "T1574.006".to_string(),
                "T1574.007".to_string(),
                "T1574.001".to_string(),
                "T1574.011".to_string(),
                "T1546.009".to_string(),
                "T1546.010".to_string(),
            ],
            remote:  false,
            uses:    vec![
                UsageScheme {
                    given:                 Token::Session | Token::Capability,
                    grant:                 Token::CapabilityAll.into(),
                    capabilities_required: predicate_builders::All::build_from(vec![
                        CapabilityTemplate {
                            to:              CapabilityType::AccessFileSysGeneric,
                            privilege_level: PrivilegeLevel::User,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                        CapabilityTemplate {
                            to:              CapabilityType::WriteFiles,
                            privilege_level: PrivilegeLevel::User,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                        CapabilityTemplate {
                            to:              CapabilityType::ConfigLocalMachine,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to:              CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::Service,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }],
                    enabler_required:      None,
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                    capability_specifier:  CapabilitySpecifier::Action,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
                UsageScheme {
                    given:                 Token::Session | Token::Capability,
                    grant:                 Token::CapabilityAll.into(),
                    capabilities_required: predicate_builders::All::build_from(vec![
                        CapabilityTemplate {
                            to:              CapabilityType::AccessFileSysGeneric,
                            privilege_level: PrivilegeLevel::Service,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                        CapabilityTemplate {
                            to:              CapabilityType::WriteFiles,
                            privilege_level: PrivilegeLevel::Service,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                        CapabilityTemplate {
                            to:              CapabilityType::ConfigCurrentUser,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit:           LimitType::Minimum,
                            device:          None,
                        },
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to:              CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::Service,
                        limit:           LimitType::Minimum,
                        device:          None,
                    }],
                    enabler_required:      Some(EnablerTemplate::new(
                        EnablerLocality::Local,
                        predicate_builders::All::build_from([EnablerImpact::ArbitraryCodeExecution].as_slice()),
                    )),
                    except_if:             predicate_builders::Empty::build_from(()),
                    privilege_origin:      PrivilegeOrigin::Service,
                    capability_specifier:  CapabilitySpecifier::Action,
                    credential_origin:     CredentialOrigin::None,
                    credential_type:       CredentialType::None,
                    specials:              vec![],
                },
            ],
            notes:   None,
        },
    );

    /* </PrivilegeEscalation> */

    /* <CredentialAccess> */

    // map.insert(
    //     counter.next(),
    //     ActionDescription {
    //         name: "".to_string(),
    //         aliases: vec![],p
    //         remote: false,
    //         uses: vec![],
    //         notes: None,
    //     }
    // );
    map.values()
        .for_each(|action| action.uses.iter().for_each(|scheme| scheme.validate()));
    map
});
