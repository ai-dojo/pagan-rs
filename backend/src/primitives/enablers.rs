use flagset::FlagSet;
use gringotts;

use crate::primitives::{
    predicate::Predicate,
    shared::{
        enums::{EnablerImpact, EnablerLocality},
        types::{ImpactRepr, ServiceHandle},
    },
};

/**
Enabler template for use in Quiver action descriptions.
**/
pub struct EnablerTemplate {
    locality: EnablerLocality,
    impact:   Predicate,
}

impl EnablerTemplate {
    #[must_use]
    pub const fn new(locality: EnablerLocality, impact: Predicate) -> Self { Self { locality, impact } }

    #[must_use]
    pub const fn locality(&self) -> &EnablerLocality { &self.locality }

    #[must_use]
    pub const fn impact(&self) -> &Predicate { &self.impact }
}

/**
Lightweight Enabler representation for computations and in-memory storing.
**/
#[derive(Debug)]
pub struct LightEnabler {
    main_software:  ServiceHandle,
    impact:         FlagSet<EnablerImpact>,
    software_setup: Vec<ServiceHandle>,
    locality:       EnablerLocality,
}

impl LightEnabler {
    #[must_use]
    pub fn matches_template(&self, template: &EnablerTemplate) -> bool {
        if self.locality != template.locality {
            return false;
        }

        template.impact().eval_impacts(self.impact)
    }

    #[must_use]
    pub fn impact(&self) -> ImpactRepr { self.impact.bits() }
}

impl From<gringotts::Enabler> for LightEnabler {
    fn from(_value: gringotts::Enabler) -> Self { todo!() }
}
