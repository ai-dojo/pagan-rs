use std::{
    borrow::{Borrow, Cow},
    collections::BTreeSet,
    hash::Hash,
    ops::BitOr,
};

use flagset::FlagSet;

use crate::{
    error::PaganError,
    primitives::{
        accounts::Account,
        person::Person,
        shared::{
            enums::CredentialType,
            traits::{ComplexStrength, IntoBorrowedCow},
            types::{DeviceHandle, OrdFlagSet, PaganResult, ServiceHandle},
            utils::{HandleSet, SetOption, SetOptionCombinatorOps},
        },
    },
};


macro_rules! impl_matches_combined {
    () => {
        #[must_use]
        pub fn matches_combined_template(&self, templates: &[&ServiceCredential]) -> bool {
            // let mut services_accumulator = HandleSet::Services(SetOption::None);
            return !self.r#type.is_disjoint(
                templates
                    .iter()
                    .fold(OrdFlagSet::new(0).unwrap(), |accumulator, elem| {
                        accumulator.bitor(elem.r#type())
                    }),
            ) && self.services.has_intersection(&templates.iter().fold(
                HandleSet::Services(SetOption::None),
                |mut accumulator, elem| {
                    accumulator.intersection(elem.services()).unwrap();
                    accumulator
                },
            ));
        }
    };
}

/**
Chooses the most fitting credential chain of the possibilities.
Each element of a chain is a factor of authentication.
**/
macro_rules! has_chain {
    ($cred:expr, $paren_collection:ident) => {{
        let mut actual_cred = (*$cred).borrow();
        loop {
            match actual_cred {
                Credential::System(sc) => match sc.next_in_chain() {
                    None => break $paren_collection.contains(&Cow::Borrowed(actual_cred)),
                    Some(next_cred) => {
                        actual_cred = next_cred;
                    },
                },
                _ => break false,
            }
        }
    }};
}

macro_rules! find_chain {
    ($ordered_iter:expr, $parent_collection:ident, $services:ident) => {{
        loop {
            let actual = $ordered_iter.next();
            match actual {
                Some(cred) => match (*cred).borrow() {
                    Credential::System(_) => {
                        if has_chain!(cred, $parent_collection) {
                            break Some(cred.make_borrowed());
                        }
                    },
                    Credential::SplitMultiCred { .. } => break Some(cred.make_borrowed()),
                    Credential::Service(_) if !cred.r#type().is_disjoint(CredentialType::Mfa) => {
                        break Some(cred.make_borrowed())
                    },
                    Credential::Service(sc) => {
                        if let Some(pair) = $parent_collection
                            .iter()
                            .filter(|item| !item.r#type().is_disjoint(CredentialType::Mfa))
                            .max_by_key(|mfa_item| mfa_item.services(true).intersection_cardinality(sc.services()))
                        {
                            let matching_mfa = match (*pair).borrow() {
                                Credential::SplitMultiCred { primary: _primary, mfa } => mfa,
                                Credential::Service(sc) => sc,
                                _ => continue,
                            };
                            break Some(Cow::Owned(Credential::SplitMultiCred {
                                primary: Cow::Borrowed(unsafe {
                                    std::mem::transmute::<&ServiceCredential, &'static ServiceCredential>(sc)
                                }),
                                mfa:     Cow::Borrowed(unsafe {
                                    std::mem::transmute::<&ServiceCredential, &'static ServiceCredential>(matching_mfa)
                                }),
                            }));
                        }
                    },
                },
                None => break None,
            }
        }
    }};
}

/**
An existing credential in the infrastructure (domain).
**/
#[derive(Debug, Hash, PartialEq, Eq, serde::Serialize, PartialOrd, Clone, Ord)]
pub struct SystemCredential {
    r#type:                  OrdFlagSet<CredentialType>,
    services:                HandleSet,
    accounts:                Vec<&'static Account>,
    previous_in_chain:       Option<&'static Credential>,
    next_in_chain:           Option<&'static Credential>,
    unseal_complexity:       u8, // 0 - 100
    online_guess_complexity: u8, // 0 - 100
}

impl SystemCredential {
    impl_matches_combined!();

    pub fn new(
        r#type: FlagSet<CredentialType>,
        service: SetOption<ServiceHandle>,
        accounts: Vec<&'static Account>,
        previous_in_chain: Option<&'static Credential>,
        next_in_chain: Option<&'static Credential>,
        unseal_complexity: u8,
        online_guess_complexity: u8,
    ) -> PaganResult<Self> {
        if !r#type.is_disjoint(CredentialType::None | CredentialType::AppPrimary) {
            return Err(PaganError::InvalidCallError(
                "System credential can only be of OS or MFA type.".into(),
            ));
        }
        Ok(Self {
            r#type: r#type.into(),
            services: HandleSet::Services(service),
            accounts,
            previous_in_chain,
            next_in_chain,
            unseal_complexity,
            online_guess_complexity,
        })
    }

    #[must_use]
    pub const fn r#type(&self) -> OrdFlagSet<CredentialType> { self.r#type }

    #[must_use]
    pub const fn services(&self) -> &HandleSet { &self.services }

    #[must_use]
    pub const fn accounts(&self) -> &Vec<&Account> { &self.accounts }

    #[must_use]
    pub const fn previous_in_chain(&self) -> Option<&Credential> { self.previous_in_chain }

    #[must_use]
    pub const fn next_in_chain(&self) -> Option<&Credential> { self.next_in_chain }

    #[must_use]
    pub const fn unseal_complexity(&self) -> u8 { self.unseal_complexity }

    #[must_use]
    pub const fn online_guess_complexity(&self) -> u8 { self.online_guess_complexity }

    /**
    Returns true if the provided wildcard credential can be substituted by this instance.
    **/
    #[must_use]
    pub fn is_for_device(&self, device: DeviceHandle) -> bool {
        self.accounts
            .iter()
            .any(|account| account.capabilities().keys().any(|dev| dev == &device))
    }
}

#[derive(Debug, PartialEq, Hash, Eq, serde::Serialize, PartialOrd, Clone, Ord)]
pub struct ServiceCredential {
    services: HandleSet,
    r#type:   OrdFlagSet<CredentialType>,
    holder:   Option<&'static Person>,
    binding:  bool,
}

impl ServiceCredential {
    impl_matches_combined!();

    pub fn new(
        services: HandleSet,
        r#type: FlagSet<CredentialType>,
        holder: Option<&'static Person>,
        binding: bool,
    ) -> PaganResult<Self> {
        if !r#type.is_disjoint(CredentialType::Os | CredentialType::None) {
            return Err(PaganError::InvalidCallError(
                "ServiceCredential must be MFA or App".into(),
            ));
        }
        Ok(Self {
            services,
            r#type: r#type.into(),
            holder,
            binding,
        })
    }

    #[must_use]
    pub const fn services(&self) -> &HandleSet { &self.services }

    #[must_use]
    pub const fn is_for_device(&self, _device: DeviceHandle) -> bool { true }

    #[must_use]
    pub const fn r#type(&self) -> OrdFlagSet<CredentialType> { self.r#type }

    #[must_use]
    pub fn weak_match(&self, rhs: &Self) -> bool {
        !self.r#type().is_disjoint(rhs.r#type()) && self.services.has_intersection(rhs.services())
    }

    #[must_use]
    pub const fn holder(&self) -> Option<&'static Person> { self.holder }

    #[must_use]
    pub const fn binding(&self) -> bool { self.binding }
}

#[derive(Debug, PartialEq, Hash, Eq, serde::Serialize, PartialOrd, Clone, Ord)]
pub enum Credential {
    System(SystemCredential),
    Service(ServiceCredential),
    SplitMultiCred {
        primary: Cow<'static, ServiceCredential>,
        mfa:     Cow<'static, ServiceCredential>,
    },
}

impl Credential {
    #[must_use]
    pub fn devices(&self) -> SetOption<DeviceHandle> {
        match self {
            Self::System(sc) => sc
                .accounts
                .iter()
                .flat_map(|acc| acc.devices())
                .collect::<SetOption<DeviceHandle>>(),
            _ => SetOption::All,
        }
    }

    #[must_use]
    pub fn r#type(&self) -> OrdFlagSet<CredentialType> {
        match self {
            Self::System(sc) => sc.r#type(),
            Self::Service(sc) => sc.r#type(),
            Self::SplitMultiCred { primary, mfa } => primary.r#type().bitor(mfa.r#type()),
        }
    }

    #[must_use]
    pub fn services(&self, mfa_required: bool) -> Cow<HandleSet> {
        match self {
            Self::System(sc) => Cow::Borrowed(sc.services()),
            Self::Service(sc) => Cow::Borrowed(sc.services()),
            Self::SplitMultiCred { primary, mfa } => {
                if mfa_required {
                    let mut cloned_s = primary.services().clone();
                    cloned_s.intersection(mfa.services()).unwrap();
                    Cow::Owned(cloned_s)
                } else {
                    Cow::Borrowed(primary.services())
                }
            },
        }
    }

    #[must_use]
    pub fn matching(&self, rhs: &Self) -> bool {
        match self {
            Self::System(self_sys) => match rhs {
                Self::System(other_sys) => self_sys == other_sys,
                Self::Service(_) => false,
                Self::SplitMultiCred { primary, mfa } => {
                    self_sys.matches_combined_template(&[primary.borrow(), mfa.borrow()])
                },
            },
            Self::Service(self_serv) => match rhs {
                Self::System(_) => false,
                Self::Service(other_serv) => self_serv == other_serv,
                Self::SplitMultiCred { primary, mfa } => {
                    self_serv.matches_combined_template(&[primary.borrow(), mfa.borrow()])
                },
            },
            Self::SplitMultiCred {
                primary: my_primary,
                mfa: my_mfa,
            } => match rhs {
                Self::System(sc) => sc.matches_combined_template(&[my_primary.borrow(), my_mfa.borrow()]),
                Self::Service(sc) => sc.matches_combined_template(&[my_primary.borrow(), my_mfa.borrow()]),
                Self::SplitMultiCred {
                    primary: other_primary,
                    mfa: other_mfa,
                } => my_primary.weak_match(other_primary) && my_mfa.weak_match(other_mfa),
            },
        }
    }

    #[must_use]
    pub fn is_for_device(&self, device: DeviceHandle) -> bool {
        match self {
            Self::System(sc) => sc.is_for_device(device),
            _ => false,
        }
    }

    /**
    Select the most appropriate credentials from the provided collection.
    Only used when the credential has an impact on the obtained capabilities for the attacker.
    **/
    #[must_use]
    pub fn select_strongest(
        credentials: BTreeSet<&Cow<'static, Self>>,
        for_device: DeviceHandle,
        for_services: &HandleSet,
        device_auth: bool,
        needs_mfa: bool,
    ) -> (Option<Cow<'static, Self>>, Option<Cow<'static, Self>>) {
        let mut local_ord = credentials
            .iter()
            .filter(|cred| {
                !cred.r#type().is_disjoint(if device_auth {
                    CredentialType::Os
                } else {
                    CredentialType::AnyPrimary
                }) && cred.is_for_device(for_device)
                    && cred.services(needs_mfa).has_intersection(for_services)
            })
            .map(|double_ref| *double_ref)
            .collect::<Vec<&Cow<Self>>>();

        let mut global_ord = Vec::new();

        if device_auth {
            global_ord = local_ord
                .iter()
                .map(|double_ref| *double_ref)
                .collect::<Vec<&Cow<Self>>>();
            local_ord.sort_by(|cred_a, cred_b| {
                cred_a
                    .strength(Some(for_device))
                    .cmp(&cred_b.strength(Some(for_device)))
            });

            global_ord.sort_by(|cred_a, cred_b| {
                cred_a
                    .strength(None as Option<DeviceHandle>)
                    .cmp(&cred_b.strength(None as Option<DeviceHandle>))
            });
        } else {
            local_ord.sort_by(|cred_a, cred_b| {
                cred_a
                    .services(needs_mfa)
                    .intersection_cardinality(for_services)
                    .cmp(&cred_b.services(needs_mfa).intersection_cardinality(for_services))
            });
        }

        if !needs_mfa {
            return (
                local_ord.get(1).map(|specimen| specimen.make_borrowed()),
                if device_auth {
                    global_ord.get(1).map(|specimen| specimen.make_borrowed())
                } else {
                    local_ord.get(1).map(|specimen| specimen.make_borrowed())
                },
            );
        }

        let local_best = find_chain!(local_ord.iter(), credentials, services);
        let global_best = if device_auth {
            find_chain!(global_ord.iter(), credentials, services)
        } else {
            None
        };

        (local_best, global_best)
    }
}

impl ComplexStrength for Credential {
    fn strength(&self, target: Option<DeviceHandle>) -> u64 {
        match self {
            Self::System(sc) => sc
                .accounts
                .iter()
                .fold(0, |accumulator, item| accumulator + item.strength(target)),
            _ => 1,
        }
    }
}
