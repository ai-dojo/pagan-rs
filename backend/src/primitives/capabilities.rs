use std::borrow::Cow;

use crate::primitives::shared::{
    enums::{CapabilityType, LimitType, PrivilegeLevel},
    traits::PrimitiveStrength,
    types::{AccountHandle, DeviceHandle, RunAs},
};

pub trait MatchesCapabilityTemplate {
    fn matched_by(&self, rhs: &CapabilityTemplate) -> bool;
}

#[derive(Hash, PartialEq, Eq, serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct CapabilityTemplate {
    pub to:              CapabilityType,
    pub privilege_level: PrivilegeLevel,
    pub limit:           LimitType,
    pub device:          Option<DeviceHandle>,
}

impl CapabilityTemplate {
    #[must_use]
    pub const fn to(&self) -> CapabilityType { self.to }

    #[must_use]
    pub const fn privilege_level(&self) -> PrivilegeLevel { self.privilege_level }

    #[must_use]
    pub const fn limit(&self) -> LimitType { self.limit }

    #[must_use]
    pub const fn device(&self) -> Option<DeviceHandle> { self.device }
}

impl CapabilityTemplate {
    #[must_use]
    pub const fn new(
        to: CapabilityType,
        privilege_level: PrivilegeLevel,
        limit: LimitType,
        device: Option<DeviceHandle>,
    ) -> Self {
        Self {
            to,
            privilege_level,
            limit,
            device,
        }
    }
}

impl MatchesCapabilityTemplate for CapabilityTemplate {
    fn matched_by(&self, rhs: &Self) -> bool {
        if self.to != rhs.to {
            return false;
        };

        if let Some(lhs_dev) = &self.device {
            if let Some(rhs_dev) = &rhs.device {
                if lhs_dev != rhs_dev {
                    return false;
                };
            };
        };

        match self.limit {
            LimitType::Minimum => self.privilege_level <= rhs.privilege_level,
            LimitType::Exact => self.privilege_level == rhs.privilege_level,
            LimitType::Maximum => self.privilege_level >= rhs.privilege_level,
        }
    }
}

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone, PartialEq, Eq, Hash, Ord, PartialOrd)]
pub struct Capability {
    to:              CapabilityType,
    privilege_level: PrivilegeLevel,
    device:          DeviceHandle,
}

impl Capability {
    #[must_use]
    pub const fn new(to: CapabilityType, privilege_level: PrivilegeLevel, device: DeviceHandle) -> Self {
        Self {
            to,
            privilege_level,
            device,
        }
    }

    #[must_use]
    pub const fn to(&self) -> CapabilityType { self.to }

    #[must_use]
    pub const fn privilege_level(&self) -> PrivilegeLevel { self.privilege_level }

    #[must_use]
    pub const fn limit(&self) -> LimitType { LimitType::Exact }

    #[must_use]
    pub const fn device(&self) -> DeviceHandle { self.device }
}

impl PrimitiveStrength for Capability {
    fn strength(&self) -> u64 { self.to.strength() * self.privilege_level.strength() }
}

impl MatchesCapabilityTemplate for Capability {
    fn matched_by(&self, rhs: &CapabilityTemplate) -> bool {
        if self.to != rhs.to {
            return false;
        };

        if let Some(rhs_dev) = rhs.device() {
            if self.device() != rhs_dev {
                return false;
            };
        };

        match rhs.limit {
            LimitType::Minimum => self.privilege_level >= rhs.privilege_level,
            LimitType::Exact => self.privilege_level == rhs.privilege_level,
            LimitType::Maximum => self.privilege_level <= rhs.privilege_level,
        }
    }
}

#[derive(serde::Serialize, Debug, Clone, PartialEq, Eq, Hash, Ord, PartialOrd)]
pub struct AttackerCapability {
    base:        Cow<'static, Capability>,
    account_sid: RunAs,
}

impl MatchesCapabilityTemplate for AttackerCapability {
    fn matched_by(&self, rhs: &CapabilityTemplate) -> bool { self.base.matched_by(rhs) }
}

impl AttackerCapability {
    /**
    Wraps an existing capability.
     **/

    #[must_use]
    pub const fn new_inherited(base: &'static Capability, account_sid: AccountHandle) -> Self {
        Self {
            base:        Cow::Borrowed(base),
            account_sid: RunAs::AccountHandle(account_sid),
        }
    }

    #[must_use]
    pub const fn new(capability: Capability) -> Self {
        Self {
            account_sid: RunAs::Privilege(capability.privilege_level()),
            base:        Cow::Owned(capability),
        }
    }

    #[must_use]
    pub fn is_for_device(&self, target: DeviceHandle) -> bool { self.base.device() == target }

    #[must_use]
    pub const fn sid(&self) -> RunAs { self.account_sid }

    #[must_use]
    pub fn to(&self) -> CapabilityType { self.base.to() }
}

impl PrimitiveStrength for AttackerCapability {
    fn strength(&self) -> u64 { self.base.strength() }
}
