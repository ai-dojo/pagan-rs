#![allow(clippy::module_name_repetitions)]
use std::cmp::min;

use flagset::FlagSet;
use gringotts;

use crate::primitives::shared::{
    enums::{service_impacts_as_capabilities, CapabilityType, PrivilegeLevel, ServiceImpact},
    types::ImpactRepr,
};

/**
Lightweight service representation for computations and in-memory storing
**/
#[derive(Debug)]
pub struct LightService {
    impacts:           FlagSet<ServiceImpact>,
    executable_access: PrivilegeLevel,
    auto_elevation:    bool,
}

impl LightService {
    #[must_use]
    pub const fn get_effective_privilege(&self) -> PrivilegeLevel {
        if self.auto_elevation {
            match &self.executable_access {
                PrivilegeLevel::Administrator => PrivilegeLevel::LocalSystem,
                PrivilegeLevel::User => PrivilegeLevel::Administrator,
                _ => self.executable_access,
            }
        } else {
            self.executable_access
        }
    }

    #[must_use]
    pub fn get_running_integrity(&self, spawner_privilege: PrivilegeLevel) -> PrivilegeLevel {
        if self.auto_elevation {
            self.get_effective_privilege()
        } else {
            min(spawner_privilege, self.get_effective_privilege())
        }
    }

    #[must_use]
    pub fn impacts_as_capabilities(&self) -> Vec<CapabilityType> { service_impacts_as_capabilities(self.impacts) }

    #[must_use]
    pub fn impacts(&self) -> ImpactRepr { self.impacts.bits() }
}

impl From<gringotts::Service> for LightService {
    fn from(_value: gringotts::Service) -> Self { todo!() }
}
