use std::{
    borrow::Cow,
    collections::{BTreeMap, BTreeSet, HashMap},
    ops::BitOr,
};

use flagset::FlagSet;

use crate::{
    error::PaganError,
    primitives::{
        capabilities::{AttackerCapability, CapabilityTemplate},
        enablers::EnablerTemplate,
        predicate::{FindAtomic, Predicate},
        shared::{
            enums::{
                CapabilitySpecifier,
                CredentialOrigin,
                CredentialType,
                PrivilegeLevel,
                PrivilegeOrigin,
                Specials,
                Token,
            },
            traits::PrimitiveStrength,
            types::{DeviceHandle, PaganResult, RunAs},
        },
    },
};

macro_rules! filter_for_device {
    ($collection:expr, $device:ident) => {
        $collection
            .iter()
            .filter(|item| item.is_for_device($device))
            .collect::<BTreeSet<_>>()
    };
}

/**
Wraps the ACS, so it can be used in comparison with predicates in
a more straightforward way.
**/
#[derive(Debug)]
pub struct ArtifactSatchel<'exp> {
    tokens:       FlagSet<Token>,
    capabilities: BTreeSet<&'exp AttackerCapability>,
}

impl<'exp> ArtifactSatchel<'exp> {
    /**
    Creates a new `ArtifactSatchel` of only those artifacts that
    are applicable to the target device or are global.
    **/
    #[must_use]
    pub fn with_filter(
        tokens: &BTreeMap<Option<DeviceHandle>, FlagSet<Token>>,
        capabilities: &'exp Cow<'static, BTreeSet<AttackerCapability>>,
        device: DeviceHandle,
    ) -> Self {
        let local_tokens = tokens
            .get(&Option::from(device))
            .unwrap_or(&FlagSet::from(Token::None))
            .to_owned();
        let global_tokens = tokens.get(&None).unwrap_or(&FlagSet::from(Token::None)).to_owned();
        Self {
            tokens:       local_tokens.bitor(global_tokens),
            capabilities: filter_for_device!(capabilities, device),
        }
    }

    #[must_use]
    pub const fn tokens(&self) -> &FlagSet<Token> { &self.tokens }

    #[must_use]
    pub const fn capabilities(&self) -> &BTreeSet<&'_ AttackerCapability> { &self.capabilities }

    /**
    Computes the most appropriate set of capabilities (limited to a single user account)
    for the provided scheme.
    **/
    pub fn compute_best_capabilities(&self, scheme: &UsageScheme) -> PaganResult<(RunAs, Vec<&AttackerCapability>)> {
        let filtered_caps_by_user = {
            let mut temp_map: HashMap<RunAs, Vec<&AttackerCapability>> = HashMap::new();

            self.capabilities
                .iter()
                .filter(|a_cap| scheme.capabilities_required.atomic_present(**a_cap))
                .for_each(|a_cap| temp_map.entry(a_cap.sid()).or_insert_with(Vec::new).push(a_cap));

            temp_map.retain(|_key, value| {
                scheme
                    .capabilities_required
                    .eval_state(FlagSet::from(Token::None), value)
            });

            temp_map
        };

        let best_option = {
            let mut max_cap_strength = 0f64;

            let mut best_batch_id = RunAs::Privilege(PrivilegeLevel::None);

            for (key, caps) in &filtered_caps_by_user {
                let avg = caps.iter().map(|a_cap| a_cap.strength()).sum::<u64>() as f64 / caps.len() as f64;

                if avg > max_cap_strength {
                    max_cap_strength = avg;

                    best_batch_id = *key;
                }
            }

            best_batch_id
        };

        match &best_option {
            RunAs::Privilege(level) => match level {
                PrivilegeLevel::None => Ok((best_option, vec![])),
                _ => Err(PaganError::AutomatonBuildingError(
                    "Capabilities not connected to an account should not be used in prerequisites.".into(),
                )),
            },
            _ => Ok((
                best_option,
                filtered_caps_by_user
                    .get(&best_option)
                    .ok_or(PaganError::NoSuchPrimaryKey("Invalid user idx".into()))?
                    .to_owned(),
            )),
        }
    }

    /**
    Creates a new `ArtifactSatchel` with no preprocessing, useful for scenarios where custom
    processing is needed, and is done by the caller.
    **/

    pub fn new<'store: 'exp>(
        tokens: FlagSet<Token>,
        capabilities: impl Iterator<Item = &'store AttackerCapability>,
    ) -> Self {
        Self {
            tokens,
            capabilities: capabilities.collect::<BTreeSet<&AttackerCapability>>(),
        }
    }
}

/**
The scheme describing the ACS requirements and transformations done by a Quiver action.
 **/
pub struct UsageScheme {
    pub given:                 FlagSet<Token>,
    pub grant:                 FlagSet<Token>,
    pub capabilities_required: Predicate,
    pub capabilities_provided: Vec<CapabilityTemplate>,
    pub enabler_required:      Option<EnablerTemplate>,
    pub except_if:             Predicate,
    pub privilege_origin:      PrivilegeOrigin,
    pub capability_specifier:  CapabilitySpecifier,
    pub credential_origin:     CredentialOrigin,
    pub credential_type:       CredentialType,
    pub specials:              Vec<(Specials, PrivilegeLevel)>,
}

impl UsageScheme {
    /**
    Return true if the satchels contents conform to the usage requirements.
     **/
    #[must_use]
    pub fn check_applicability(&self, satchel: &ArtifactSatchel) -> bool {
        satchel.tokens.contains(self.given)
            && self.capabilities_required.eval_satchel(satchel)
            && !self.except_if.eval_satchel(satchel)
    }

    pub fn validate(&self) {
        assert!(
            !self.grant.contains(Token::Capability)
                && (self.privilege_origin == PrivilegeOrigin::None
                    || self.capability_specifier == CapabilitySpecifier::None),
            "Invalid Quiver action: grants capability but does not specify its origin or limits."
        );
        assert!(
            !(self.capability_specifier == CapabilitySpecifier::Enabler && self.enabler_required.is_none()),
            "Invalid Quiver action: Capability limits stem from the enabler but none is required."
        );
        assert!(
            !(self.grant.contains(Token::Credential) || self.grant.contains(Token::CredentialMfa))
                && (self.credential_type == CredentialType::None || self.credential_origin == CredentialOrigin::None),
            "Invalid Quiver action: grants credential but does not specify its origin or limits."
        );
    }
}

pub struct ActionDescription {
    pub name:    String,
    pub aliases: Vec<String>,
    pub remote:  bool,
    pub uses:    Vec<UsageScheme>,
    pub notes:   Option<String>,
}

impl ActionDescription {
    #[must_use]
    pub const fn uses(&self) -> &Vec<UsageScheme> { &self.uses }
}
