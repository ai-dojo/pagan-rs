use std::{ops::Coroutine, pin::Pin};

use crate::primitives::{accounts::Account, credentials::Credential, shared::types::PaganResult};

#[derive(PartialEq, Hash, Eq, Debug, serde::Serialize, serde::Deserialize, Ord, PartialOrd)]
pub enum BusinessRole {}

#[derive(PartialEq, Hash, Eq, Debug, serde::Serialize, PartialOrd, Ord)]
pub struct Person {
    accounts:    Vec<&'static Account>,
    credentials: Vec<&'static Credential>,
    role:        BusinessRole,
}

impl Person {
    pub fn new(
        accounts: Vec<&'static Account>,
        credentials: Vec<&'static Credential>,
        role: BusinessRole,
    ) -> PaganResult<Self> {
        Ok(Self {
            accounts,
            credentials,
            role,
        })
    }

    pub fn add_account(&mut self, account: &'static Account) { self.accounts.push(account) }

    pub fn add_credential(&mut self, credential: &'static Credential) { self.credentials.push(credential); }

    #[must_use]
    pub fn credentials(&self) -> Pin<Box<dyn Coroutine<Yield = &Credential, Return = ()> + '_>> {
        let generator = || {
            for account in &self.accounts {
                for credential in account.credentials() {
                    yield *credential;
                }
            }

            for credential in &self.credentials {
                yield *credential;
            }
        };

        Box::pin(generator)
    }
}
