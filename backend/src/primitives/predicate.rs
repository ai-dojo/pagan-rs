#![allow(clippy::module_name_repetitions)]

use std::borrow::Borrow;

use flagset::FlagSet;

use crate::primitives::{
    adversary::ArtifactSatchel,
    capabilities::{AttackerCapability, CapabilityTemplate, MatchesCapabilityTemplate},
    shared::enums::{EnablerImpact, Token},
};

pub trait FindAtomic<T> {
    fn atomic_present(&self, atomic: T) -> bool;
}

pub enum PredicateNode {
    Intermediary(Vec<Predicate>),
    TokenLeaf(Vec<Token>),
    CapabilityLeaf(Vec<CapabilityTemplate>),
    ImpactLeaf(Vec<EnablerImpact>),
}

impl PredicateNode {
    #[must_use]
    pub fn eval_satchel_all(&self, satchel: &ArtifactSatchel) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().all(|elem| elem.eval_satchel(satchel)),
            Self::TokenLeaf(inner) => inner
                .iter()
                .all(|token_type| satchel.tokens().contains(token_type.to_owned())),
            Self::CapabilityLeaf(inner) => inner
                .iter()
                .all(|template| satchel.capabilities().iter().any(|cap| cap.matched_by(template))),
            Self::ImpactLeaf(_inner) => false,
        }
    }

    #[must_use]
    pub fn eval_satchel_any(&self, satchel: &ArtifactSatchel) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().any(|elem| elem.eval_satchel(satchel)),
            Self::TokenLeaf(inner) => inner
                .iter()
                .any(|token_type| satchel.tokens().contains(token_type.to_owned())),
            Self::CapabilityLeaf(inner) => inner
                .iter()
                .any(|template| satchel.capabilities().iter().any(|cap| cap.matched_by(template))),
            Self::ImpactLeaf(_inner) => false,
        }
    }

    #[must_use]
    pub fn eval_state_any(&self, tokens: FlagSet<Token>, capabilities: &[impl Borrow<AttackerCapability>]) -> bool {
        match self {
            Self::Intermediary(inner) => inner.iter().all(|pred| pred.eval_state(tokens, capabilities)),
            Self::TokenLeaf(inner) => inner.iter().any(|token_type| tokens.contains(token_type.to_owned())),
            Self::CapabilityLeaf(inner) => inner
                .iter()
                .any(|template| capabilities.iter().any(|cap| (*cap).borrow().matched_by(template))),
            Self::ImpactLeaf(_) => false,
        }
    }

    #[must_use]
    pub fn eval_state_all(&self, tokens: FlagSet<Token>, capabilities: &[impl Borrow<AttackerCapability>]) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().all(|elem| elem.eval_state(tokens, capabilities)),
            Self::TokenLeaf(inner) => inner.iter().all(|token_type| tokens.contains(token_type.to_owned())),
            Self::CapabilityLeaf(inner) => inner
                .iter()
                .all(|template| capabilities.iter().any(|cap| (*cap).borrow().matched_by(template))),
            Self::ImpactLeaf(_inner) => false,
        }
    }

    #[must_use]
    pub fn eval_impacts_any(&self, impacts: FlagSet<EnablerImpact>) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().any(|elem| elem.eval_impacts(impacts)),
            Self::TokenLeaf(_) | Self::CapabilityLeaf(_) => false,
            Self::ImpactLeaf(inner) => inner.iter().any(|elem| impacts.contains(elem.to_owned())),
        }
    }

    #[must_use]
    pub fn eval_impacts_all(&self, impacts: FlagSet<EnablerImpact>) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().all(|elem| elem.eval_impacts(impacts)),
            Self::TokenLeaf(_) | Self::CapabilityLeaf(_) => false,
            Self::ImpactLeaf(inner) => inner.iter().all(|elem| impacts.contains(elem.to_owned())),
        }
    }
}

impl FindAtomic<Token> for PredicateNode {
    #[must_use]
    fn atomic_present(&self, atomic: Token) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().any(|pred| pred.atomic_present(atomic)),
            Self::TokenLeaf(inner) => inner.iter().any(|ttype| ttype == atomic.borrow()),
            Self::CapabilityLeaf(_) | Self::ImpactLeaf(_) => false,
        }
    }
}

impl FindAtomic<&AttackerCapability> for PredicateNode {
    #[must_use]
    fn atomic_present(&self, atomic: &AttackerCapability) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().any(|pred| pred.atomic_present(atomic)),
            Self::TokenLeaf(_) | Self::ImpactLeaf(_) => false,
            Self::CapabilityLeaf(inner) => inner.iter().any(|template| atomic.matched_by(template)),
        }
    }
}

impl FindAtomic<EnablerImpact> for PredicateNode {
    #[must_use]
    fn atomic_present(&self, atomic: EnablerImpact) -> bool {
        match &self {
            Self::Intermediary(inner) => inner.iter().any(|pred| pred.atomic_present(atomic)),
            Self::ImpactLeaf(inner) => inner.iter().any(|impact| impact == atomic.borrow()),
            Self::CapabilityLeaf(_) | Self::TokenLeaf(_) => false,
        }
    }
}

/**
   Predicates to write rules for Quiver actions. This enums provide the base types. Extended
   options are available through Builders which provides named combinators.

    For predicate instantiation use the Builders in `::predicate_builders::*`;
**/
pub enum Predicate {
    All(PredicateNode),
    Any(PredicateNode),
    Not(Box<Predicate>),
    Empty,
}

impl Predicate {
    #[must_use]
    pub fn eval_satchel(&self, satchel: &ArtifactSatchel) -> bool {
        match &self {
            Self::All(inner) => inner.eval_satchel_all(satchel),
            Self::Any(inner) => inner.eval_satchel_any(satchel),
            Self::Not(inner) => !inner.eval_satchel(satchel),
            Self::Empty => true,
        }
    }

    #[must_use]
    pub fn eval_state(&self, tokens: FlagSet<Token>, capabilities: &[impl Borrow<AttackerCapability>]) -> bool {
        match &self {
            Self::All(inner) => inner.eval_state_all(tokens, capabilities),
            Self::Any(inner) => inner.eval_state_any(tokens, capabilities),
            Self::Not(inner) => !inner.eval_state(tokens, capabilities),
            Self::Empty => true,
        }
    }

    #[must_use]
    pub fn eval_impacts(&self, impacts: FlagSet<EnablerImpact>) -> bool {
        match &self {
            Self::All(inner) => inner.eval_impacts_all(impacts),
            Self::Any(inner) => inner.eval_impacts_any(impacts),
            Self::Not(inner) => !inner.eval_impacts(impacts),
            Self::Empty => true,
        }
    }
}

impl FindAtomic<&AttackerCapability> for Predicate {
    #[must_use]
    fn atomic_present(&self, atomic: &AttackerCapability) -> bool {
        match &self {
            Self::All(inner) | Self::Any(inner) => inner.atomic_present(atomic),
            Self::Not(inner) => inner.atomic_present(atomic),
            Self::Empty => false,
        }
    }
}

impl FindAtomic<Token> for Predicate {
    #[must_use]
    fn atomic_present(&self, atomic: Token) -> bool {
        match &self {
            Self::All(inner) | Self::Any(inner) => inner.atomic_present(atomic),
            Self::Not(inner) => inner.atomic_present(atomic),
            Self::Empty => false,
        }
    }
}

impl FindAtomic<EnablerImpact> for Predicate {
    #[must_use]
    fn atomic_present(&self, atomic: EnablerImpact) -> bool {
        match &self {
            Self::All(inner) | Self::Any(inner) => inner.atomic_present(atomic),
            Self::Not(inner) => inner.atomic_present(atomic),
            Self::Empty => false,
        }
    }
}

pub mod predicate_builders {

    use strum::IntoEnumIterator;

    use crate::primitives::{
        capabilities::CapabilityTemplate,
        predicate::{Predicate, PredicateNode},
        shared::enums::{EnablerImpact, Token},
    };

    pub trait Builder<I> {
        fn build_from(value: I) -> Predicate;
    }

    pub struct Not {}

    pub struct Empty {}

    pub struct All {}

    pub struct Any {}

    pub struct NoneOf {}

    pub struct NotJust {}

    impl Builder<&[Token]> for All {
        fn build_from(value: &[Token]) -> Predicate { Predicate::All(PredicateNode::TokenLeaf(Vec::from(value))) }
    }

    impl Builder<&[CapabilityTemplate]> for All {
        fn build_from(value: &[CapabilityTemplate]) -> Predicate {
            Predicate::All(PredicateNode::CapabilityLeaf(Vec::from(value)))
        }
    }

    impl Builder<&[EnablerImpact]> for All {
        fn build_from(value: &[EnablerImpact]) -> Predicate {
            Predicate::All(PredicateNode::ImpactLeaf(Vec::from(value)))
        }
    }

    impl<const N: usize> Builder<[Predicate; N]> for All {
        fn build_from(value: [Predicate; N]) -> Predicate {
            Predicate::All(PredicateNode::Intermediary(Vec::from(value)))
        }
    }

    impl Builder<Vec<Token>> for All {
        fn build_from(value: Vec<Token>) -> Predicate { Predicate::All(PredicateNode::TokenLeaf(value)) }
    }

    impl Builder<Vec<CapabilityTemplate>> for All {
        fn build_from(value: Vec<CapabilityTemplate>) -> Predicate {
            Predicate::All(PredicateNode::CapabilityLeaf(value))
        }
    }

    impl Builder<Vec<EnablerImpact>> for All {
        fn build_from(value: Vec<EnablerImpact>) -> Predicate { Predicate::All(PredicateNode::ImpactLeaf(value)) }
    }

    impl Builder<Vec<Predicate>> for All {
        fn build_from(value: Vec<Predicate>) -> Predicate { Predicate::All(PredicateNode::Intermediary(value)) }
    }

    impl Builder<&[Token]> for Any {
        fn build_from(value: &[Token]) -> Predicate { Predicate::Any(PredicateNode::TokenLeaf(Vec::from(value))) }
    }

    impl Builder<&[CapabilityTemplate]> for Any {
        fn build_from(value: &[CapabilityTemplate]) -> Predicate {
            Predicate::Any(PredicateNode::CapabilityLeaf(Vec::from(value)))
        }
    }

    impl Builder<&[EnablerImpact]> for Any {
        fn build_from(value: &[EnablerImpact]) -> Predicate {
            Predicate::Any(PredicateNode::ImpactLeaf(Vec::from(value)))
        }
    }

    impl<const N: usize> Builder<[Predicate; N]> for Any {
        fn build_from(value: [Predicate; N]) -> Predicate {
            Predicate::Any(PredicateNode::Intermediary(Vec::from(value)))
        }
    }

    impl Builder<Vec<Token>> for Any {
        fn build_from(value: Vec<Token>) -> Predicate { Predicate::Any(PredicateNode::TokenLeaf(value)) }
    }

    impl Builder<Vec<CapabilityTemplate>> for Any {
        fn build_from(value: Vec<CapabilityTemplate>) -> Predicate {
            Predicate::Any(PredicateNode::CapabilityLeaf(value))
        }
    }

    impl Builder<Vec<EnablerImpact>> for Any {
        fn build_from(value: Vec<EnablerImpact>) -> Predicate { Predicate::Any(PredicateNode::ImpactLeaf(value)) }
    }

    impl Builder<Vec<Predicate>> for Any {
        fn build_from(value: Vec<Predicate>) -> Predicate { Predicate::Any(PredicateNode::Intermediary(value)) }
    }

    impl Builder<Box<Predicate>> for Not {
        fn build_from(value: Box<Predicate>) -> Predicate { Predicate::Not(value) }
    }

    impl Builder<EnablerImpact> for Not {
        fn build_from(value: EnablerImpact) -> Predicate {
            Predicate::Not(Box::new(Any::build_from([value].as_slice())))
        }
    }

    impl Builder<&[Token]> for NoneOf {
        fn build_from(value: &[Token]) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl Builder<&[EnablerImpact]> for NoneOf {
        fn build_from(value: &[EnablerImpact]) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl Builder<&[CapabilityTemplate]> for NoneOf {
        fn build_from(value: &[CapabilityTemplate]) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl<const N: usize> Builder<[Predicate; N]> for NoneOf {
        fn build_from(value: [Predicate; N]) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl Builder<Vec<Token>> for NoneOf {
        fn build_from(value: Vec<Token>) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl Builder<Vec<EnablerImpact>> for NoneOf {
        fn build_from(value: Vec<EnablerImpact>) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl Builder<Vec<CapabilityTemplate>> for NoneOf {
        fn build_from(value: Vec<CapabilityTemplate>) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl Builder<Vec<Predicate>> for NoneOf {
        fn build_from(value: Vec<Predicate>) -> Predicate { Not::build_from(Box::new(Any::build_from(value))) }
    }

    impl Builder<&[Token]> for NotJust {
        fn build_from(value: &[Token]) -> Predicate {
            let remainders = Token::iter()
                .filter(|variant| !value.contains(variant))
                .collect::<Vec<Token>>();

            All::build_from([All::build_from(value), Any::build_from(remainders.as_slice())])
        }
    }

    impl Builder<&[EnablerImpact]> for NotJust {
        fn build_from(value: &[EnablerImpact]) -> Predicate {
            let remainders = EnablerImpact::iter()
                .filter(|variant| !value.contains(variant))
                .collect::<Vec<EnablerImpact>>();

            All::build_from([All::build_from(value), Any::build_from(remainders.as_slice())])
        }
    }

    impl Builder<Vec<Token>> for NotJust {
        fn build_from(value: Vec<Token>) -> Predicate {
            let remainders = Token::iter()
                .filter(|variant| !value.contains(variant))
                .collect::<Vec<Token>>();

            All::build_from([All::build_from(value), Any::build_from(remainders.as_slice())])
        }
    }

    impl Builder<Vec<EnablerImpact>> for NotJust {
        fn build_from(value: Vec<EnablerImpact>) -> Predicate {
            let remainders = EnablerImpact::iter()
                .filter(|variant| !value.contains(variant))
                .collect::<Vec<EnablerImpact>>();

            All::build_from([All::build_from(value), Any::build_from(remainders.as_slice())])
        }
    }

    impl Builder<()> for Empty {
        fn build_from(_value: ()) -> Predicate { Predicate::Empty }
    }
}

#[cfg(test)]
mod test {}
