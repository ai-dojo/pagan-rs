use std::collections::{HashSet, VecDeque};

use rand::{prelude::IteratorRandom, seq::SliceRandom};

use crate::{
    error::PaganError,
    primitives::{
        accounts::Account,
        credentials::Credential,
        shared::{
            enums::{CapabilityType, DeviceRole, OperatingSystem, PrivilegeLevel, SegmentType},
            traits::PrimitiveStrength,
            types::{DeviceHandle, PaganResult, ServiceHandle},
            utils::{HandleSet, Port, SetOption, SetOptionAtomicOps},
        },
    },
};

macro_rules! random_acc_with_priv {
    ($priv:expr, $self:ident) => {{
        let mut rng = rand::thread_rng();
        let self_id = $self
            .id
            .ok_or(PaganError::TopologyError("Device has no ID set.".into()))?;
        Ok($self
            .accounts
            .iter()
            .filter(|acc| {
                acc.capabilities()
                    .get(&self_id)
                    .unwrap()
                    .iter()
                    .any(|cap| cap.to() == CapabilityType::CodeExecution && cap.privilege_level() == $priv)
            })
            .choose(&mut rng)
            .and_then(|val| Some(*val)))
    }};
}

#[derive(PartialEq, Eq, Hash, Debug, serde::Serialize, PartialOrd, Ord)]
pub struct Device {
    name:                String,
    vlan:                (i32, SegmentType),
    roles:               Vec<DeviceRole>,
    os:                  OperatingSystem,
    prohibited_software: HandleSet,
    compulsory_software: HandleSet,
    open_ports:          Vec<Port>,
    cache_capacity:      usize,
    credential_cache:    VecDeque<&'static Credential>, // simulates LSASS cache
    accounts:            Vec<&'static Account>,
    r#virtual:           bool,
    usb_access:          bool,
    system_account:      Option<&'static Account>,
    service_account:     Option<&'static Account>,
    id:                  Option<DeviceHandle>,
}

impl Device {
    #[must_use]
    pub fn new(
        name: String,
        vlan: (i32, SegmentType),
        mut roles: Vec<DeviceRole>,
        os: OperatingSystem,
        prohibited_software: SetOption<ServiceHandle>,
        compulsory_software: SetOption<ServiceHandle>,
        open_ports: Vec<Port>,
        cache_capacity: usize,
        r#virtual: bool,
        usb_access: bool,
    ) -> Self {
        roles.sort_by_key(PrimitiveStrength::strength);

        Self {
            name,
            vlan,
            roles,
            os,
            prohibited_software: HandleSet::Services(prohibited_software),
            compulsory_software: HandleSet::Services(compulsory_software),
            open_ports,
            cache_capacity,
            credential_cache: VecDeque::with_capacity(cache_capacity),
            accounts: Vec::new(),
            r#virtual,
            usb_access,
            system_account: None,
            service_account: None,
            id: None,
        }
    }

    #[must_use]
    pub fn name(&self) -> &str { &self.name }

    #[must_use]
    pub const fn vlan(&self) -> &(i32, SegmentType) { &self.vlan }

    #[must_use]
    pub const fn roles(&self) -> &Vec<DeviceRole> { &self.roles }

    #[must_use]
    pub const fn os(&self) -> OperatingSystem { self.os }

    #[must_use]
    pub const fn prohibited_software(&self) -> &HandleSet { &self.prohibited_software }

    #[must_use]
    pub const fn compulsory_software(&self) -> &HandleSet { &self.compulsory_software }

    #[must_use]
    pub const fn open_ports(&self) -> &Vec<Port> { &self.open_ports }

    #[must_use]
    pub const fn cache_capacity(&self) -> usize { self.cache_capacity }

    #[must_use]
    pub const fn credential_cache(&self) -> &VecDeque<&Credential> { &self.credential_cache }

    #[must_use]
    pub const fn accounts(&self) -> &Vec<&'static Account> { &self.accounts }

    #[must_use]
    pub const fn r#virtual(&self) -> bool { self.r#virtual }

    #[must_use]
    pub const fn usb_access(&self) -> bool { self.usb_access }

    pub fn add_prohibited_software(&mut self, software: ServiceHandle) { self.prohibited_software.insert(software); }

    pub fn add_compulsory_software(&mut self, software: ServiceHandle) { self.compulsory_software.insert(software) }

    pub fn add_account(&mut self, account: &'static Account) { self.accounts.push(account) }

    pub fn add_system_account(&mut self, account: &'static Account) {
        self.accounts.push(account);
        self.system_account = Some(account);
    }

    pub fn add_service_account(&mut self, account: &'static Account) {
        self.accounts.push(account);
        self.service_account = Some(account);
    }

    #[must_use]
    pub fn get_random_account(&self) -> Option<&'static Account> {
        let mut rng = rand::thread_rng();

        self.accounts.choose(&mut rng).copied()
    }

    pub fn get_random_user_account(&self) -> PaganResult<Option<&'static Account>> {
        random_acc_with_priv!(PrivilegeLevel::User, self)
    }

    pub fn get_random_admin_account(&self) -> PaganResult<Option<&'static Account>> {
        random_acc_with_priv!(PrivilegeLevel::Administrator, self)
    }

    pub fn get_random_interactive_account(&self) -> PaganResult<Option<&'static Account>> {
        let user = self.get_random_user_account()?;
        let admin = self.get_random_admin_account()?;

        match user {
            None => match admin {
                None => Ok(None),
                Some(adm) => Ok(Some(adm)),
            },
            Some(user_account) => match admin {
                None => Ok(Some(user_account)),
                Some(admin_account) => {
                    let mut rng = rand::thread_rng();
                    Ok(Some(*[user_account, admin_account].choose(&mut rng).unwrap()))
                },
            },
        }
    }

    /**
     Simulate the LSASS cache as on account login.
    **/
    pub fn cache_credential(&mut self, credential: &'static Credential) {
        if self.credential_cache.contains(&credential) {
            return;
        }

        if self.credential_cache.len() >= self.cache_capacity {
            self.credential_cache.pop_front();
        }

        self.credential_cache.push_back(credential);
    }

    /**
    Yields all credentials that are associated with this device. Meaning that the owner (person)
    has an account on this device.
    **/
    #[must_use]
    pub fn get_associated_credentials(&self) -> HashSet<&'static Credential> {
        let mut credential_set = HashSet::new();

        for account in &self.accounts {
            if account.owner().is_some() {
                match account.owner() {
                    None => continue,
                    Some(person) => credential_set.extend(std::iter::from_coroutine(person.credentials())),
                }
            }
        }

        credential_set
    }

    #[must_use]
    pub const fn system_account(&self) -> Option<&'static Account> { self.system_account }

    #[must_use]
    pub const fn service_account(&self) -> Option<&'static Account> { self.service_account }

    pub fn set_id(&mut self, id: DeviceHandle) { self.id = Some(id); }
}

impl PrimitiveStrength for Device {
    fn strength(&self) -> u64 { self.roles.iter().map(DeviceRole::strength).sum() }
}
