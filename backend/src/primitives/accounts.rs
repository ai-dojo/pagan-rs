use std::{collections::BTreeMap, hash::Hash};

use crate::primitives::{
    capabilities::Capability,
    credentials::Credential,
    device::Device,
    person::Person,
    shared::{
        traits::{ComplexStrength, PrimitiveStrength},
        types::{AccountHandle, DeviceHandle, PaganResult},
    },
};

#[derive(Debug, Hash, PartialEq, Eq, serde::Serialize, PartialOrd, Ord)]
pub struct Account {
    owner:          Option<&'static Person>,
    nickname:       String,
    credentials:    Vec<&'static Credential>,
    capabilities:   BTreeMap<DeviceHandle, Vec<Box<Capability>>>,
    domain_account: bool,
    valid:          bool,
    devices:        BTreeMap<DeviceHandle, &'static Device>,
    sid:            Option<AccountHandle>,
}

impl Account {
    pub fn new(
        owner: Option<&'static Person>,
        nickname: String,
        credentials: Vec<&'static Credential>,
        capabilities: BTreeMap<DeviceHandle, Vec<Box<Capability>>>,
        domain_account: bool,
        devices: Option<BTreeMap<DeviceHandle, &'static Device>>,
    ) -> PaganResult<Self> {
        Ok(Self {
            owner,
            nickname,
            credentials,
            capabilities,
            domain_account,
            valid: true,
            devices: devices.unwrap_or_default(),
            sid: None,
        })
    }

    #[must_use] pub const fn owner(&self) -> Option<&Person> { self.owner }

    #[must_use] pub fn nickname(&self) -> &str { &self.nickname }

    #[must_use] pub const fn credentials(&self) -> &Vec<&Credential> { &self.credentials }

    #[must_use] pub const fn capabilities(&self) -> &BTreeMap<DeviceHandle, Vec<Box<Capability>>> { &self.capabilities }

    #[must_use] pub const fn domain_account(&self) -> bool { self.domain_account }

    pub fn add_credential(&mut self, credential: &'static Credential) -> PaganResult<()> {
        self.credentials.push(credential);

        Ok(())
    }

    pub fn add_capability(&mut self, capability: Capability) {
        self.capabilities
            .entry(capability.device())
            .or_default()
            .push(Box::new(capability));
    }

    #[must_use] pub fn get_associated_credentials(&self) -> Vec<&Credential> {
        self.owner.as_ref().map_or_else(Vec::new, |person| std::iter::from_coroutine(person.credentials()).collect::<Vec<&Credential>>())
    }

    pub fn invalidate(&mut self) { self.valid = false; }

    pub fn add_device(&mut self, handle: DeviceHandle, device: &'static Device) { self.devices.insert(handle, device); }

    #[must_use] pub fn is_for_device(&self, device: DeviceHandle) -> bool {
        self.capabilities.keys().any(|handle| handle == &device)
    }

    #[must_use] pub fn devices(&self) -> Vec<DeviceHandle> { self.capabilities.keys().copied().collect::<Vec<DeviceHandle>>() }

    pub fn set_sid(&mut self, sid: AccountHandle) { self.sid = Some(sid); }

    #[must_use] pub const fn sid(&self) -> Option<AccountHandle> { self.sid }
}

impl ComplexStrength for Account {
    fn strength(&self, target: Option<DeviceHandle>) -> u64 {
        target.as_ref().map_or_else(|| {
                let mut res = 0u64;

                for (handle, caps) in &self.capabilities {
                    res += caps.iter().map(|cap| cap.strength()).sum::<u64>()
                        * self.devices.get(handle).unwrap().strength();
                }

                res
            }, |dev| self.capabilities.get(dev).map_or(0u64, |caps| self.devices.get(dev).unwrap().strength() * caps.iter().map(|cap| cap.strength()).sum::<u64>()))
    }
}
