use std::{
    collections::{BTreeSet, HashMap},
    pin::Pin,
};

use gringotts::Service;

use crate::{
    error::PaganError,
    primitives::{
        service::LightService,
        shared::{
            types::{PaganResult, ServiceHandle},
            utils::Counter,
        },
    },
    stores::{StoreRead, StoreWrite},
};

#[derive(Debug)]
pub struct ServiceStore {
    elems:   HashMap<ServiceHandle, Pin<Box<LightService>>>,
    counter: Counter,
}

impl StoreRead for ServiceStore {
    type Item = LightService;
    type Key = ServiceHandle;

    fn handles(&self) -> BTreeSet<Self::Key> { self.elems.keys().copied().collect::<BTreeSet<Self::Key>>() }

    fn cardinality(&self) -> usize { self.elems.len() }

    fn get(&self, pk: Self::Key) -> PaganResult<&Self::Item> {
        Ok(self
            .elems
            .get(&pk)
            .ok_or(PaganError::NoSuchPrimaryKey(format!("No service with ID: {pk}").into()))?
            .as_ref()
            .get_ref())
    }
}

impl StoreWrite for ServiceStore {
    type Item = Service;

    fn add_item(&mut self, item: Self::Item) { self.elems.insert(item.id as ServiceHandle, Box::pin(item.into())); }
}
