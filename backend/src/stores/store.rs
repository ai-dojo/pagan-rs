use std::collections::BTreeSet;

use num::Num;

use crate::primitives::shared::types::PaganResult;

pub trait StoreRead {
    type Item;

    type Key: Num + Copy;

    fn handles(&self) -> BTreeSet<Self::Key>;

    fn cardinality(&self) -> usize;

    fn get(&self, pk: Self::Key) -> PaganResult<&Self::Item>;
}

pub trait StoreWrite {
    type Item;

    fn add_item(&mut self, item: Self::Item);
}

pub trait StoreReverseGet {
    type Item;
    type Key: Num + Copy;

    fn get_handle(&self, val: &Self::Item) -> Option<Self::Key>;
}
