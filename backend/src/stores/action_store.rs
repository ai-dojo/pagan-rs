use std::collections::BTreeSet;

use crate::{
    error::PaganError,
    primitives::{
        adversary::{ActionDescription, ArtifactSatchel, UsageScheme},
        quiver::ADVERSARY_ACTION_MAP,
        shared::types::{ActionHandle, PaganResult},
    },
    stores::StoreRead,
};

#[derive(Clone, Debug, Default)]
pub struct ActionStore {}

impl ActionStore {
    pub fn get_applicable_actions(&self, satchel: &ArtifactSatchel) -> Vec<(ActionHandle, usize, &UsageScheme)> {
        let mut res = Vec::new();

        for (handle, action) in ADVERSARY_ACTION_MAP.iter() {
            for (idx, scheme) in action.uses.iter().enumerate() {
                if scheme.check_applicability(satchel) {
                    res.push((handle.to_owned(), idx, scheme));
                }
            }
        }

        res
    }
}

impl StoreRead for ActionStore {
    type Item = ActionDescription;
    type Key = ActionHandle;

    fn handles(&self) -> BTreeSet<Self::Key> { ADVERSARY_ACTION_MAP.keys().copied().collect::<BTreeSet<_>>() }

    fn cardinality(&self) -> usize { ADVERSARY_ACTION_MAP.len() }

    fn get(&self, pk: Self::Key) -> PaganResult<&Self::Item> {
        ADVERSARY_ACTION_MAP
            .get(&pk)
            .map_or_else(|| Err(PaganError::NoSuchPrimaryKey("Action does not exist".into())), Ok)
    }
}
