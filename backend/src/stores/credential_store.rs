use std::{
    collections::{BTreeSet, HashMap},
    pin::Pin,
};

use crate::{
    error::PaganError,
    primitives::{
        credentials::Credential,
        shared::{
            types::{CredentialHandle, PaganResult},
            utils::Counter,
        },
    },
    stores::{StoreRead, StoreWrite},
};

#[derive(Default)]
pub struct CredentialStore {
    elems:   HashMap<CredentialHandle, Pin<Box<Credential>>>,
    counter: Counter,
}

impl StoreRead for CredentialStore {
    type Item = Credential;
    type Key = CredentialHandle;

    fn handles(&self) -> BTreeSet<Self::Key> { self.elems.keys().copied().collect::<BTreeSet<Self::Key>>() }

    fn cardinality(&self) -> usize { self.elems.len() }

    fn get(&self, pk: Self::Key) -> PaganResult<&Self::Item> {
        Ok(self
            .elems
            .get(&pk)
            .ok_or(PaganError::NoSuchPrimaryKey(
                format!("Credential with id {pk} not found").into(),
            ))?
            .as_ref()
            .get_ref())
    }
}

impl StoreWrite for CredentialStore {
    type Item = Credential;

    fn add_item(&mut self, item: Self::Item) { self.elems.insert(self.counter.give(), Box::pin(item)); }
}
