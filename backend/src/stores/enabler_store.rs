use std::{
    collections::{BTreeSet, HashMap},
    pin::Pin,
};

use gringotts::Enabler;

use crate::{
    error::PaganError,
    primitives::{
        enablers::LightEnabler,
        shared::types::{EnablerHandle, PaganResult},
    },
    stores::{StoreRead, StoreWrite},
};

#[derive(Debug)]
pub struct EnablerStore {
    elems: HashMap<EnablerHandle, Pin<Box<LightEnabler>>>,
}


impl StoreRead for EnablerStore {
    type Item = LightEnabler;
    type Key = EnablerHandle;

    fn handles(&self) -> BTreeSet<Self::Key> { self.elems.keys().copied().collect::<BTreeSet<EnablerHandle>>() }

    fn cardinality(&self) -> usize { self.elems.len() }

    fn get(&self, pk: Self::Key) -> PaganResult<&Self::Item> {
        Ok(self
            .elems
            .get(&pk)
            .ok_or(PaganError::NoSuchPrimaryKey(
                format!("Invalid enabler id ({pk}) queried.").into(),
            ))?
            .as_ref()
            .get_ref())
    }
}

impl StoreWrite for EnablerStore {
    type Item = Enabler;

    fn add_item(&mut self, item: Self::Item) { self.elems.insert(item.id as EnablerHandle, Box::pin(item.into())); }
}
