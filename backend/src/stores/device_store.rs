use std::{
    collections::{BTreeSet, HashMap},
    pin::Pin,
};

use crate::{
    error::PaganError,
    primitives::{
        device::Device,
        shared::{
            types::{DeviceHandle, PaganResult},
            utils::Counter,
        },
    },
    stores::{StoreRead, StoreWrite},
};

#[derive(Default)]
pub struct DeviceStore {
    elems:    HashMap<DeviceHandle, Pin<Box<Device>>>,
    attacker: Option<DeviceHandle>,
    counter:  Counter,
}

impl DeviceStore {
    #[must_use]
    pub const fn devices(&self) -> &HashMap<DeviceHandle, Pin<Box<Device>>> { &self.elems }

    #[must_use]
    pub const fn attacker(&self) -> Option<DeviceHandle> { self.attacker }
}


impl DeviceStore {
    pub(super) fn add_device(&mut self, device: Device, attacker: bool) -> PaganResult<()> {
        if attacker {
            match self.attacker {
                None => {},
                Some(_) => return Err(PaganError::TopologyError("Can not have multiple attackers".into())),
            }
        }

        let handle = self.counter.give();

        self.elems.insert(handle, Box::pin(device));

        if attacker {
            self.attacker = Some(handle);
        }

        Ok(())
    }
}

impl StoreRead for DeviceStore {
    type Item = Device;
    type Key = DeviceHandle;

    fn handles(&self) -> BTreeSet<Self::Key> { todo!() }

    fn cardinality(&self) -> usize { todo!() }

    fn get(&self, _pk: Self::Key) -> PaganResult<&Self::Item> { todo!() }
}

impl StoreWrite for DeviceStore {
    type Item = Device;

    fn add_item(&mut self, mut item: Self::Item) {
        let did = self.counter.give();
        item.set_id(did);
        self.elems.insert(did, Box::pin(item));
    }
}
