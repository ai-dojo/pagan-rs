use std::{
    collections::{BTreeSet, HashMap},
    pin::Pin,
};

use crate::{
    error::PaganError,
    primitives::{
        accounts::Account,
        shared::{
            types::{AccountHandle, PaganResult},
            utils::Counter,
        },
    },
    stores::{StoreRead, StoreReverseGet, StoreWrite},
};

#[derive(Default)]
pub struct AccountStore {
    elems:         HashMap<AccountHandle, Pin<Box<Account>>>,
    uac_pairs:     HashMap<AccountHandle, AccountHandle>,
    reverse_store: HashMap<&'static Account, AccountHandle>,
    counter:       Counter,
}

impl AccountStore {
    #[must_use]
    pub fn uac_elevate(&self, handle: AccountHandle) -> AccountHandle {
        self.uac_pairs.get(&handle).unwrap_or(&handle).to_owned()
    }
}

impl StoreRead for AccountStore {
    type Item = Account;
    type Key = AccountHandle;

    fn handles(&self) -> BTreeSet<Self::Key> { self.elems.keys().copied().collect::<BTreeSet<Self::Key>>() }

    fn cardinality(&self) -> usize { self.elems.len() }

    fn get(&self, pk: Self::Key) -> PaganResult<&'static Self::Item> {
        Ok(unsafe {
            std::mem::transmute::<&Self::Item, &'static Self::Item>(
                self.elems
                    .get(&pk)
                    .ok_or(PaganError::NoSuchPrimaryKey(
                        format!("Account with id {pk} not found").into(),
                    ))?
                    .as_ref()
                    .get_ref(),
            )
        })
    }
}

impl StoreWrite for AccountStore {
    type Item = Account;

    fn add_item(&mut self, mut item: Self::Item) {
        let sid = self.counter.give();
        item.set_sid(sid);
        self.elems.insert(sid, Box::pin(item));
    }
}

impl StoreReverseGet for AccountStore {
    type Item = Account;
    type Key = AccountHandle;

    fn get_handle(&self, val: &Self::Item) -> Option<Self::Key> { self.reverse_store.get(val).copied() }
}
