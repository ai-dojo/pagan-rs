# PAGAN-RS

Database and API for `pagan`. Possibly a future implementation of the whole generation framework in Rust.

## Usage

  1. Start the database container via `docker compose up -d`.
  2. Build the project: `cargo build --release --workspace`.
  3. Do database migration: `.\target\release\migration.exe`.
  4. Populate the database: `.\target\release\populate.exe --mock <scenario>`.
  5. Start the API: `.\target\release\kb_api.exe --interface localhost`.