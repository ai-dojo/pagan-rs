#[macro_export]
macro_rules! compare_optional_weakref_attributes_for_eq {
    ($self_attr:expr, $other_attr:expr) => {
        match &$self_attr {
            None => match &$other_attr {
                None => {},
                Some(_) => return false,
            },
            Some(self_weak) => match &$other_attr {
                None => return false,
                Some(other_weak) => {
                    let maybe_self_strong = self_weak.upgrade();
                    let maybe_other_strong = other_weak.upgrade();
                    match &maybe_self_strong {
                        None => match &maybe_other_strong {
                            None => {},
                            Some(_) => return false,
                        },
                        Some(self_strong) => match &maybe_other_strong {
                            None => return false,
                            Some(other_strong) => match self_strong == other_strong {
                                true => {},
                                false => return false,
                            },
                        },
                    }
                },
            },
        }
    };
}

#[macro_export]
macro_rules! hash_optional_weakref_attribute {
    ($attr:expr, $ref_type:ty, $hasher:ident, $err:literal) => {
        match &$attr {
            None => None::<$ref_type>.hash($hasher),
            Some(weak_ref) => {
                let maybe_strong_ref = weak_ref.upgrade();
                match maybe_strong_ref {
                    None => {
                        panic!($err)
                    },
                    Some(strong_ref) => strong_ref.hash($hasher),
                }
            },
        }
    };
}
